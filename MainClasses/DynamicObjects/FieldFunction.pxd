#!/usr/bin/env python

"""
Python Profile CRF Project - FieldFunction.py
"""

__author__ = "Romulo Barroso Victor"
__credits__ = ["Romulo Barroso Victor", "Gerson Zaverucha", "Juliana Bernardes"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Romulo Barroso Victor"
__email__ = "rbvictor@cos.ufrj.br"
__status__ = "Development"
__date__ = "10-Jul-2013"


from MainClasses.FirstOrderLogic.FOLClassificationTree cimport FOLClassificationTree
from MainClasses.FirstOrderLogic.FOLDecisionTree cimport FOLDecisionTree
from MainClasses.FirstOrderLogic.FOLPathExample cimport FOLPathExample
from MainClasses.SuperFamily.InitialFunction cimport InitialFunction


cdef class FieldFunction:
    """The FieldFunction object stores the regression tree used to calculate the random field function value"""

    cdef public object default_value
    cdef public str dao_suffix
    cdef public int group_id
    cdef public object rmode_manager
    cdef public bint has_single_tree
    cdef public list decision_trees
    cdef public InitialFunction initial_value_function
    cdef public bint keeps_init_func
    cdef public object dao_work_manager_queue
    cdef public str ff_type
    cdef public float min_variance
    cdef public FOLDecisionTree new_tree
    cdef public FOLClassificationTree classification_tree
    cdef public list negative_example_scores

    cdef object check_example(self, FOLPathExample example)
    cdef long double calculate_ff(self, FOLPathExample example, object tested_positive=*) except 1e10
    cdef int feed_example(self, FOLPathExample example, bint coverage_mode_on=*) except -1
    cdef int add_negative_score(self, long double negative_score) except -1
    cdef int update_classification_function(self) except -1
    cdef int update_function(self) except -1
    cdef int set_value_to_false_leaf(self, long double average, long double exp_average, long double variance=*) except -1
    cdef int save_trees_to_db(self, int sf_id, bytes target_f_char_id) except -1
    cdef int save_last_tree_to_db(self, int sf_id, bytes target_f_char_id) except -1
    cdef int load_trees_from_db(self, bytes target_f_char_id, int num_trees) except -1