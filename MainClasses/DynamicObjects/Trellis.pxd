
from MainClasses.FirstOrderLogic.FOLPathExample cimport FOLPathExample

cdef class Trellis:
    cdef public int trellis_id
    cdef public list pc_rows
    cdef public list aa_columns
    cdef public list ss_columns
    cdef public dict node_dict
    cdef public dict scaled_coef_dict
    cdef public int n_groups
    cdef public Node first_node
    cdef public Node last_node

    cdef int _build_trellis(self, list pc_rows, list aa_columns, list ss_columns=*) except -1
    cdef tuple get_best_path(self, Node node, int window_length)
    cdef int calculate_trellis(self, int n_iterations, dict ff_dict, object is_testing=*) except -10
    cdef int set_golden_edges(self, list aligned_sequence, list marked_positions) except -1
    cdef long double _get_scaled_coef_multiplied(self)
    cdef int generate_examples(self, dict ff_dict, object neg_example_queue, bint is_classification=*) except -1
    cdef long double _get_best_path_log_probability(self)
    cdef long double _get_null_model_log_probability(self)
    cdef tuple get_avg_log_score(self)

cdef class Node:
    cdef public dict prev_edge_dict
    cdef public dict next_edge_dict
    cdef public long double best_forward_path_log_score
    cdef public long double best_backward_path_log_score
    cdef public Edge best_prev_edge
    cdef public Edge best_next_edge
    cdef public long double scaled_alpha_value
    cdef public long double alpha_value
    cdef public long double scaled_beta_value
    cdef public long double beta_value
    cdef public long double gamma_value
    cdef public int group_id
    cdef public int column
    cdef public int level
    cdef public int position
    cdef public unsigned long node_id
    cdef public bytes aa
    cdef public bytes ss
    cdef public bint is_aux_match
    cdef public bint is_in_queue

cdef class Edge:
    cdef public bytes edge_type
    cdef public Node prev_node
    cdef public Node next_node
    cdef public unsigned long edge_id
    cdef public bint tested_positive
    cdef public bint is_golden
    cdef public FOLPathExample example
    cdef public long double log_score
    cdef public long double xi_value



