
import MyGlobals
from MainClasses.FirstOrderLogic.FOLPathExample cimport FOLPathExample
from MainClasses.DynamicObjects.FieldFunction cimport FieldFunction


cdef class Trellis:
    def __cinit__(self, int trellis_id, list pc_rows, list aa_columns, list ss_columns, list aligned_sequence=None,
                  list marked_positions=None):
        self.trellis_id = trellis_id
        self.pc_rows = ['_'] + pc_rows
        self.aa_columns = ['_'] + aa_columns
        self.ss_columns = ['_'] + ss_columns
        self.node_dict = {}
        self.scaled_coef_dict = {0: 1.0}
        self.n_groups = 0
        self.first_node = None
        self.last_node = None
        self._build_trellis(self.pc_rows, self.aa_columns, self.ss_columns)
        if aligned_sequence and marked_positions:
            self.set_golden_edges(aligned_sequence, marked_positions)

    cdef int _build_trellis(self, list pc_rows, list aa_columns, list ss_columns = None) except -1:
        cdef int row, row_plus_column, max_column, min_column, position
        cdef int n_rows = len(pc_rows)
        cdef int n_columns = len(aa_columns)
        cdef int max_row_plus_column = (n_rows - 1) + (n_columns - 1)
        cdef bint is_aux_match
        cdef list positions, edge_type_prev_column_list
        cdef Node prev_node
        cdef Node node

        self.n_groups = n_rows

        if ss_columns is None:
            ss_columns = []

        if len(ss_columns) < n_columns:
            ss_columns += ['-'] * (n_columns - len(ss_columns))

        for row_plus_column in xrange(max_row_plus_column + 1):

            self.node_dict[row_plus_column] = {}
            min_column = max(0, row_plus_column - n_rows + 1)
            max_column = min(n_columns, row_plus_column + 1)

            for column in range(min_column, max_column):
                positions = [2 * column] + ([2 * column + 1] if column + 1 < max_column else [])
                row = row_plus_column - column

                for position in positions:
                    is_aux_match = position % 2

                    node = Node(self.trellis_id, row, column, row_plus_column, position, aa_columns[(position+1)/2],
                                ss_columns[(position+1)/2], is_aux_match)

                    self.node_dict[row_plus_column][position] = node

                    if row_plus_column == 0:
                        self.first_node = node
                        node.prev_edge_dict['begin'] = Edge('begin', None, node)
                        node.best_prev_edge = node.prev_edge_dict['begin']

                    else:
                        if is_aux_match:
                            edge_type_prev_column_list = [('m', position - 1)]
                        else:
                            edge_type_prev_column_list = [('d', position), ('m', position - 1), ('i', position - 2)]

                        for edge_type, prev_column in edge_type_prev_column_list:
                            if prev_column in self.node_dict[row_plus_column - 1]:
                                prev_node = self.node_dict[row_plus_column - 1][prev_column]
                                edge = Edge(edge_type, prev_node, node)
                                prev_node.next_edge_dict[edge_type] = edge
                                node.prev_edge_dict[edge_type] = edge

                        if row_plus_column == max_row_plus_column:
                            node.next_edge_dict['end'] = Edge('end', node, None)
                            node.best_next_edge = node.next_edge_dict['end']
                            self.last_node = node


        return 0

    cdef tuple get_best_path(self, Node node, int window_length):
        cdef int window_limit, group_id
        cdef bint is_first
        cdef Node current_node
        cdef Node next_node
        cdef Edge best_step_edge
        cdef bytes aa, ss, edge_type
        
        cdef list forward_path = []
        cdef list backward_path = []

        for step_towards_next, half_path in [(False, forward_path), (True, backward_path)]:
            current_node = node
            window_limit = (window_length+int(not step_towards_next))/2

            best_step_edge = current_node.best_next_edge if step_towards_next else current_node.best_prev_edge
            while window_limit > 0 and best_step_edge is not None:
                next_node = best_step_edge.next_node
                if not next_node or not next_node.is_aux_match:
                    edge_type = best_step_edge.edge_type

                    if next_node:
                        aa, ss = (next_node.aa, next_node.ss) if edge_type != 'd' else ('-', '-')
                        path_item = (edge_type + str(next_node.group_id), aa, ss)
                    else:
                        path_item = (edge_type, '_', '_')  #END STATE

                    if step_towards_next:
                        half_path.append(path_item)
                    else:
                        half_path.insert(0, path_item)
                    window_limit -= 1

                current_node = best_step_edge.next_node if step_towards_next else best_step_edge.prev_node
                if current_node:
                    best_step_edge = current_node.best_next_edge if step_towards_next else current_node.best_prev_edge
                else:
                    best_step_edge = None

            if step_towards_next:
                group_id = current_node.group_id
                is_first = True
                while window_limit > 0 and group_id < self.n_groups :
                    if group_id < self.n_groups - 1:
                        if is_first and not (current_node.best_prev_edge and current_node.best_prev_edge.edge_type == 'i'):
                            half_path.append(('i%d' % group_id, '_', '_'))
                            is_first = False
                        half_path.append(('m%d' % (group_id + 1), '_', '_'))
                        half_path.append(('d%d' % (group_id + 1), '_', '_'))
                    else:
                        half_path.append(('end', '_', '_'))
                    group_id += 1
                    window_limit -= 1

        return forward_path, backward_path

    cdef int calculate_trellis(self, int n_iterations, dict ff_dict, object is_testing=False) except -1:
        from math import exp

        cdef FieldFunction ff
        cdef int ff_group_id
        cdef int n_levels = len(self.node_dict)
        cdef bint gold_found
        cdef Node first_node = self.first_node
        cdef Node last_node = self.last_node
        cdef Node node, prev_node, next_node
        cdef Edge edge, best_step_edge
        cdef bytes edge_type
        cdef long double scaled_coef, path_log_score, best_path_log_score, edge_and_next_node_score, exp_edge_log_score
        cdef long double alpha_value_sum, scaled_alpha_value_sum
        cdef long double beta_value_sum, scaled_beta_value_sum
        cdef list forward_path, backward_path
        cdef dict node_dict_for_level

        for k in xrange(n_iterations):

            #------------------ forward algorithm -------------------#
            first_node.alpha_value = 1.0
            first_node.scaled_alpha_value = 1.0
            first_node.best_forward_path_log_score = 0.0
            first_node.best_prev_edge = first_node.prev_edge_dict['begin']

            for level in range(1, n_levels):                # ---> begins in level 1
                scaled_coef = 0.0
                node_dict_for_level = self.node_dict[level]
                for node in node_dict_for_level.values():
                    scaled_alpha_value_sum = 0.0
                    alpha_value_sum = 0.0
                    best_step_edge = None
                    best_path_log_score = -float('inf')
                    gold_found = False

                    for edge in node.prev_edge_dict.values():

                        if node.is_aux_match:
                            edge.log_score = 0.0
                        else:
                            node.best_prev_edge = edge
                            edge_type = edge.edge_type
                            forward_path, backward_path = self.get_best_path(node, MyGlobals.WINDOW_LENGTH)
                            ff_group_id = (edge_type != 'i') * 1000 + node.group_id
                            example = FOLPathExample(forward_path, backward_path, ff_group_id, edge.edge_id, edge.is_golden)
                            ff = ff_dict[edge_type=='i'][ff_group_id]
                            edge.tested_positive = ff.check_example(example)
                            edge.log_score = ff.calculate_ff(example, edge.tested_positive)
                            # print edge.tested_positive, edge.log_score
                            if is_testing:
                                example = None
                            else:
                                edge.example = example

                        prev_node = edge.prev_node
                        exp_edge_log_score = exp(edge.log_score)

                        path_log_score = prev_node.best_forward_path_log_score + edge.log_score

                        scaled_alpha_value_sum += prev_node.scaled_alpha_value * exp_edge_log_score
                        alpha_value_sum += prev_node.alpha_value * exp_edge_log_score

                        if edge.is_golden or (path_log_score >= best_path_log_score and not gold_found):
                            best_path_log_score = path_log_score
                            best_step_edge = edge
                            gold_found = edge.is_golden

                    node.best_forward_path_log_score = best_path_log_score
                    node.best_prev_edge = best_step_edge
                    best_step_edge.prev_node.best_next_edge = best_step_edge
                    node.alpha_value = alpha_value_sum
                    node.scaled_alpha_value = scaled_alpha_value_sum
                    scaled_coef += scaled_alpha_value_sum

                self.scaled_coef_dict[level] = scaled_coef

                node_dict_for_level = self.node_dict[level]
                for node in node_dict_for_level.values():
                    node.scaled_alpha_value /= scaled_coef

            #------------------ backward algorithm -------------------#
            last_node.beta_value = 1.0
            last_node.scaled_beta_value = 1.0 / self.scaled_coef_dict[n_levels-1]
            last_node.best_backward_path_log_score = 0.0
            last_node.best_next_edge = last_node.next_edge_dict['end']
            last_node.gamma_value = last_node.scaled_alpha_value * last_node.scaled_beta_value * self.scaled_coef_dict[n_levels-1]

            for level in range(n_levels-2, -1, -1):             #---> begins in level n-1 downward
                node_dict_for_level = self.node_dict[level]
                for node in node_dict_for_level.values():
                    scaled_beta_value_sum = 0.0
                    beta_value_sum = 0.0
                    best_step_edge = None
                    best_path_log_score = -float('inf')
                    gold_found = False

                    for edge in node.next_edge_dict.values():
                        next_node = edge.next_node

                        exp_edge_log_score = exp(edge.log_score)
                        edge_and_next_node_score = exp_edge_log_score * next_node.scaled_beta_value

                        path_log_score = edge.log_score + next_node.best_backward_path_log_score
                        edge.xi_value = node.scaled_alpha_value * edge_and_next_node_score

                        scaled_beta_value_sum += edge_and_next_node_score
                        beta_value_sum += exp_edge_log_score * next_node.beta_value

                        # another_xi = node.alpha_value * exp_edge_log_score  * next_node.beta_value / self.last_node.alpha_value
                        # print((edge.xi_value, another_xi))

                        if edge.is_golden or (path_log_score >= best_path_log_score and not gold_found):
                            best_step_edge = edge
                            best_path_log_score = path_log_score
                            gold_found = edge.is_golden

                    node.best_backward_path_log_score = best_path_log_score
                    node.best_next_edge = best_step_edge
                    node.beta_value = beta_value_sum
                    node.scaled_beta_value = scaled_beta_value_sum / self.scaled_coef_dict[level]
                    node.gamma_value = node.scaled_alpha_value * node.scaled_beta_value * self.scaled_coef_dict[level]

        return 0

    cdef int set_golden_edges(self, list aligned_sequence, list marked_positions) except -1:

        cdef Edge next_edge
        cdef Node node = self.first_node

        for i in xrange(len(aligned_sequence)):
            if int(marked_positions[i]):
                if aligned_sequence[i] == '-':
                    next_edge = node.next_edge_dict['d']
                else:
                    next_edge = node.next_edge_dict['m']
            elif aligned_sequence[i] != '-':
                next_edge = node.next_edge_dict['i']
            else:
                next_edge = None

            if next_edge:
                next_edge.is_golden = True
                node = next_edge.next_node

                if node.is_aux_match:
                    next_edge = node.next_edge_dict['m']
                    next_edge.is_golden = True
                    node = next_edge.next_node

        assert len(self.node_dict) == node.level + 1, "It has not finished in the last node."
        return 0

    cdef int generate_examples(self, dict ff_dict, object neg_example_queue, bint is_classification=False) except -1:
        from math import exp
        
        cdef FieldFunction ff
        cdef Node node, next_node
        cdef Edge prev_edge, next_edge, next_next_edge
        cdef list node_queue = []
        cdef dict neg_example_log_score_dict, neg_example_exp_score_dict, neg_example_count_dict
        cdef bytes edge_type
        cdef long double delta_value, log_average, exp_average
        cdef int count
        cdef bint is_insert, is_positive
        cdef tuple neg_example_item
        
        neg_example_log_score_dict = {0: {}, 1: {}}
        neg_example_exp_score_dict = {0: {}, 1: {}}
        neg_example_count_dict = {0: {}, 1: {}}

        node_queue.append(self.first_node)

        while node_queue:
            node = node_queue.pop(0)
            for prev_edge in node.prev_edge_dict.values():
                # if (not MyGlobals.USE_CLASSIFICATION_TREE or prev_edge.is_golden) and prev_edge.prev_node:
                if prev_edge.prev_node:
                    
                    edge_type = prev_edge.edge_type
                    is_insert = edge_type == 'i'
                    ff_group_id = (not is_insert) * 1000 + node.group_id
                    delta_value = MyGlobals.LEARNING_RATE * (prev_edge.is_golden - prev_edge.xi_value)

                    is_positive = prev_edge.is_golden if is_classification else prev_edge.tested_positive
                    if not MyGlobals.USE_CLASSIFICATION_TREE or is_positive:
                        prev_edge.example.set_value(delta_value)
                        ff = ff_dict[edge_type=='i'][ff_group_id]
                        ff.feed_example(prev_edge.example)
                    
                    else:
                        neg_example_count_dict[is_insert][ff_group_id] = \
                            neg_example_count_dict[is_insert].setdefault(ff_group_id, 0) + 1

                        neg_example_log_score_dict[is_insert][ff_group_id] = \
                            neg_example_log_score_dict[is_insert].setdefault(ff_group_id, 0.0) + delta_value 
                        
                        neg_example_exp_score_dict[is_insert][ff_group_id] = \
                            neg_example_exp_score_dict[is_insert].setdefault(ff_group_id, 0.0) + exp(delta_value) 

                    prev_edge.example = None

            for next_edge in node.next_edge_dict.values():
                # if not MyGlobals.USE_CLASSIFICATION_TREE or next_edge.is_golden:
                next_node = next_edge.next_node

                if next_node and next_node.is_aux_match:
                    next_next_edge = next_node.next_edge_dict['m']
                    next_node = next_next_edge.next_node

                if next_node and not next_node.is_in_queue:
                    node_queue.append(next_node)
                    next_node.is_in_queue = True

        if MyGlobals.USE_CLASSIFICATION_TREE:
            neg_example_item = (neg_example_count_dict, neg_example_log_score_dict, neg_example_exp_score_dict)
            neg_example_queue.put(neg_example_item)

        return 0

    cdef long double _get_scaled_coef_multiplied(self):
        cdef long double result = 1.0
        cdef int level

        for level in self.scaled_coef_dict:
            result *= self.scaled_coef_dict[level]

        return result

    cdef long double _get_best_path_log_probability(self):
        from math import log

        cdef Node node
        cdef Edge prev_edge
        cdef long double log_probability = 0.0

        node = self.last_node
        prev_edge = node.best_prev_edge

        while prev_edge:
            log_probability += (prev_edge.log_score - log(self.scaled_coef_dict[node.level]))
            node = prev_edge.prev_node
            prev_edge = node.best_prev_edge if node else None

        return log_probability

    cdef long double _get_null_model_log_probability(self):
        from math import log
        from MainClasses.Priors.BackgroundProbability import Q_prob

        cdef long double null_model_probability = 0.0

        for aa in self.aa_columns[1:]:
            null_model_probability += log(Q_prob[aa])

        return null_model_probability
    
    cdef tuple get_avg_log_score(self):
        cdef long double log_probability
        cdef long double avg_log_score
        cdef int length

        log_probability = self._get_best_path_log_probability()
        length = len(self.node_dict)
        avg_log_score = log_probability/length

        return length, avg_log_score
        

cdef class Node:

    def __cinit__(self, int trellis_id, int group_id, int column, int level, int position, bytes aa, bytes ss, bint is_aux_match):
        self.prev_edge_dict = {}
        self.next_edge_dict = {}
        self.best_prev_edge = None
        self.best_next_edge = None
        self.alpha_value = 1.0
        self.scaled_alpha_value = 1.0
        self.beta_value = 1.0
        self.scaled_beta_value = 1.0
        self.gamma_value = 1.0
        self.group_id = group_id
        self.column = column
        self.level = level
        self.position = position
        self.node_id = (<unsigned long>trellis_id * 10000 + <unsigned long>level) * 10000 + <unsigned long>position
        self.aa = aa
        self.ss = ss
        self.is_aux_match = is_aux_match
        self.best_forward_path_log_score = 0.0
        self.best_backward_path_log_score = 0.0
        self.is_in_queue = False


cdef class Edge:

    def __cinit__(self, bytes edge_type, Node prev_node, Node next_mode):
        self.edge_type = edge_type
        self.prev_node = prev_node
        self.next_node = next_mode
        self.tested_positive = False
        self.is_golden = False
        self.example = None
        self.edge_id = next_mode.node_id * 10 + <unsigned long>{'d': 0, 'm': 1, 'i': 2, 'begin': 3, 'end': 4}[edge_type]
        self.log_score = 0.0
        self.xi_value = 1.0