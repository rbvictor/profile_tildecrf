#!/usr/bin/env python

"""
Python Profile CRF Project - FieldFunction.py
"""

__author__ = "Romulo Barroso Victor"
__credits__ = ["Romulo Barroso Victor", "Gerson Zaverucha", "Juliana Bernardes"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Romulo Barroso Victor"
__email__ = "rbvictor@cos.ufrj.br"
__status__ = "Development"
__date__ = "10-Jul-2013"

import MyGlobals

cdef class FieldFunction:
    """The FieldFunction object stores the regression tree used to calculate the random field function value"""

    def __cinit__(self, int group_id, object rmode_manager=None, object default_value=None, str dao_suffix="",
                  bint has_single_tree=False, InitialFunction initial_value_function=None, str ff_type=None,
                  object keeps_init_func=None, object dao_work_manager_queue=None):

        # If the default value is set, the field function returns always the default value
        self.default_value = default_value
        self.dao_suffix = dao_suffix
        self.group_id = group_id
        self.rmode_manager = rmode_manager
        self.has_single_tree = has_single_tree
        self.decision_trees = []
        self.initial_value_function = <InitialFunction> initial_value_function
        self.keeps_init_func = bool(keeps_init_func) if keeps_init_func is not None else MyGlobals.FF_KEEPS_INIT_FUNC
        self.dao_work_manager_queue = dao_work_manager_queue

        assert not (self.has_single_tree and self.keeps_init_func), \
            "Field functions cannot have single tree and keep initial function at the same time."

        self.ff_type = ff_type
        self.min_variance = MyGlobals.INSERT_MIN_VARIANCE \
            if ff_type and ff_type[0].lower() == 'i' else MyGlobals.MIN_VARIANCE

        # First-Order Logic Decision Tree object, which is a regression tree that 'emulates' the random field function
        if self.default_value is None:
            self.new_tree = FOLDecisionTree(group_id=self.group_id, rmode_manager=rmode_manager,
                                            dao_suffix=self.dao_suffix, min_variance=self.min_variance)
        else:
            self.new_tree = None

        self.classification_tree = None
        self.negative_example_scores = []

    def __getstate__(self):
        state_dict = {
            "default_value": self.default_value,
            "dao_suffix": self.dao_suffix,
            "group_id": self.group_id,
            "rmode_manager": self.rmode_manager,
            "has_single_tree": self.has_single_tree,
            "decision_trees": self.decision_trees,
            "initial_value_function": self.initial_value_function,
            "keeps_init_func": self.keeps_init_func,
            "dao_work_manager_queue": self.dao_work_manager_queue,
            "ff_type": self.ff_type,
            "min_variance": self.min_variance,
            "new_tree": self.new_tree,
            "classification_tree" : self.classification_tree,
            "negative_example_scores" : self.negative_example_scores}
        return state_dict
    
    def __setstate__(self, state_dict):
        self.default_value = state_dict["default_value"]
        self.dao_suffix = state_dict["dao_suffix"]
        self.group_id = state_dict["group_id"]
        self.rmode_manager = state_dict["rmode_manager"]
        self.has_single_tree = state_dict["has_single_tree"]
        self.decision_trees = state_dict["decision_trees"]
        self.initial_value_function = state_dict["initial_value_function"]
        self.keeps_init_func = state_dict["keeps_init_func"]
        self.dao_work_manager_queue = state_dict["dao_work_manager_queue"]
        self.ff_type = state_dict["ff_type"]
        self.min_variance = state_dict["min_variance"]
        self.new_tree = state_dict["new_tree"]
        self.classification_tree = state_dict["classification_tree"]
        self.negative_example_scores = state_dict["negative_example_scores"]

    def __reduce__(self):
        init_args = (self.group_id,)
        return FieldFunction, init_args, self.__getstate__()

    cdef object check_example(self, FOLPathExample example):
        if self.classification_tree:
            return self.classification_tree.check_example(example)
        else:
            return True

    cdef long double calculate_ff(self, FOLPathExample example, object tested_positive=None) except 1e10:
        """This method returns the value of the random field function, given a first-order example.
        A first-order example is a set of first-order logic atoms. In this case, it describes a sequence of amino acids.
        """
        cdef FOLDecisionTree tree
        cdef long double initial_value, value
        cdef list tree_calculated_values

        if self.default_value is not None:
            #If the default value is set, then the function always returns this default value.
            return <float> self.default_value
        else:
            _tested_positive = True if self.classification_tree is None else \
                (self.classification_tree.check_example(example) if tested_positive is None else tested_positive)

            if _tested_positive:
                initial_value = 0.0
                value = 0.0

                ##debug-comment
                #print(">>>> BEGIN: %s | IS_INIT_VAL: %s | KEEPS_INIT_FUNC: %s | INIT_FUNC: %s"
                #      % (str(example.path_list[example.path_position]), is_initial_value, self.keeps_init_func,
                #         self.initial_value_function is not None))

                if self.keeps_init_func:
                    if self.initial_value_function is not None:
                        ##debug-comment
                        #print(">>>> END: %s" % str(example.path_list[example.path_position]))
                        initial_value = (<InitialFunction> self.initial_value_function).execute(example)

                if self.decision_trees:
                    #If there is no default value,
                    # the function value will be calculated using the regression tree of the object
                    tree_calculated_values = [tree.calculate_foldt(example) for tree in self.decision_trees]
                    value = sum(tree_calculated_values)

                print ("positive", example.is_golden, initial_value + value)
                return initial_value + value

            else:
                value = sum(self.negative_example_scores)

                print ("negative", example.is_golden, value)
                return value

    cdef int feed_example(self, FOLPathExample example, bint coverage_mode_on=False) except -1:
        """
        This method pushes a new first-order logic example into the FieldFunction list for the regression tree induction
        that will be triggered by self.update_function()
        """
        if self.group_id != example.group_id:
            raise Exception("Different group ID! Check FieldFunction.pyx ..")

        if self.default_value is None:
            if coverage_mode_on:
                self.classification_tree.feed_example(example, self.dao_work_manager_queue)
            else:
                self.new_tree.feed_example(example, self.dao_work_manager_queue)

        # elif is_last:
        #     MyDAO(suffix=self.dao_suffix).finish_example_queue()

        return 0

    cdef int update_classification_function(self) except -1:
        """ This method trigger the top-down induction for first-order logic decision trees for this field function.
        """

        if self.default_value is None:
            #When all examples are fed to the decision tree, the top-down induction is executed based on these examples.
            self.classification_tree = FOLClassificationTree(self.group_id, rmode_manager=self.rmode_manager,
                                                             dao_suffix=self.dao_suffix)

            self.classification_tree.induce_tree_from_root()

        return 0

    cdef int add_negative_score(self, long double negative_score) except -1:
        self.negative_example_scores.append(negative_score)
        return 0

    cdef int update_function(self) except -1:
        """ This method trigger the top-down induction for first-order logic decision trees for this field function.
        """
        cdef FOLDecisionTree tree

        if self.default_value is None:
            #When all examples are fed to the decision tree, the top-down induction is executed based on these examples.
            tree = self.new_tree

            tree.induce_tree_from_root(len(self.decision_trees))
            tree.clear_example_id_list()

            if self.has_single_tree:
                self.decision_trees = []

            self.decision_trees.append(tree)
            self.new_tree = FOLDecisionTree(group_id=self.group_id, rmode_manager=self.rmode_manager,
                                            dao_suffix=self.dao_suffix, min_variance=self.min_variance)

        return 0

    cdef int set_value_to_false_leaf(self, long double average, long double exp_average, long double variance=0.0) except -1:
        return (<FOLDecisionTree> self.new_tree).set_value_to_false_leaf(average, exp_average, variance)

    cdef int save_trees_to_db(self, int sf_id, bytes target_f_char_id) except -1:

        cdef FOLDecisionTree tree
        cdef int tree_index

        for tree_index, tree in enumerate(self.decision_trees):
            tree.save_tree_to_db(sf_id, target_f_char_id, self.group_id, tree_index)

        return 0

    cdef int save_last_tree_to_db(self, int sf_id, bytes target_f_char_id) except -1:

        cdef FOLDecisionTree tree
        cdef int tree_index

        tree_index = len(self.decision_trees) - 1
        tree = self.decision_trees[tree_index]
        tree.save_tree_to_db(sf_id, target_f_char_id, self.group_id, tree_index)

        return 0

    cdef int load_trees_from_db(self, bytes target_f_char_id, int num_trees) except -1:
        cdef FOLDecisionTree tree
        self.decision_trees = []

        for tree_index in xrange(num_trees):
            tree = FOLDecisionTree(group_id=self.group_id, rmode_manager=self.rmode_manager,
                                   dao_suffix=self.dao_suffix, min_variance=self.min_variance)

            if tree.load_tree_from_db(target_f_char_id, self.group_id, tree_index):
                self.decision_trees.append(tree)

        return 0