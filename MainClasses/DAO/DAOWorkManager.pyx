__author__ = 'rbvictor'

import warnings
import multiprocessing
import MyGlobals
import MySQLdb
import sqlite3


warnings.filterwarnings('ignore', category=Warning)


class DAOWorkManager(multiprocessing.Process):
    def __init__(self, num_queries_before_commit="", suffix="", host="", port="", db_name="", user="", passwd="",
                 input_queue=None):

        super(DAOWorkManager, self).__init__()

        self.host = host if host else MyGlobals.MYSQL_HOST
        self.port = port if port else MyGlobals.MYSQL_PORT
        self.db_name = db_name if db_name else MyGlobals.MYSQL_DBNAME
        self.user = user if user else MyGlobals.MYSQL_LOGIN
        self.passwd = passwd if passwd else MyGlobals.MYSQL_PASSWD
        self.num_queries_before_commit = (num_queries_before_commit if num_queries_before_commit
                                          else MyGlobals.NUM_OF_QUERIES_BEFORE_COMMIT)
        self.suffix = suffix
        self.use_parallel = MyGlobals.USE_MULTIPROCESSING and MyGlobals.USE_PP_IN_DAO
        self.use_separate_dbs = MyGlobals.USE_SQLITE3 and MyGlobals.USE_TABLE_NAME_WITH_GROUP_ID
        self.input_queue = input_queue
        self.job_count = 0

    def _is_numeric(self, obj):
        try:
            float(obj)
        except ValueError:
            return False
        return True

    def put(self, item):
        table_name, values = item

        if self.use_parallel:
            self.input_queue.put(item)
        else:
            str_group_id = self._get_str_group_id(table_name)
            conn, cursor = self._create_db_connection(str_group_id)

            delimiters = ["" if self._is_numeric(value) else "'" for value in values]
            str_values = ", ".join([delimiter + str(value) + delimiter
                                    for delimiter, value in zip(delimiters, values)])
            sql_query = "INSERT INTO %s VALUES (%s)" % (table_name, str_values)

            cursor.execute(sql_query)
            self.job_count += 1

            if self.job_count >= self.num_queries_before_commit or self.use_parallel:
                conn.commit()
                self.job_count = 0

    def run(self):
        cdef bint use_separate_dbs = self.use_separate_dbs
        cdef bint kill_received = False
        cdef dict dict_group_table_values = {}
        cdef dict table_values_dict
        cdef int job_count = 0
        cdef str table_name, key_table_name, str_group_id, key_str_group_id, value_types
        cdef tuple values
        cdef list list_of_values
        cdef object conn, cursor

        while not kill_received:
            table_name, values = self.input_queue.get()

            kill_received = not table_name and not values
            str_group_id = ""

            if use_separate_dbs:
                table_name_split = table_name.split("_gr_")
                if len(table_name_split) == 2:
                    str_group_id = table_name_split[1]

            if table_name:
                dict_group_table_values.setdefault(str_group_id, {}).setdefault(table_name, []).append(values)
                job_count += 1

            if job_count >= self.num_queries_before_commit or kill_received:
                job_count = 0
                for key_str_group_id in dict_group_table_values.keys():
                    conn = self._create_db_connection(key_str_group_id)
                    cursor = conn.cursor()
                    table_values_dict = dict_group_table_values.pop(key_str_group_id)

                    for key_table_name in table_values_dict.keys():
                        list_of_values = table_values_dict.pop(key_table_name)
                        value_types = ", ".join(['?' if MyGlobals.USE_SQLITE3 else """%s"""] * len(list_of_values[0]))
                        try:
                            cursor.executemany("insert into %s values (%s)" % (key_table_name, value_types), list_of_values)
                        except OverflowError, e:
                            print "insert into %s values (%s)" % (key_table_name, value_types)
                            print list_of_values
                            raise e

                    conn.commit()
                    cursor.close()
                    conn.close()

            self.input_queue.task_done()
            #end while

    def finish_jobs(self):
        if self.use_parallel:
            poison_pill = ("", ())
            self.input_queue.put(poison_pill)
        # else:
        #     self._close_db_connection()

    def _get_str_group_id(self, table_name):
        str_group_id = ""
        if self.use_separate_dbs:
            table_name_split = table_name.split("_gr_")
            if len(table_name_split) == 2:
                str_group_id = table_name_split[1]

        return str_group_id

    def _create_db_connection(self, str str_group_id=""):
        cdef object conn
        cdef str db_file

        if MyGlobals.USE_SQLITE3:
            db_file = MyGlobals.SQLITE3_DB_FILENAME % (MyGlobals.SQLITE3_DB_FOLDER, self.suffix, str_group_id)
            conn = sqlite3.connect(db_file, timeout=MyGlobals.SQLITE3_TIMEOUT)
            for pragma_statement in MyGlobals.SQLITE3_PRAGMAS:
                conn.execute(pragma_statement)
        else:
            conn = self._get_MySQLdb_connection()
        return conn

    def _get_MySQLdb_connection(self, num_attempts=None):
        try:
            conn = MySQLdb.connect(host=self.host, port=self.port, user=self.user, passwd=self.passwd, db=self.db_name)
            str_max_table_size = "1024 * 1024 * 1024 * %d" % MyGlobals.MAX_MYSQL_TABLE_SIZE_IN_GB
            conn.query("SET tmp_table_size = %s" % str_max_table_size)
            conn.query("SET max_heap_table_size = %s" % str_max_table_size)
            return conn

        except MySQLdb.OperationalError:
            if num_attempts is None:
                num_attempts = MyGlobals.NUM_MYSQL_CONNECT_ATTEMPTS

            if num_attempts > 0:
                from time import sleep

                sleep(MyGlobals.NUM_MYSQL_CONNECT_ATTEMPTS - num_attempts + 1)
                return self._get_MySQLdb_connection(num_attempts - 1)

            else:
                raise MySQLdb.OperationalError


# def _DAOWorkManager__consume_queue(mysql_credentials, queue, consumer_id, suffix):
#     import time
#     import MyGlobals
#
#     dict_group_table_values = {}
#     host, port, db_name, user, passwd = mysql_credentials
#
#     # db_conn.autocommit(False)
#     kill_received = False
#     insert_string = "INSERT INTO"
#     conn_dict = {}
#     use_separate_dbs = MyGlobals.USE_SQLITE3 and MyGlobals.USE_TABLE_NAME_WITH_GROUP_ID
#     sqlite3_db_folder = MyGlobals.SQLITE3_DB_FOLDER
#     sqlite3_db_filename = MyGlobals.SQLITE3_DB_FILENAME
#     sqlite3_timeout = MyGlobals.SQLITE3_TIMEOUT
#     max_mysql_table_size_in_gb = MyGlobals.MAX_MYSQL_TABLE_SIZE_IN_GB
#     num_mysql_connect_attempts = MyGlobals.NUM_MYSQL_CONNECT_ATTEMPTS
#     num_consumers = MyGlobals.NUM_OF_CONSUMERS_IN_DAO
#     str_group_id_list = []
#
#     while not kill_received:
#         table_name, values = queue.get()
#
#         str_group_id = ""
#         if use_separate_dbs:
#             table_name_split = table_name.split("_gr_")
#             if len(table_name_split) == 2:
#                 str_group_id = table_name_split[1]
#
#         if str_group_id not in conn_dict and (not use_separate_dbs or str_group_id):
#             db_file = sqlite3_db_filename % (sqlite3_db_folder, suffix, str_group_id)
#             for num_attempts in xrange(num_mysql_connect_attempts):
#                 try:
#                     if MyGlobals.USE_SQLITE3:
#                         conn = sqlite3.connect(db_file, timeout=sqlite3_timeout)
#                         for pragma_statement in MyGlobals.SQLITE3_PRAGMAS:
#                             conn.execute(pragma_statement)
#                     else:
#                         conn = MySQLdb.connect(host=host, port=port, user=user, passwd=passwd, db=db_name)
#                         str_max_table_size = "1024 * 1024 * 1024 * %d" % max_mysql_table_size_in_gb
#                         conn.query("SET tmp_table_size = %s" % str_max_table_size)
#                         conn.query("SET max_heap_table_size = %s" % str_max_table_size)
#                         close_connection = True
#                     break
#
#                 except MySQLdb.OperationalError as e:
#                     if num_attempts < num_mysql_connect_attempts:
#                         time.sleep(num_attempts + 2)
#                     else:
#                         raise e
#
#                 except sqlite3.OperationalError as e:
#                     if num_attempts < num_mysql_connect_attempts:
#                         time.sleep(num_attempts + 2)
#                     else:
#                         raise e
#
#             conn_dict[str_group_id] = conn
#             str_group_id_list.append(str_group_id)
#
#         if table_name and table_name.upper() != "COMMIT" :
#             dict_group_table_values.setdefault(str_group_id, {}).setdefault(table_name, []).append(values)
#         else:           # if table_name.upper() == "COMMIT" or kill_received:
#             kill_received = not table_name and not values
#             # print(str((table_name, values)))
#
#             for key_str_group_id in dict_group_table_values.keys():
#                 conn = conn_dict[key_str_group_id]
#                 cursor = conn.cursor()
#                 table_values_dict = dict_group_table_values.pop(key_str_group_id)
#
#                 for key_table_name in table_values_dict.keys():
#                     values = table_values_dict.pop(key_table_name)
#                     value_types = ", ".join(['?' if MyGlobals.USE_SQLITE3 else """%s"""] * len(values[0]))
#                     cursor.executemany("insert into " + key_table_name + " values ( " + value_types + " )", values)
#
#                 conn.commit()
#                 cursor.close()
#
#             #close sqlite connection
#             if len(str_group_id_list) > 2:
#                 conn_dict.pop(str_group_id_list.pop(0)).close()
#
#             #conn_keys = sorted(conn_dict.keys(), key=lambda x: int(x) % 1000)
#             #for key in range(0, min(0, min_group_id_key - 10)):
#             #    if key in conn_dict:
#             #        conn = conn_dict.pop(key)
#             #        conn.close()
#
#         queue.task_done()
#         #end while
#
#     for conn in conn_dict.values():
#         conn.close()



#if MyGlobals.DAO_CONSUMERS_IN_ROUND_ROBIN:
#    self.consumer_index = (self.consumer_index + 1) % MyGlobals.NUM_OF_CONSUMERS_IN_DAO
#
#    if self.job_count >= self.num_queries_before_commit:
#        self.consumer_queues[self.commit_index].put(("COMMIT", ""))
#        self.commit_index = (self.commit_index + 1) % MyGlobals.NUM_OF_CONSUMERS_IN_DAO
#        self.job_count = 0
