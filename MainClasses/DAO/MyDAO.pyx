#!/usr/bin/env python

"""
Python Profile CRF Project - MyDAO.py
"""
__author__ = "Romulo Barroso Victor"
__credits__ = ["Romulo Barroso Victor", "Gerson Zaverucha", "Juliana Bernardes"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Romulo Barroso Victor"
__email__ = "rbvictor@cos.ufrj.br"
__status__ = "Development"
__date__ = "10-Jul-2013"

#imports
import warnings
import MySQLdb
import sqlite3
from MySQLdb import cursors
from MySQLdb.cursors import DictCursor
import MyGlobals

#cimports
from MainClasses.FirstOrderLogic.FOLQuery cimport FOLQuery
from MainClasses.FirstOrderLogic.FOLPathExample cimport FOLPathExample

warnings.filterwarnings('ignore', category=Warning)

_current_dir = MyGlobals.get_current_dir()

class MyDAO:
    special_op_dict = dict({('eq', '='), ('=', '='), ('==', '='),
                            ('ne', '<>'), ('<>', '<>'), ('!=', '<>'),
                            ('gt', '>'), ('>', '>'), ('ge', '>='), ('>=', '>='),
                            ('lt', '<'), ('<', '<'), ('le', '<='), ('<=', '<=')})

    def __init__(self, suffix="", create_db_connection=True, host="", port="", db_name="", user="", passwd="",
                 sqlite3_in_memory=False, dao_work_manager_queue=None):

        self.host = host if host else MyGlobals.MYSQL_HOST
        self.port = port if port else MyGlobals.MYSQL_PORT
        self.db_name = db_name if db_name else MyGlobals.MYSQL_DBNAME
        self.user = user if user else MyGlobals.MYSQL_LOGIN
        self.passwd = passwd if passwd else MyGlobals.MYSQL_PASSWD
        self.sqlite3_in_memory = sqlite3_in_memory

        self.db_conn = None
        self.cursor = None

        self.lite_conn_dict = {}
        self.use_separate_dbs = MyGlobals.USE_SQLITE3 and MyGlobals.USE_TABLE_NAME_WITH_GROUP_ID

        if create_db_connection and not sqlite3_in_memory:
            self._create_db_connection()

        self.example_cursor = None
        self.suffix = suffix
        self.loaded_sequences_table_name = "LOADED_SEQUENCES"
        self.secondary_structures_table_name = "SECONDARY_STRUCTURES"
        self.aligned_sequences_table_name = "ALIGNED_SEQUENCES"
        self.hmmer_table_name = "HMMER_AUC_RESULTS"
        self.training_info_table_name = "TRAINING_INFO"
        self.results_table_name = "RESULTS"
        self.field_function_table_name = "FIELD_FUNCTIONS"
        self.value_table_name = "VALUE_TABLE"
        self.cache_table_list = None
        self.cache_lite_table_list_dict = {}
        self.indexed_tables = []
        self.dao_work_manager_queue = dao_work_manager_queue

    def __del__(self):
        self._close_db_connection()
        if MyGlobals.USE_SQLITE3 or self.sqlite3_in_memory:
            self._close_db_connection(lite=True)

    def _get_mysqldb_connection(self, num_attempts=MyGlobals.NUM_MYSQL_CONNECT_ATTEMPTS):
        try:
            conn = MySQLdb.connect(host=self.host, port=self.port, user=self.user, passwd=self.passwd, db=self.db_name)
            str_max_table_size = "1024 * 1024 * 1024 * %d" % MyGlobals.MAX_MYSQL_TABLE_SIZE_IN_GB
            conn.query("SET tmp_table_size = %s" % str_max_table_size)
            conn.query("SET max_heap_table_size = %s" % str_max_table_size)
            return conn

        except MySQLdb.OperationalError:
            if num_attempts > 0:
                from time import sleep

                sleep(MyGlobals.NUM_MYSQL_CONNECT_ATTEMPTS - num_attempts + 1)
                return self._get_mysqldb_connection(num_attempts - 1)

            else:
                raise MySQLdb.OperationalError

    def _get_str_group_id(self, table_name):
        str_group_id = ""
        if self.use_separate_dbs and "_gr_" in table_name:
            table_name_split = table_name.split("_gr_")
            if len(table_name_split) == 2:
                str_group_id = table_name_split[1]

        return str_group_id

    def _create_db_connection(self, lite=False, str_group_id=""):

        if (lite and MyGlobals.USE_SQLITE3) or self.sqlite3_in_memory:
            if str_group_id in self.lite_conn_dict:
                conn = self.lite_conn_dict[str_group_id]
            else:
                db_file = ":memory:" if self.sqlite3_in_memory \
                    else MyGlobals.SQLITE3_DB_FILENAME % (MyGlobals.SQLITE3_DB_FOLDER, self.suffix, str_group_id)

                conn = sqlite3.connect(db_file, timeout=MyGlobals.SQLITE3_TIMEOUT)
                for pragma_statement in MyGlobals.SQLITE3_PRAGMAS:
                    conn.execute(pragma_statement)

                self.lite_conn_dict[str_group_id] = conn

            return conn, conn.cursor()
        else:
            if self.db_conn is None:
                self.db_conn = self._get_mysqldb_connection()

            if self.cursor is None:
                self.cursor = self.db_conn.cursor(cursors.Cursor)

            return self.db_conn, self.cursor

    def _close_db_connection(self, lite=False):

        if lite:
            for lite_conn in self.lite_conn_dict.values():
                lite_conn.close()

            self.lite_conn_dict = {}

        else:
            if self.cursor is not None:
                self.cursor.close()
                self.cursor = None

            if self.db_conn is not None:
                self.db_conn.close()
                self.db_conn = None

            if self.example_cursor is not None:
                self.example_cursor.close()
                self.example_cursor = None

    def _is_connection_closed(self, lite=False):
        conn = self.lite_conn if lite and MyGlobals.USE_SQLITE3 else self.db_conn
        cursor = self.lite_cursor if lite and MyGlobals.USE_SQLITE3 else self.cursor

        return not cursor or not conn

    def _execute_and_return(self, sql_query, lite=False, str_group_id=""):
        num_attempts = MyGlobals.NUM_MYSQL_CONNECT_ATTEMPTS

        for i in xrange(num_attempts):
            try:
                conn, cursor = self._create_db_connection(lite, str_group_id)
                cursor.execute(sql_query)
                result_set = list(cursor.fetchall())
                return result_set

            except MySQLdb.OperationalError as e:
                if i < num_attempts-1:
                    self._create_db_connection(lite, str_group_id)
                    close_connection = True
                else:
                    raise MySQLdb.OperationalError("\n>>> Message: %s\n>>> SQL query: %s" % (e, sql_query))

            except sqlite3.OperationalError as e:
                if 'no such table' in e.message:
                    #print("** My Warning: %s, empty list returned **" % e)
                    return []

                else:
                    if i < num_attempts-1:
                        self._create_db_connection(lite, str_group_id)
                        close_connection = True
                    else:
                        raise sqlite3.OperationalError("\n>>> Message: %s\n>>> SQL query: %s" % (e, sql_query))

            except MySQLdb.ProgrammingError as e:
                raise MySQLdb.ProgrammingError("\n>>> Message: %s\n>>> SQL query: %s" % (e, sql_query))

            except sqlite3.ProgrammingError as e:
                raise sqlite3.ProgrammingError("\n>>> Message: %s\n>>> SQL query: %s" % (e, sql_query))

    def _execute(self, sql_query, with_commit=True, lite=False, str_group_id=""):
        num_attempts = MyGlobals.NUM_MYSQL_CONNECT_ATTEMPTS

        for i in xrange(num_attempts):
            try:
                conn, cursor = self._create_db_connection(lite, str_group_id)
                cursor.execute(sql_query)

                if with_commit:
                    conn.commit()

                return 0

            except MySQLdb.OperationalError as e:
                if i < num_attempts-1:
                    self._create_db_connection(lite, str_group_id)
                    close_connection = True
                else:
                    raise MySQLdb.OperationalError("\n>>> Message: %s\n>>> SQL query: %s" % (e, sql_query))

            except sqlite3.OperationalError as e:
                if i < num_attempts-1:
                    self._create_db_connection(lite, str_group_id)
                    close_connection = True
                else:
                    raise  sqlite3.OperationalError("\n>>> Message: %s\n>>> SQL query: %s" % (e, sql_query))

            except MySQLdb.ProgrammingError as e:
                raise MySQLdb.ProgrammingError("\n>>> Message: %s\n>>> SQL query: %s" % (e, sql_query))

            except sqlite3.ProgrammingError as e:
                raise sqlite3.ProgrammingError("\n>>> Message: %s\n>>> SQL query: %s" % (e, sql_query))

    def _executemany(self, table_name, insert_values, column_names="", with_commit=True):
        if self._is_connection_closed():
            self._create_db_connection()
            close_connection = True
        else:
            close_connection = False

        value_types = ", ".join(["""%s"""] * len(insert_values[0]))
        self.cursor.executemany("insert into " + table_name + " "+column_names+" values ( " + value_types + " )",
                                insert_values)

        if with_commit:
            self.cursor.execute("COMMIT")

        if close_connection:
            self._close_db_connection()

    def _get_table_list(self, lite=False, str_group_id=""):
        if lite and MyGlobals.USE_SQLITE3:
            if str_group_id not in self.cache_lite_table_list_dict:
                sql_query = "SELECT name FROM sqlite_master WHERE type='table' %s ORDER BY name" \
                            % (("AND name LIKE '%" + self.suffix + "%'") if self.suffix else "")
                table_list = [row[0] for row in self._execute_and_return(sql_query, lite=True, str_group_id=str_group_id)]
                self.cache_lite_table_list_dict[str_group_id] = table_list
            else:
                table_list = self.cache_lite_table_list_dict[str_group_id]
        else:
            if not self.cache_table_list:
                #save tables list into cache variable
                sql_query = "SHOW TABLES %s" % (("LIKE '%" + self.suffix + "%'") if self.suffix else "")
                self.cache_table_list = [row[0] for row in self._execute_and_return(sql_query, lite=False)]
            table_list = self.cache_table_list

        return table_list

    def drop_tables(self, suffix=None, lite=False):
        str_group_ids = self.lite_conn_dict.keys() if lite else [""]

        for str_group_id in str_group_ids:
            tables_to_drop = [table for table in self._get_table_list(lite, str_group_id)]

            if suffix:
                tables_to_drop = [table for table in tables_to_drop if suffix in table]

            for table in tables_to_drop:
                self._drop_index(table, lite=lite)
                self._execute("DROP TABLE IF EXISTS %s" % table, lite=lite, str_group_id=str_group_id)

        if lite and MyGlobals.USE_SQLITE3:
            self.cache_lite_table_list_dict = {}
        else:
            self.cache_table_list = None

    def _get_type_from_arg(self, arg):
        if isinstance(arg, int):
            return "INT"
        elif isinstance(arg, float):
            return "FLOAT"
        else:
            return "VARCHAR(30)"

    def _create_index(self, table_name, lite=False):

        if table_name not in self.indexed_tables:
            self.indexed_tables.append(table_name)

            str_if_not_exists = "IF NOT EXISTS" if lite else ""
            str_using_hash = "" if lite else "USING BTREE"
            create_group_id_index = not MyGlobals.USE_TABLE_NAME_WITH_GROUP_ID
            str_group_id = self._get_str_group_id(table_name)
            index_prefix = ("%s_" % table_name) if lite else ""

            #index has to be created in "group_id, id"

            sql_queries = ["CREATE INDEX %s %sid_index ON %s(id) %s"
                           % (str_if_not_exists, index_prefix, table_name, str_using_hash)]

            if create_group_id_index:
                sql_queries.append("CREATE INDEX %s %sgroup_id_index ON %s(group_id) %s"
                                   % (str_if_not_exists, index_prefix, table_name, str_using_hash))

            for sql_query in sql_queries:
                try:
                    #debug-comment#print("Creating index of table %s..." % table_name)
                    self._execute(sql_query, lite=lite, str_group_id=str_group_id)
                except MySQLdb.OperationalError:
                    pass

    def _drop_index(self, table_name, lite=False):

        try:
            self.indexed_tables.remove(table_name)
        except ValueError:
            pass

        #index has to be created in "group_id, id"
        str_if_exists = "IF EXISTS" if lite else ""
        str_on_table_name = "" if lite else "ON %s" % table_name
        drop_group_id_index = not MyGlobals.USE_TABLE_NAME_WITH_GROUP_ID
        str_group_id = self._get_str_group_id(table_name)
        index_prefix = ("%s_" % table_name) if lite else ""

        sql_queries = ["DROP INDEX %s %sid_index %s" % (str_if_exists, index_prefix, str_on_table_name)]

        if drop_group_id_index:
            sql_queries.append("DROP INDEX %s %sgroup_id_index %s" % (str_if_exists, index_prefix, str_on_table_name))

        for sql_query in sql_queries:
            try:
                self._execute(sql_query, lite=lite, str_group_id=str_group_id)
            except MySQLdb.OperationalError:
                pass

    def _create_table_if_not_exists(self, str table_name, tuple args):

        cdef bint use_sqlite3 = MyGlobals.USE_SQLITE3 or self.sqlite3_in_memory
        cdef str str_group_id = self._get_str_group_id(table_name)
        cdef list table_list = self._get_table_list(lite=use_sqlite3, str_group_id=str_group_id)
        cdef str str_engine, str_query, str_args

        if table_name not in table_list:
            #create table with columns based on args
            # only two types used: varchar(20) or int

            if use_sqlite3:
                str_engine = ""
                self.cache_lite_table_list_dict[str_group_id].append(table_name)
            else:
                str_engine = "ENGINE=MEMORY"
                self.cache_table_list = None

            sql_query = "CREATE TABLE IF NOT EXISTS %s " \
                        "(group_id BIGINT UNSIGNED, id BIGINT UNSIGNED, is_golden BOOLEAN, example_value DOUBLE" % table_name
            str_args = "".join([", arg%d %s" % (i, self._get_type_from_arg(args[i])) for i in xrange(0, len(args))])
            sql_query = "%s%s ) %s" % (sql_query, str_args, str_engine)

            self._execute(sql_query, lite=True, str_group_id=str_group_id)

    def _insert_values(self, str_group_id, table_name, values):
        conn, cursor = self._create_db_connection(str_group_id=str_group_id)
        delimiters = ["" if isinstance(value, int) or isinstance(value, float) or isinstance(value, long) else "'"
                      for value in values]
        str_values = ", ".join([delimiter + str(value) + delimiter
                                for delimiter, value in zip(delimiters, values)])
        sql_query = "INSERT INTO %s VALUES (%s)" % (table_name, str_values)
        cursor.execute(sql_query)

    def _save_atom_to_db(self, int group_id, unsigned long example_id, int is_golden, double example_value, tuple atom):

        # atom has format (table, [arg1, arg2, ...] )
        cdef str table_name = "%s%s" % (atom[0], self.suffix)

        if MyGlobals.USE_TABLE_NAME_WITH_GROUP_ID:
            table_name += "_gr_%d" % group_id

        cdef tuple args = atom[1]

        self._create_table_if_not_exists(table_name, args)

        #insert args as values into table
        # sql_query = "INSERT INTO %s values ( %d, %d" % (table, example.group_id, example.example_id)
        # str_args = "".join([(", %d" % int(arg) if isinstance(arg, int) else ", '%s'" % str(arg)) for arg in args])
        # sql_query = "%s%s )" % (sql_query, str_args)

        cdef tuple queue_item = (table_name, (group_id, example_id, is_golden, example_value) + tuple(args))
        cdef str str_group_id = str(group_id)
        cdef tuple values

        if self.sqlite3_in_memory:
            table_name, values = queue_item
            self._insert_values(str_group_id, table_name, values)
        else:
            self.dao_work_manager_queue.put(queue_item)

    # @staticmethod
    # def finish_example_queue():
    #     pass
    #     # if global_dao_work_manager:
    #     #     global_dao_work_manager.finish_jobs()
    #     #     del global_dao_work_manager

    def save_example_to_db(self, FOLPathExample example, bint atoms_with_don_t_care=False):

        # if not global_dao_work_manager and not self.sqlite3_in_memory:
        #     global_dao_work_manager = DAOWorkManager(MyGlobals.NUM_OF_QUERIES_BEFORE_COMMIT, self.suffix)
            #print("global_dao_work_manager created")

        #the example value (score) has to be saved into the database
        #the value_table is a standard table where we register the example id and
        # its real number used in the regression tree
        #a value is for a regression tree the same way a class is for a classification tree

        from math import isnan

        cdef double log_value = 0.0 if isnan(example.value) else float(example.value)
        cdef double exp_value = 0.0 if isnan(example.exp_value) else example.exp_value
        cdef double example_value = exp_value if MyGlobals.USE_EXP_VARIANCE else log_value
        cdef dict atom_dict = example.atom_dict
        cdef tuple atom_args
        cdef str value_table_name = self.value_table_name + self.suffix
        cdef str atom_name

        if MyGlobals.USE_TABLE_NAME_WITH_GROUP_ID:
            value_table_name += "_gr_%d" % example.group_id

        self._create_table_if_not_exists(value_table_name, (log_value,))
        cdef int group_id, is_golden
        cdef unsigned long example_id
        group_id, example_id, is_golden = example.group_id, example.example_id, int(example.is_golden)

        if self.dao_work_manager_queue and not self.sqlite3_in_memory:
            self.dao_work_manager_queue.put((value_table_name, (group_id, example_id, is_golden, exp_value, log_value)))

        #save every atom from the example atoms list into the database
        for atom_name in atom_dict:
            for atom_args in atom_dict[atom_name]:
                self._save_atom_to_db(group_id, example_id, is_golden, example_value, (atom_name, atom_args))

        # if is_last:
        #     self.finish_example_queue()

    #################################################################################################
    def _get_sql_table_query(self, group_id, example_id_list, fol_query, get_col_aliases=False, dao_suffix=None,
                             include_example_value=False):

        suffix = dao_suffix if dao_suffix is not None else self.suffix
        if MyGlobals.USE_TABLE_NAME_WITH_GROUP_ID:
            suffix += "_gr_%d" % group_id

        atom_list = (<FOLQuery> fol_query).atom_list
        n_atoms = len(atom_list)

        dict_arg_name_to_arg_pos = {atom_list[atom_index][1][arg_index]: (atom_index, arg_index)
                                          for atom_index in xrange(n_atoms - 1, -1, -1)
                                          for arg_index in xrange(len(atom_list[atom_index][1]) - 1, -1, -1)
                                          if str.isupper(str(atom_list[atom_index][1][arg_index])[0])}

        list_atom_pos_to_table_name = ["%s%s" % (atom_list[atom_index][0], suffix)
                                            for atom_index in xrange(n_atoms)]

        list_atom_pos_to_table_alias = ["%s_index%d" % (list_atom_pos_to_table_name[atom_index], atom_index)
                                             for atom_index in xrange(n_atoms)]

        list_arg_pos_to_column_alias = [["%s__arg%d" % (list_atom_pos_to_table_alias[atom_index], arg_index)
                                          for arg_index in xrange(len(atom_list[atom_index][1]))]
                                         for atom_index in xrange(n_atoms)]

        sql_query = ""
        all_column_aliases = []
        get_item = lambda my_list, position: my_list[position[0]][position[1]]

        for atom_index in xrange(n_atoms):
            atom_name, args = atom_list[atom_index]
            if atom_name not in self.special_op_dict:
                n_args = len(args)

                if get_col_aliases:
                    all_column_aliases.extend(list_arg_pos_to_column_alias[atom_index])

                table_name = list_atom_pos_to_table_name[atom_index]
                table_alias = list_atom_pos_to_table_alias[atom_index]

                column_clause = ',\n'.join(["arg%d AS %s"
                                            % (arg_index, list_arg_pos_to_column_alias[atom_index][arg_index])
                                            for arg_index in xrange(n_args)])

                group_where_clauses = ["WHERE group_id = %d" % group_id]
                if example_id_list is not None:
                    group_where_clauses.append("id IN (%s)"
                                               % (','.join([('' if i % 10 else '\n\t\t\t') + str(example_id_list[i])
                                                            for i in xrange(len(example_id_list))])))
                    
                #variable equalities
                #fixme: '_' does not always work if first variable is '_'
                where_clauses = ["(%s = %s OR %s = '_' OR %s = '_')"
                                 % (list_arg_pos_to_column_alias[atom_index][arg_index],
                                    get_item(list_arg_pos_to_column_alias, dict_arg_name_to_arg_pos[args[arg_index]]),
                                    list_arg_pos_to_column_alias[atom_index][arg_index],
                                    get_item(list_arg_pos_to_column_alias, dict_arg_name_to_arg_pos[args[arg_index]]))
                                 for arg_index in xrange(n_args)
                                 if args[arg_index] in dict_arg_name_to_arg_pos
                                    and dict_arg_name_to_arg_pos[args[arg_index]] != (atom_index, arg_index)]
                
                #constant equalities
                where_clauses.extend(["(%s = %s OR %s = '_')"
                                      % (list_arg_pos_to_column_alias[atom_index][arg_index],
                                         ("%d" % args[arg_index]) if isinstance(args[arg_index], int) or
                                                                     isinstance(args[arg_index], float) or 
                                                                     isinstance(args[arg_index], long) 
                                                                  else ("'%s'" % str(args[arg_index])),
                                         list_arg_pos_to_column_alias[atom_index][arg_index])
                                      for arg_index in xrange(n_args)
                                      if not str.isupper(str(args[arg_index])[0])])

                #special operations: >, <, !=, etc.
                sp_op_atom_index = atom_index + 1

                while sp_op_atom_index < n_atoms and atom_list[sp_op_atom_index][0] in self.special_op_dict:
                    sp_op_args = atom_list[sp_op_atom_index][1]
                    sp_op_symbol = self.special_op_dict[atom_list[sp_op_atom_index][0]]

                    sp_op_terms = \
                        [get_item(list_arg_pos_to_column_alias, dict_arg_name_to_arg_pos[sp_op_args[arg_index]])
                         if sp_op_args[arg_index] in dict_arg_name_to_arg_pos
                         else ("%d" % sp_op_args[arg_index] if isinstance(sp_op_args[arg_index], int) or
                                                               isinstance(sp_op_args[arg_index], float) or
                                                               isinstance(sp_op_args[arg_index], long)
                                                            else "'%s'" % sp_op_args[arg_index])
                         for arg_index in xrange(len(sp_op_args))]

                    sp_op_comparison = "(%s %s %s OR %s = '_' OR %s = '_')" \
                                       % (sp_op_terms[0], sp_op_symbol, sp_op_terms[1], sp_op_terms[0], sp_op_terms[1])

                    where_clauses.append(sp_op_comparison)
                    sp_op_atom_index += 1

                if where_clauses:
                    where_clauses[0] = "WHERE " + where_clauses[0]

                str_example_value = " is_golden, example_value," if include_example_value else ""

                new_sql_query = "(\n\tSELECT id,%s\n\t%s\n\tFROM %s\n\t%s) AS %s USING(id)\n%s" \
                                % (str_example_value, column_clause.replace('\n', '\n\t'), table_name,
                                   '\n\t  AND '.join(group_where_clauses), table_alias, '\n  AND '.join(where_clauses))

                if atom_index == 0:
                    sql_query = "\nSELECT * FROM %s" % new_sql_query.replace('USING(id)', '')
                else:
                    sql_query2 = str(sql_query).replace('\n', '\n\t')
                    sql_query = "SELECT * FROM (%s\n) AS T%d \nINNER JOIN %s" % (sql_query2, atom_index, new_sql_query)

        return (sql_query, all_column_aliases) if get_col_aliases else sql_query

    #################################################################################################

    def get_example_id_list(self, group_id):
        table_name = self.value_table_name + self.suffix
        str_group_id = ""
        if MyGlobals.USE_TABLE_NAME_WITH_GROUP_ID:
            table_name += "_gr_%d" % group_id
            str_group_id = str(group_id)
        sql_query = "SELECT DISTINCT id FROM %s WHERE group_id = %d ORDER BY id" % (table_name, group_id)
        results = self._execute_and_return(sql_query, lite=MyGlobals.USE_SQLITE3, str_group_id=str_group_id)
        return [] if len(results) == 0 else list(zip(*results)[0])

    def get_true_ids(self, group_id, example_id_list, fol_query):
        """
        This method returns a list with ids found in the result set of a query executed from the
        joined tables in table_list and filtered by the condition stated in sql_where_clause.
        """

        #concatenate the filtering condition stated in sql_where_clause
        sql_query = "SELECT id FROM ( %s \n) AS T" \
                    % self._get_sql_table_query(group_id, example_id_list, fol_query).replace("\n", "\n\t")

        #debug-comment#print ("_____ ok a.1.1 \tquery: %s\n_______________ sql query: \n%s" % (str(query.atom_list), sql_query))

        results = self._execute_and_return(sql_query, lite=MyGlobals.USE_SQLITE3, str_group_id=str(group_id))

        #debug-comment#print ("_____ ok a.1.2")
        return [] if len(results) == 0 else sorted(list(zip(*results)[0]))

    def get_result_set(self, group_id, example_id_list, fol_query):
        """
        This method returns a list with ids found in the result set of a query executed from the
        joined tables in table_list and filtered by the condition stated in sql_where_clause.
        """

        #concatenate the filtering condition stated in sql_where_clause
        sql_query = "SELECT * FROM ( %s \n) AS T" \
                    % self._get_sql_table_query(group_id, example_id_list, fol_query).replace("\n", "\n\t")

        return self._execute_and_return(sql_query, lite=(MyGlobals.USE_SQLITE3 or self.sqlite3_in_memory), 
                                        str_group_id=str(group_id))

    def get_list_from_const_gen(self, dao_suffix, group_id, example_id_list, max_examples, max_values_per_example,
                                var_names, FOLQuery gen_query, average=None, coverage_mode_on=False, max_coverage=1.0,
                                total_length=0, false_example_id_list=None):

        if not isinstance(var_names, list):
            var_names_list = list(var_names)
        else:
            var_names_list = var_names

        is_to_get_column_aliases = True
        sql_table_query, all_column_aliases = self._get_sql_table_query(group_id, example_id_list, gen_query,
                                                                        is_to_get_column_aliases, dao_suffix,
                                                                        include_example_value=True)

        if example_id_list:
            str_num_examples = str(len(example_id_list))
        else:
            value_table_name = self.value_table_name + dao_suffix
            str_num_examples = "count(0) FROM " + value_table_name + " WHERE group_id = " + str(group_id)

        column_list = \
            [all_column_aliases[(<FOLQuery> gen_query).get_var_first_position(var_name)] for var_name in var_names_list]

        str_columns = ', '.join(column_list)

        if coverage_mode_on:
            str_diff = ""

            for col in reversed(column_list):
                if "arg0" in col:
                    str_diff = " + (%s + unicode(%s)) * 1e-5 + SUM(id) * 1e-15 / COUNT(0)" % (str_diff, col)
                    break

            min_coverage = MyGlobals.MIN_COVERAGE
            
            if false_example_id_list:
                str_in_false_example_id_list = " (SUM(id IN (%s)) * 1.0 / %d) AS false_example_coverage," \
                                               % (','.join([str(false_example_id) for false_example_id in false_example_id_list]), 
                                                  len(false_example_id_list))
                str_where_false_example_coverage = " AND false_example_coverage > %f" % MyGlobals.MIN_FALSE_EXAMPLE_COVERAGE
                str_false_example_coverage_order = "false_example_coverage DESC, "
            else:
                str_in_false_example_id_list = ""
                str_where_false_example_coverage = ""
                str_false_example_coverage_order = ""

            sql_query = \
                "SELECT " + str_columns + " FROM (                                                                 \n" \
                "    SELECT (COUNT(0)*1.0/" + str(total_length) + str_diff + ") AS coverage,                       \n" \
                "           " + str_in_false_example_id_list + str_columns + "                                     \n" \
                "    FROM (" + sql_table_query.replace('\n', '\n\t\t\t\t') + ") AS A                               \n" \
                "    GROUP BY " + str_columns + "                                                                  \n" \
                "    HAVING coverage > " + str(min_coverage) + "                                                   \n" \
                "        AND coverage <= " + str(max_coverage) + " " + str_where_false_example_coverage + "        \n" \
                ") AS T                                                                                            \n" \
                "ORDER BY " + str_false_example_coverage_order + "coverage DESC                                    \n"

            # if "blosum" in sql_query:
            #     print sql_query

            fetched_const_list_per_var = \
                self._execute_and_return(sql_query, lite=MyGlobals.USE_SQLITE3, str_group_id=str(group_id))

            if fetched_const_list_per_var:
                fetched_const_list_per_var = fetched_const_list_per_var[:10]

            fetched_const_list_per_var_transposed = zip(*fetched_const_list_per_var)    # to be returned in the end

        else:
            if average is None:
                str_average = \
                    "AVG(example_value)    \n" \
                    "          FROM (SELECT id, example_value \n" \
                    "                FROM (" + sql_table_query.replace('\n', '\n\t\t\t\t') + ") AS TX \n" \
                    "                GROUP BY id) AS TY \n"
            else:
                str_average = str(average)

            str_avg_total = str_average
            str_n = str_num_examples
            str_n_false = "(%s - n_true)" % str_n
            str_avg_true_minus_total = "(sum_true/n_true - %s)" % str_avg_total
            str_avg_false_minus_total = "((%s * %s - sum_true)/%s - %s)" \
                                        % (str_n, str_avg_total, str_n_false, str_avg_total)

            str_ess = "(n_true * %s * %s + %s * %s * %s)" \
                      % (str_avg_true_minus_total, str_avg_true_minus_total,
                         str_n_false, str_avg_false_minus_total, str_avg_false_minus_total)

            sql_query = \
                "SELECT " + str_ess + " as ess,                                                 \n" \
                "       " + str_columns + "                                                     \n" \
                "FROM (SELECT COUNT(0) AS n_true,                                               \n" \
                "             SUM(example_value) AS sum_true, " + str_columns + "               \n" \
                "      FROM (" + sql_table_query.replace('\n', '\n\t\t\t\t') + ") AS A          \n" \
                "      GROUP BY " + str_columns + ") AS B                                       \n"
                # "ORDER BY ess DESC                                                          \n"

            #if group_id >= 1000:  # if match or delete state example
            #    sql_query +=  "WHERE sum_golden > 0"

            fetched_const_list_per_var = \
                self._execute_and_return(sql_query, lite=MyGlobals.USE_SQLITE3, str_group_id=str(group_id))

            if fetched_const_list_per_var:
                fetched_const_list_per_var = [max(fetched_const_list_per_var, key=lambda x: x[0])[1:]]

            fetched_const_list_per_var_transposed = zip(*fetched_const_list_per_var)

        return fetched_const_list_per_var_transposed

    def get_list_from_atom_name(self, group_id, atom_name, i, example_id_list=None):
        """This method returns a list of the distinct values within the column arg<i> of the table <atom_name>"""
        table = atom_name + self.suffix
        if MyGlobals.USE_TABLE_NAME_WITH_GROUP_ID:
            table += "_gr_%d" % group_id

        if example_id_list:
            sql_query = "SELECT arg%d FROM (SELECT DISTINCT arg%d FROM %s WHERE group_id = %d) AS T WHERE id IN (%s)" \
                        % (i, i, table, group_id, ','.join(example_id_list))
        else:
            sql_query = "SELECT DISTINCT arg%d FROM %s WHERE group_id = %d" % (i, table, group_id)

        return self._execute_and_return(sql_query, lite=MyGlobals.USE_SQLITE3, str_group_id=str(group_id))

    def calculate_avg_and_variance(self, group_id, example_id_list=None):
        """
        This method returns the average and the variance of the value column in value_table, given a list of example ids
        fortunately, mysql has already got built-in functions to calculate the average and variance of values within a
        column
        """
        if example_id_list is not None and len(example_id_list) == 0:
            return 0.0, 0.0, 0.0

        else:
            use_sqlite3 = MyGlobals.USE_SQLITE3
            value_table_name = self.value_table_name + self.suffix

            if MyGlobals.USE_TABLE_NAME_WITH_GROUP_ID:
                value_table_name += "_gr_%d" % group_id

            self._create_index(value_table_name, lite=use_sqlite3)

            str_example_ids = "" if example_id_list is None \
                else "AND id IN ( %s )" % (", ".join([str(example_id) for example_id in example_id_list]))

            str_sql_avg_of_squares = "AVG(example_value*example_value)" if MyGlobals.USE_EXP_VARIANCE else "AVG(arg0*arg0)"

            sql_query = "SELECT AVG(arg0), %s, AVG(example_value) \n" \
                        "FROM %s \n" \
                        "WHERE group_id = %d %s" \
                        % (str_sql_avg_of_squares, value_table_name, group_id, str_example_ids)

            #  File "MyDAO.pyx", line 627, in MainClasses.DAO.MyDAO.MyDAO.calculate_avg_and_variance (MainClasses/DAO/MyDAO.c:14961)
            #TypeError: unsupported operand type(s) for *: 'NoneType' and 'NoneType'
            avg, avg_of_squares, exp_average = \
                self._execute_and_return(sql_query, lite=use_sqlite3, str_group_id=str(group_id))[0]

            try:
                variance = avg_of_squares - ((exp_average * exp_average) if MyGlobals.USE_EXP_VARIANCE else (avg * avg))
            except Exception, e:
                print("Error - query: %s" % sql_query)
                raise e

            return avg, variance, exp_average

    def load_sequence_file_to_db(self, fasta_filename):

        self.cache_table_list = None
        self._execute("DROP TABLE IF EXISTS %s" % self.loaded_sequences_table_name)
        self._execute("CREATE TABLE %s (sf_char_id VARCHAR(20), f_char_id VARCHAR(20), char_id VARCHAR(20), "
                      "char_id2 VARCHAR(20), description VARCHAR(100), sf_id INT UNSIGNED, f_id INT UNSIGNED, "
                      "id BIGINT UNSIGNED, aa_string MEDIUMTEXT) ENGINE=InnoDB"
                      % self.loaded_sequences_table_name)

        line_num = 0

        f = open(fasta_filename, 'r')
        i = 0
        nb_lines_before_commit = 1000

        line = f.readline()

        while line:
            first_line = line.rstrip('\n')

            second_line = ""
            line = f.readline()

            while line and line[0] != '>':
                second_line += line.strip('\n').strip('\r')
                line = f.readline()

            line_num += 1
            seq_info = self._extract_substrings(first_line, second_line)
            sf_char_id, f_char_id, char_id, char_id2, description, sf_id, f_id, seq_id, aa_string = seq_info

            seq_id = line_num if seq_id is None or MyGlobals.USE_LINE_NUM_AS_SEQ_ID else seq_id

            sql_query = "INSERT INTO %s " \
                        "(sf_char_id, f_char_id, char_id, char_id2, description, sf_id, f_id, id, aa_string) " \
                        "values ('%s', '%s', '%s', '%s', '%s', %d, %d, %d, '%s')" \
                        % (self.loaded_sequences_table_name, sf_char_id, f_char_id, char_id, char_id2, description,
                           sf_id, f_id, seq_id, aa_string)

            self._execute(sql_query=sql_query, with_commit=False, lite=False)

            i = (i + 1) % nb_lines_before_commit
            if i == 0:
                self.db_conn.commit()

        self.db_conn.commit()
        f.close()

        #index has to be created in "sf_id" and "f_id"
        self._execute("CREATE INDEX sf_id_index ON %s(sf_id) USING BTREE" % self.loaded_sequences_table_name)
        self._execute("CREATE INDEX f_id_index ON %s(f_id) USING BTREE" % self.loaded_sequences_table_name)
        self._execute("CREATE INDEX char_id_index ON %s(char_id) USING BTREE" % self.loaded_sequences_table_name)
        self._execute("CREATE INDEX f_char_id_index ON %s(f_char_id) USING BTREE" % self.loaded_sequences_table_name)

    def _extract_substrings(self, first_line, second_line):
        split_line = first_line[1:].rstrip('\n').rstrip('\r').replace("'", "''").split(' ', 2)
        len_split_line = len(split_line)
        split_mul = 1000

        char_id = split_line[0] if len_split_line > 0 else ""
        description = split_line[2] if len_split_line > 2 else ""

        f_char_id_split = split_line[1].lower().split('.') if len_split_line > 1 else []
        len_f_char_id_split = len(f_char_id_split)
        assert len_f_char_id_split >= 4, "Error in splitting family id"

        sf_char_id = '.'.join(f_char_id_split[:3])
        f_char_id = '%s.%s' % (sf_char_id, f_char_id_split[3])
        char_id2 = '' if len_f_char_id_split < 5 else '%s.%s' % (f_char_id, f_char_id_split[4])

        first_number = (ord(f_char_id_split[0]) - ord('a') + 1) \
            if ord('a') <= ord(f_char_id_split[0]) <= ord('z') \
            else int(f_char_id_split[0])

        sf_id = (first_number * split_mul + int(f_char_id_split[1])) * split_mul + int(f_char_id_split[2])
        f_id = sf_id * split_mul + int(f_char_id_split[3])
        seq_id = f_id * split_mul + int(f_char_id_split[4]) if len_f_char_id_split > 4 else None

        aa_string = second_line.lower().strip('\n').strip('\r')

        return sf_char_id, f_char_id, char_id, char_id2, description, sf_id, f_id, seq_id, aa_string


    def load_alignments_to_db(self, run_hmmer=False):
        global _current_dir

        import os
        import shutil
        from MainClasses.RunCmd import RunCmd

        tmp_folder = "%s%s" % (_current_dir, MyGlobals.TMP_FOLDER)
        clustal_path = "%s%s" % (_current_dir, MyGlobals.CLUSTAL_PATH)
        clustal_input_filename_format = MyGlobals.CLUSTAL_INPUT_FILENAME_FORMAT
        clustal_output_filename_format = MyGlobals.CLUSTAL_OUTPUT_FILENAME_FORMAT
        hmmalign_output_filename_format = MyGlobals.HMMALIGN_OUTPUT_FILENAME_FORMAT
        use_hmmalign_outfile = MyGlobals.INITIAL_VALUE_FUNCTION_TYPE == 'hmm' or MyGlobals.ALWAYS_USE_HMMER_CONSENSUS

        if (not use_hmmalign_outfile) or run_hmmer:
            if os.path.exists(tmp_folder):
                shutil.rmtree(tmp_folder)
            os.makedirs(tmp_folder)

        sql_query = "SELECT DISTINCT target_f_char_id FROM %s " % self.training_info_table_name
        target_f_char_id_list = list(zip(*self._execute_and_return(sql_query))[0])

        self.cache_table_list = None
        self._execute("DROP TABLE IF EXISTS %s" % self.aligned_sequences_table_name)
        self._execute("CREATE TABLE %s (char_id VARCHAR(20), target_f_char_id VARCHAR(20), "
                      "aligned_aa_string MEDIUMTEXT, marked_positions MEDIUMTEXT) ENGINE=InnoDB"
                      % self.aligned_sequences_table_name)

        if run_hmmer:
            self._execute("DROP TABLE IF EXISTS %s" % self.hmmer_table_name)
            self._execute("CREATE TABLE %s (target_f_char_id VARCHAR(20), auc FLOAT) ENGINE=InnoDB"
                          % self.hmmer_table_name)

        for target_f_char_id in target_f_char_id_list:
            input_filename = tmp_folder + clustal_input_filename_format % target_f_char_id

            if not os.path.exists(input_filename):
                self._make_input_fasta_file(target_f_char_id, input_filename)

            hmmalign_output_filename = tmp_folder + hmmalign_output_filename_format % target_f_char_id

            if run_hmmer or (use_hmmalign_outfile and not os.path.exists(hmmalign_output_filename)):
                self._run_hmmer(target_f_char_id, input_filename)

            if use_hmmalign_outfile:
                self._load_output_aln_to_db(target_f_char_id, hmmalign_output_filename)

            else:
                output_filename = tmp_folder + clustal_output_filename_format % target_f_char_id
                cmd = "%s -i %s -t PROTEIN -o %s --outfmt=st --force -v" % (clustal_path, input_filename, output_filename)
                exec_is_successful = RunCmd(cmd).run_cmd()
                if exec_is_successful:
                    self._load_output_aln_to_db(target_f_char_id, output_filename)

        self._execute("CREATE INDEX target_f_char_id_index ON %s(target_f_char_id) USING BTREE"
                      % self.aligned_sequences_table_name)

        self._execute("CREATE INDEX char_id_index ON %s(char_id) USING BTREE" % self.aligned_sequences_table_name)

        if os.path.exists(tmp_folder) and not MyGlobals.KEEP_TMP_FILES:
            shutil.rmtree(tmp_folder)
            os.makedirs(tmp_folder)

    def _run_hmmer(self, target_f_char_id, input_filename):
        from MainClasses.RunCmd import RunCmd
        import MainClasses.AucRoc as AucRoc

        global _current_dir

        print("    >>>> Running hmmer for target family %s ..." % target_f_char_id)

        clustal_path = "%s%s" % (_current_dir, MyGlobals.CLUSTAL_PATH)
        tmp_folder = "%s%s" % (_current_dir, MyGlobals.TMP_FOLDER)
        hmmbuild_path = "%s%s/hmmbuild" % (_current_dir, MyGlobals.HMMER_PATH)
        hmmalign_path = "%s%s/hmmalign" % (_current_dir, MyGlobals.HMMER_PATH)
        hmmsearch_path = "%s%s/hmmsearch" % (_current_dir, MyGlobals.HMMER_PATH)
        run_cmd_timeout = 60

        #hmm_target_f_char_id = target_f_char_id.replace(".","_")

        hmmbuild_infile = tmp_folder + MyGlobals.HMMBUILD_INPUT_FILENAME_FORMAT % target_f_char_id
        hmmbuild_outfile = tmp_folder + MyGlobals.HMMBUILD_OUTPUT_FILENAME_FORMAT % target_f_char_id
        hmmalign_outfile = tmp_folder + MyGlobals.HMMALIGN_OUTPUT_FILENAME_FORMAT % target_f_char_id
        hmmsearch_infile = tmp_folder + MyGlobals.HMMSEARCH_INPUT_FILENAME_FORMAT % target_f_char_id
        hmmsearch_outfile = tmp_folder + MyGlobals.HMMSEARCH_OUTPUT_FILENAME_FORMAT % target_f_char_id

        hmm_clustal_cmd = "%s -i %s -t PROTEIN -o %s --outfmt=st --force" \
                          % (clustal_path, input_filename, hmmbuild_infile)
        RunCmd(hmm_clustal_cmd, run_cmd_timeout).run_cmd()

        hmmbuild_cmd = "%s --amino %s %s %s" % (hmmbuild_path,
                                                 "" if MyGlobals.USE_PRIORS_IN_HMMER else "--pnone --enone --wnone --fast",
                                                hmmbuild_outfile, hmmbuild_infile)
        RunCmd(hmmbuild_cmd, run_cmd_timeout).run_cmd()

        hmmalign_cmd = "%s --amino -o %s %s %s" % (hmmalign_path, hmmalign_outfile, hmmbuild_outfile, input_filename)
        RunCmd(hmmalign_cmd, run_cmd_timeout).run_cmd()

        cnt = self._make_input_fasta_file(target_f_char_id, hmmsearch_infile, [3, 4] )
        hmmsearch_cmd = "%s --max --noali -E inf --domE inf --incE inf --incdomE inf %s %s > %s" \
                         % (hmmsearch_path, hmmbuild_outfile, hmmsearch_infile, hmmsearch_outfile)

        RunCmd(hmmsearch_cmd, run_cmd_timeout).run_cmd()

        char_id_list = []

        prob_and_if_positive_list = []
        with open(hmmsearch_outfile, 'r') as f:
            for line in f:
                split_line = line.split()
                if len(split_line) > 0:
                    is_float, float_value = self._get_float(split_line[0])
                    if is_float:
                        is_positive = (split_line[9] == target_f_char_id)
                        char_id_list.append(split_line[8])
                        prob_and_if_positive_list.append((float_value, is_positive))
                    elif "inclusion threshold" in split_line[0]:
                        pass
                    elif len(prob_and_if_positive_list) > 0:
                        break
            f.close()

        last_prob = prob_and_if_positive_list[-1][0] + 1.0

        sql_query = "SELECT seq_type FROM %s                                                         \n" \
                     "WHERE target_f_char_id = '%s' AND seq_type IN (3, 4) AND char_id NOT IN ('%s') \n" \
                     "ORDER BY seq_type ASC" \
                     % (self.training_info_table_name, target_f_char_id, "', '".join(char_id_list))

        out_of_threshold_seq_list = self._execute_and_return(sql_query)

        for (seq_type,) in out_of_threshold_seq_list:
            prob_and_if_positive_list.append((last_prob, seq_type==3))

        hmmer_auc = AucRoc.calculate_auc(prob_and_if_positive_list, reversed_sort=False)
        self._execute("INSERT INTO %s VALUES ('%s', %f)" % (self.hmmer_table_name, target_f_char_id, hmmer_auc))

    @staticmethod
    def _get_float(n):
        try:
            value = float(n)
            return True, value
        except ValueError:
            return False, None

    def _make_input_fasta_file(self, target_f_char_id, filename, seq_type=1):

        str_seq_type = ("IN (%s)" % (", ".join([str(seq_t) for seq_t in seq_type]))) \
            if isinstance(seq_type, list) \
            else "= %d" % seq_type

        sql_query = "SELECT char_id, f_char_id, description, aa_string                    \n" \
                    "FROM " + self.loaded_sequences_table_name + "                        \n" \
                    "INNER JOIN (                                                         \n" \
                    "            SELECT char_id FROM " + self.training_info_table_name+"  \n" \
                    "            WHERE target_f_char_id = '" + target_f_char_id + "'      \n" \
                    "                  AND seq_type " + str_seq_type + ") AS T            \n" \
                    "USING(char_id)"

        sequence_info_list = self._execute_and_return(sql_query)

        file_string = ""
        for sequence_info in sequence_info_list:
            file_string += ">"+' '.join(sequence_info[0:3]) + "\n" + sequence_info[3] + "\n"

        with open(filename, 'w') as f:
            f.write(file_string)
            f.close()

        return len(sequence_info_list)

    def _load_output_aln_to_db(self, target_f_char_id, filename):
        alignment_dict = {}
        consensus_label = '#=GC RF'
        len_consensus_label = len(consensus_label)
        consensus_line = ""

        with open(filename, 'r') as f:
            line = f.readline()
            while line:
                split_line = line.strip('\n').lower().replace('.', '-').split()
                if len(split_line) == 2 and line[0] != '#':
                    char_id = split_line[0]
                    alignment_dict[char_id] = alignment_dict.setdefault(char_id, "") + split_line[1]
                elif line[:len_consensus_label] == consensus_label:
                    consensus_line += line[len_consensus_label+1:].strip()
                line = f.readline()
            f.close()

        if consensus_line:
            consensus_line = consensus_line.replace('x', '1').replace('.', '0')
        else:
            consensus_line = None

        insert_values = [(char_id, target_f_char_id, alignment_dict[char_id].lower(), consensus_line)
                         for char_id in alignment_dict]

        self._executemany(self.aligned_sequences_table_name, insert_values,
                          column_names="(char_id, target_f_char_id, aligned_aa_string, marked_positions)")

    @staticmethod
    def is_int(self, number):
        try:
            int(number)
            return True
        except ValueError:
            return False

    def _get_target_family_clause(self, target_family_id_list, use_equal_sign=False):
        sign = '=' if use_equal_sign else '<>'
        if target_family_id_list:
            target_family_clause = " AND ".join([("f_char_id %s '%s'" % (sign, str(target_family_id)))
                                                 if not self.is_int(target_family_id)
                                                 else ("f_id %s %d" % (sign, int(target_family_id)))
                                                 for target_family_id in target_family_id_list])
        else:
            target_family_clause = ""

        return target_family_clause

    def get_training_info_list(self, seq_type=1):
        sql_query = "SELECT sf_id, target_f_char_id FROM                                           \n" \
                    "(SELECT char_id, target_f_char_id FROM " + self.training_info_table_name + "  \n" \
                    "     WHERE seq_type = " + str(seq_type) + "                                   \n" \
                    "     GROUP BY target_f_char_id) AS A                                          \n" \
                    "INNER JOIN " + self.loaded_sequences_table_name + "                           \n" \
                    "USING(char_id)                                                                \n" \
                    "ORDER BY sf_id, target_f_char_id"

        return self._execute_and_return(sql_query)

    def get_testing_examples(self, target_f_char_id, seq_type=None):
        # seq_type = (0: not present; 1: positive train; 2: negative train;
        #             3: positive test; 4: negative test).

        seq_type = [3, 4] if seq_type is None else seq_type
        if isinstance(seq_type, list):
            str_seq_type = ', '.join([str(x) for x in seq_type])
        else:
            str_seq_type = str(seq_type)

        sql_query = "SELECT id, char_id, char_id2, f_char_id, aa_string, secstr FROM                      \n" \
                    "(SELECT * FROM                                                                       \n" \
                    "    (SELECT char_id, target_f_char_id FROM " + self.training_info_table_name + "     \n" \
                    "         WHERE target_f_char_id = '" + target_f_char_id + "'                         \n" \
                    "               AND seq_type in (" + str_seq_type + ")) AS A                          \n" \
                    "    INNER JOIN " + self.loaded_sequences_table_name + "                              \n" \
                    "    USING(char_id)                                                                   \n" \
                    ") AS C                                                                               \n" \
                    "LEFT JOIN " + self.secondary_structures_table_name + "                               \n" \
                    "USING(char_id)                                                                       \n" \
                    "ORDER BY (f_char_id = target_f_char_id) DESC, LENGTH(aa_string) DESC"

        return self._execute_and_return(sql_query)

    def get_super_family_average_length(self, sf_id, target_f_char_id=None):
        target_family_clause = "" if target_f_char_id is None else ("AND f_char_id <> '%s'" % target_f_char_id)

        sql_query = "SELECT ROUND(AVG(LENGTH(aa_string))) AS nb_match_states\n" \
                    "FROM %s \n" \
                    "WHERE sf_id = %d %s" \
                    % (self.loaded_sequences_table_name, sf_id, target_family_clause)

        return self._execute_and_return(sql_query)

    def select_super_family_sequences(self, sf_id, target_f_char_id=None):

        if not self.example_cursor:
            self.example_cursor = self.db_conn.cursor(DictCursor)

        family_clause = "" if target_f_char_id is None \
            else (" AND f_char_id <> '" + target_f_char_id + "'")
        target_family_clause = "" if target_f_char_id is None \
            else (" WHERE target_f_char_id = '" + target_f_char_id + "'")

        sql_query = "SELECT 1 as is_positive, sf_id, f_char_id, f_id, id, aa_string, aligned_aa_string, secstr  \n" \
                    "FROM (                                                                                     \n" \
                    "   SELECT * FROM                                                                           \n" \
                    "       (SELECT * FROM %s WHERE sf_id = %d %s) AS A                                         \n" \
                    "       INNER JOIN                                                                          \n" \
                    "       (SELECT * FROM %s %s) AS B                                                          \n" \
                    "   USING(char_id)                                                                          \n" \
                    ") AS C                                                                                     \n" \
                    "LEFT JOIN %s USING(char_id)                                                               \n" \
                    "ORDER BY RAND()                                                                              " \
                    % (self.loaded_sequences_table_name, sf_id, family_clause,
                       self.aligned_sequences_table_name, target_family_clause, self.secondary_structures_table_name)

        self.example_cursor.execute(sql_query)

    # def select_family_sequences(self, f_id):
    #     if not self.example_cursor:
    #         self.example_cursor = self.db_conn.cursor(DictCursor)
    #
    #     sql_query = "SELECT sf_id, f_id, char_id, id, aa_string \nFROM %s \nWHERE %s \n" \
    #                 "ORDER BY RAND()" \
    #                 % (self.loaded_sequences_table_name, self._get_target_family_clause([f_id], use_equal_sign=True))
    #
    #     self.example_cursor.execute(sql_query)

    def fetch_next_sequence(self):
        return self.example_cursor.fetchone()

    def fetch_all_sequences(self):
        return list(self.example_cursor.fetchall())

    def _get_dict_of_chars(self):
        list_of_chars = [chr(i) for i in xrange(ord('0'), ord('9') + 1)] + \
                        [chr(i) for i in xrange(ord('a'), ord('z') + 1)] + ['_']
        return dict(zip(list_of_chars, [i for i in xrange(0, len(list_of_chars) + 1)]))

    def get_super_family_aligned_sequences(self, sf_id, target_f_char_id, seq_type=1):

        sql_query = "SELECT aligned_aa_string, marked_positions FROM " \
                    "(SELECT char_id, aligned_aa_string, marked_positions FROM %s WHERE target_f_char_id = '%s') AS A " \
                    "INNER JOIN " \
                    "(SELECT char_id, sf_id FROM %s WHERE sf_id = %d) AS B " \
                    "USING(char_id)" \
                    % (self.aligned_sequences_table_name, target_f_char_id, self.loaded_sequences_table_name, sf_id)

        return self._execute_and_return(sql_query)

    def load_ss_file_to_db(self, filename):
        self.cache_table_list = None
        self._execute("DROP TABLE IF EXISTS %s" % self.secondary_structures_table_name)
        self._execute("CREATE TABLE %s (char_id VARCHAR(20), secstr MEDIUMTEXT) "
                      "ENGINE=InnoDB"
                      % self.secondary_structures_table_name)

        insert_values = []

        with open(filename, 'r') as f:
            for line in f:
                line_split = line.split()
                if len(line_split) == 2:
                    char_id, secstr = line_split
                    insert_values.append((char_id, secstr))
            f.close()

        self._executemany(self.secondary_structures_table_name, insert_values,
                          column_names="(char_id, secstr)",  with_commit=False)

        self._execute("CREATE INDEX char_id_index ON %s(char_id) USING BTREE" % self.secondary_structures_table_name)

    def load_training_info_table(self, filename):
        self.cache_table_list = None
        self._execute("DROP TABLE IF EXISTS %s" % self.training_info_table_name)
        self._execute("CREATE TABLE %s (char_id VARCHAR(20), target_f_char_id VARCHAR(20), seq_type TINYINT UNSIGNED) "
                      "ENGINE=InnoDB"
                      % self.training_info_table_name)
                    # seq_type = (0: not present; 1: positive train; 2: negative train;
                    #             3: positive test; 4: negative test).

        line_num = 0

        f = open(filename, 'r')
        i = 0
        nb_lines_before_commit = 1000

        line = f.readline().strip('\n').strip('\r')
        #header
        target_family_list = line.replace('\t', ' ').split()
        n_target_families = len(target_family_list)

        line = f.readline().strip('\n').strip('\r')

        while line:
            line_split = line.replace('\t', ' ').split()
            insert_values = zip([line_split[0]]*n_target_families, target_family_list[1:], line_split[1:])
            self._executemany(self.training_info_table_name, insert_values,
                              column_names="(char_id, target_f_char_id, seq_type)",  with_commit=False)

            i = (i + 1) % nb_lines_before_commit
            if i == 0:
                self.db_conn.commit()

            line_num += 1
            line = f.readline().strip('\n').strip('\r')

        self.db_conn.commit()
        f.close()

        self._execute("CREATE INDEX target_f_char_id_index ON %s(target_f_char_id) USING BTREE"
                      % self.training_info_table_name)
        self._execute("CREATE INDEX char_id_index ON %s(char_id) USING BTREE" % self.training_info_table_name)

    def create_field_function_table(self):
        self.cache_table_list = None
        self._execute("DROP TABLE IF EXISTS %s" % self.field_function_table_name)
        self._execute("CREATE TABLE %s (sf_id BIGINT, target_f_char_id VARCHAR(20), group_id BIGINT UNSIGNED, tree_index INT, "
                      "tree_json_dump LONGTEXT) ENGINE=InnoDB"
                      % self.field_function_table_name)

    def create_results_table(self):
        self.cache_table_list = None
        self._execute("DROP TABLE IF EXISTS %s" % self.results_table_name)
        self._execute("CREATE TABLE IF NOT EXISTS %s "
                      "(iter_num INT,"
                      " seq_char_id VARCHAR(20), "
                      " f_char_id VARCHAR(20), "
                      " target_f_char_id VARCHAR(20), "
                      " score DOUBLE, "
                      " path_length INT) ENGINE=InnoDB"
                      % self.results_table_name)

    def save_tree_to_db(self, sf_id, target_f_char_id, group_id, tree_index, tree_json_dump):
        #"ON DUPLICATE KEY UPDATE tree_json_dump = '%s'"
        self._execute("INSERT INTO %s VALUES (%d, '%s', %d, %d, '%s') \n"
                      % (self.field_function_table_name, sf_id, target_f_char_id, group_id, tree_index, tree_json_dump))

    def save_testing_result(self, iteration_count, result):
        (seq_char_id, f_char_id, target_f_char_id), (score, path_length, col_row_prob_list) = result
        self._execute("INSERT INTO %s (iter_num, seq_char_id, f_char_id, target_f_char_id, score, path_length) \n"
                      "VALUES ('%d', '%s', '%s', '%s', %s, %d)"
                      % (self.results_table_name,
                         iteration_count, seq_char_id, f_char_id, target_f_char_id, score, path_length))

    def get_saved_trees_info_from_db(self):
        return self._execute_and_return("SELECT sf_id, target_f_char_id, \n"
                                        "       (MAX(group_id) %% 1000) + 1 AS num_match_states, \n"
                                        "       MAX(tree_index) + 1 AS num_trees \n"
                                        "FROM %s GROUP BY sf_id, target_f_char_id" % self.field_function_table_name)

    def load_tree_from_db(self, target_f_char_id, group_id, tree_index):
        cdef str sql_query = "SELECT tree_json_dump FROM %s WHERE target_f_char_id = '%s' \n" \
                             "AND group_id = %d AND tree_index = %d" \
                             % (self.field_function_table_name, target_f_char_id, group_id, tree_index)
        return self._execute_and_return(sql_query)[0][0]

    # def get_saved_super_families_contents(self):
    #     sql_query = "SELECT sf_id, count(group_id) AS nb_match_states\n FROM %s " \
    #                 "\nWHERE group_id >= 1000 \nGROUP BY sf_id ORDER BY sf_id " % self.field_function_table_name
    #     return self._execute_and_return(sql_query)

#if MyGlobals.USE_BAUM_WELCH:
#    sql_query = "SELECT 1 as is_positive, sf_id, f_char_id, f_id, id, aa_string, NULL as aligned_aa_string \n" \
#                "FROM (SELECT * FROM %s WHERE sf_id = %d %s) AS A                                          \n" \
#                "INNER JOIN                                                                                \n" \
#                "(SELECT * FROM %s WHERE seq_type = 1 %s) AS B                                             \n" \
#                "USING(char_id)                                                                            \n" \
#                "ORDER BY RAND()" \
#                % (self.loaded_sequences_table_name, sf_id, family_clause,
#                   self.training_info_table_name, target_family_clause.replace('WHERE', 'AND'))
#else:


    #def create_index_of_all_tables(self, lite=False):
    #    print("Creating index of all tables...")
    #    import time
    #    if lite and MyGlobals.USE_SQLITE3:
    #        for str_group_id in self.lite_conn_dict:
    #            for table_name in self._get_table_list(lite=True, str_group_id=str_group_id):
    #                self._create_index(table_name, lite)
    #                time.sleep(1)
    #    else:
    #        for table_name in self._get_table_list(lite=False):
    #            self._create_index(table_name, lite)