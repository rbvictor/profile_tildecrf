#!/usr/bin/env python

"""
Python Profile CRF Project - Query.py
"""

__author__ = "Romulo Barroso Victor"
__credits__ = ["Romulo Barroso Victor", "Gerson Zaverucha", "Juliana Bernardes"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Romulo Barroso Victor"
__email__ = "rbvictor@cos.ufrj.br"
__status__ = "Development"
__date__ = "10-Jul-2013"


cdef class FOLQuery:
    """Class Query: a Query object contains a list of atoms to be used in the decision tree.
    This query determines if an example is proved or not in a decision tree node.
    """

    def __cinit__(self, list atom_list, bint convert_utf_to_str=False):
        """Query constructor"""
        self.atom_list = atom_list[:]

        if convert_utf_to_str:
            self.atom_list = [(str(atom_name) if isinstance(atom_name, unicode) else atom_name,
                               tuple([str(arg) if isinstance(arg, unicode) else arg for arg in args]))
                              for atom_name, args in self.atom_list]

    def __getstate__(self):
        state_dict = {"atom_list": self.atom_list}
        return state_dict

    def __setstate__(self, state_dict):
        self.atom_list = state_dict["atom_list"]

    def __reduce__(self):
        init_args = (self.atom_list,)
        return FOLQuery, init_args, self.__getstate__()

    cdef FOLQuery clone(self):
        """This method produces an object with same atom_list and max_var_id as the self object
        :rtype: Query"""
        return FOLQuery(self.atom_list)

    cdef bint has_atom(self, tuple atom):
        return atom in self.atom_list

    cdef void insert_atoms(self, list atoms):
        """This method inserts a new atom to the query.
        The atom has the format ('atom_name',[arg1, arg2, ...]).
        :rtype: None
        """
        _atoms = [a for a in (list(atoms) if not isinstance(atoms, list) else atoms) if a not in self.atom_list]
        self.atom_list.extend(_atoms)

    cdef int get_var_first_position(self, object var_name):
        cdef int n_all_atoms_args, n_args, i
        cdef list positions_found

        n_all_atoms_args = 0

        for atom in self.atom_list:
            atom_name, args = atom
            n_args = len(args)

            positions_found = [i for i in xrange(n_args) if var_name == args[i]]
            if positions_found:
                 return positions_found[0] + n_all_atoms_args

            n_all_atoms_args += n_args

        #every possibility has been checked and var_name has not been found
        return -1
