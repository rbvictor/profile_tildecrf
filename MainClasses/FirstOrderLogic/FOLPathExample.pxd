"""
Python Profile CRF Project - FirstOrderLogicExample.py
"""

__author__ = "Romulo Barroso Victor"
__credits__ = ["Romulo Barroso Victor", "Gerson Zaverucha", "Juliana Bernardes"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Romulo Barroso Victor"
__email__ = "rbvictor@cos.ufrj.br"
__status__ = "Development"
__date__ = "10-Jul-2013"

from MainClasses.FirstOrderLogic.FOLQuery cimport FOLQuery

cdef class FOLPathExample:

    cdef public list path_list
    cdef public int path_position
    cdef public dict atom_dict
    cdef public int group_id
    cdef public unsigned long example_id
    cdef public bint is_golden
    cdef public float value
    cdef public float exp_value
    cdef public int window_length
    cdef public bint only_match_states

    cdef int _build_atoms(self) except -1
    cdef void set_value(self, float value)
    cdef tuple check_query(self, FOLQuery query)
    cdef bint check_special_op(self, bytes atom_name, object arg_a, object arg_b)
    cdef list get_result_set_from_query(self, FOLQuery query)
    cdef list _get_new_facts_from_aa_types(self)
    cdef list _get_new_facts_from_blosum62(self)
    cdef list _get_new_facts_from_blocks9(self)
