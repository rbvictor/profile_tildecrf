"""
Python Profile CRF Project - FirstOrderLogicExample.py
"""
__author__ = "Romulo Barroso Victor"
__credits__ = ["Romulo Barroso Victor", "Gerson Zaverucha", "Juliana Bernardes"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Romulo Barroso Victor"
__email__ = "rbvictor@cos.ufrj.br"
__status__ = "Development"
__date__ = "10-Jul-2013"


import MyGlobals

special_op_dict = dict({('eq', '=='), ('=', '=='), ('==', '=='),
                        ('ne', '!='), ('<>', '!='), ('!=', '!='),
                        ('gt', '>'), ('>', '>'),
                        ('ge', '>='), ('>=', '>='),
                        ('lt', '<'), ('<', '<'),
                        ('le', '<='), ('<=', '<=')})


cdef class FOLPathExample:

    def __cinit__(self, list forward_path, list backward_path, int group_id, unsigned long example_id, bint is_golden,
                  float value=0.0, int window_length=5, bint only_match_states=True):

        from math import exp
        self.atom_dict = {}
        self.group_id = group_id
        self.example_id = example_id
        self.is_golden = is_golden
        self.value = value
        self.exp_value = exp(value)
        self.window_length = window_length
        self.only_match_states = only_match_states
        self.path_list = forward_path + backward_path
        self.path_position = len(forward_path) - 1

        if self.path_list:
            self._build_atoms()
        
    def __getstate__(self):
        state_dict = {"atom_dict": self.atom_dict,
                      "group_id": self.group_id,
                      "example_id": self.example_id,
                      "is_golden": self.is_golden,
                      "value": self.value,
                      "exp_value": self.exp_value,
                      "path_list": self.path_list,
                      "path_position": self.path_position,
                      "window_length": self.window_length,
                      "only_match_states": self.only_match_states
                     }
        return state_dict
    
    def __setstate__(self, state_dict):
        self.atom_dict = state_dict["atom_dict"]
        self.group_id = state_dict["group_id"]
        self.example_id = state_dict["example_id"]
        self.is_golden = state_dict["is_golden"]
        self.value = state_dict["value"]
        self.exp_value = state_dict["exp_value"]
        self.path_list = state_dict["path_list"]
        self.path_position = state_dict["path_position"]
        self.window_length = state_dict["window_length"]
        self.only_match_states = state_dict["only_match_states"]

    def __reduce__(self):
        init_args = (self.path_list, self.group_id, self.example_id, self.is_golden)
        return FOLPathExample, init_args, self.__getstate__()

    cdef void set_value(self, float value):
        from math import exp
        self.value = value
        self.exp_value = exp(value)

    cdef int _build_atoms(self) except -1:
        cdef bint only_match_states = self.only_match_states
        cdef list aa_list, ss_list
        cdef tuple aa_pos_list, aa_cl_list, ss_pos_list, ss_cl_list
        cdef bytes aa_index, aa, cl, ss
        cdef int i

        # if use_position:
        #     aa_pos_list, ss_pos_list = \
        #         zip(*[[("p%+d" % (i - pos), aa), ("p%+d" % (i - pos), ss)]
        #               for i, (aa, ss, cl, cl_i) in enumerate(self.path_list)
        #               if abs(i - pos) <= half_window_length])
        # else:
        #     aa_pos_list, ss_pos_list = (), ()

        aa_pos_list, ss_pos_list = (), ()
        aa_cl_list, ss_cl_list = zip(*[[(cl, aa), (cl, ss)] for i, (cl, aa, ss) in enumerate(self.path_list)])

        aa_list = list(aa_pos_list) + list(set(aa_cl_list))
        ss_list = [(ss_pos_or_cl, ss) for ss_pos_or_cl, ss in list(ss_pos_list) + list(set(ss_cl_list)) if ss != '-']

        cdef dict derived_aa_dict = {'x': ['_'], 'b': ['n', 'd'], 'z': ['e', 'q'], 'j': ['i', 'l']}
        aa_list.extend([(aa_index, derived_aa)
                        for aa_index, aa in aa_list if aa in derived_aa_dict
                        for derived_aa in derived_aa_dict[aa]])

        cdef int len_path_list = len(self.path_list)
        cdef list don_t_care_atom_args

        self.atom_dict["aa"] = aa_list
        self.atom_dict["ss"] = ss_list

        if MyGlobals.USE_BLOSUM62_RMODE:
            self.atom_dict["blosum62"] = self._get_new_facts_from_blosum62()

        if MyGlobals.USE_AA_TYPE_RMODES:
            self.atom_dict["position_aa_type"] = self._get_new_facts_from_aa_types()

        if MyGlobals.USE_BLOCKS9_RMODES:
            self.atom_dict["blocks9"] = self._get_new_facts_from_blocks9()

        if not MyGlobals.USE_AA_RMODE:
            del self.atom_dict["aa"]

        if not MyGlobals.USE_SS_RMODE:
            del self.atom_dict["ss"]

        return 0

    cdef list _get_new_facts_from_aa_types(self):
        from MainClasses.Priors.AminoAcidTypes import aa_to_type_dict
        cdef list new_facts, aa_list
        cdef bytes pos_label, aa_type

        if "aa" in self.atom_dict:
            aa_list = self.atom_dict["aa"]
            new_facts = [(pos_label, aa_type)
                         for pos_label, aa in aa_list if aa in aa_to_type_dict
                         for aa_type in aa_to_type_dict[aa]] + \
                        [(pos_label, '_') for pos_label, aa in aa_list if aa == '_']
        else:
            new_facts = []

        return new_facts

    cdef list _get_new_facts_from_blosum62(self):
        from MainClasses.Priors.Blosum62 import Blosum62_dict

        cdef list new_facts, aa_list
        cdef bytes pos_label, aa_type, aa, new_aa
        cdef float score

        if "aa" in self.atom_dict:
            aa_list = self.atom_dict["aa"]
            new_facts = [(pos_label, new_aa, score)
                         for pos_label, aa in aa_list if aa != '_'
                         for new_aa, score in Blosum62_dict[aa].items()] + \
                        [(pos_label, '_', '_') for pos_label, aa in aa_list if aa == '_']
        else:
            new_facts = []

        return new_facts

    cdef list _get_new_facts_from_blocks9(self):
        from MainClasses.Priors.Blocks9 import Blocks9_dicts

        cdef list new_facts
        cdef bytes pos_label, i, aa

        if "aa" in self.atom_dict:
            aa_list = self.atom_dict["aa"]
            new_facts = [(pos_label, i, Blocks9_dicts[i].setdefault(aa, 0.0))
                         for pos_label, aa in aa_list if aa != '_'
                         for i in xrange(len(Blocks9_dicts))] + \
                        [(pos_label, i, '_')
                         for pos_label, aa in aa_list for i in xrange(len(Blocks9_dicts)) if aa == '_']
        else:
            new_facts = []

        return new_facts

    cdef tuple check_query(self, FOLQuery query):
        #if cross_product_table has one row or more, 
        # then the example satisfies the query and the function should return True
        cdef list result_set = self.get_result_set_from_query(query)
        cdef tuple row
        cdef bint is_unified = bool(result_set)
        return is_unified, all(['_' in row for row in result_set]) if is_unified else False

    cdef bint check_special_op(self, bytes atom_name, object arg_a, object arg_b):
        global special_op_dict

        cdef bytes special_op = <bytes> special_op_dict[atom_name]
        cdef float _arg_a, _arg_b
        cdef bint result

        try:
            _arg_a = float(arg_a)
            _arg_b = float(arg_b)
            
            if special_op == '==':
                result = _arg_a == _arg_b
            elif special_op == '!=':
                result = _arg_a != _arg_b
            elif special_op == '>':
                result = _arg_a > _arg_b
            elif special_op == '>=':
                result = _arg_a >= _arg_b
            elif special_op == '<':
                result = _arg_a < _arg_b
            else:  # elif special_op == '<='
                result = _arg_a <= _arg_b

        except ValueError, e:
            if special_op == '==':
                result = arg_a == arg_b
            elif special_op == '!=':
                result = arg_a != arg_b
            elif special_op == '>':
                result = arg_a > arg_b
            elif special_op == '>=':
                result = arg_a >= arg_b
            elif special_op == '<':
                result = arg_a < arg_b
            else:  # elif special_op == '<='
                result = arg_a <= arg_b
                
        return result

    cdef list get_result_set_from_query(self, FOLQuery query):
        cdef dict var_indexes = {}
        cdef list cross_product_table = [()]
        cdef bytes q_atom_name
        cdef tuple q_atom_args
        # cdef object q_atom_args
        cdef object arg_a, arg_b, q_arg
        cdef bint a_is_variable, b_is_variable
        cdef int len_row1, i
        cdef list rows
        cdef tuple row1, row2

        global special_op_dict

        len_row1 = 0

        #correct python version of this, arg_a and arg_b cannot be converted to string
        for q_atom_name, q_atom_args in query.atom_list:
            if q_atom_name in self.atom_dict:
                rows = self.atom_dict[q_atom_name]

                #cross product between previous table and current atom table following the query.atoms_list order
                cross_product_table = \
                    [row1 + row2
                     for row1 in cross_product_table
                     for row2 in rows
                     if all([(var_indexes.setdefault(q_arg, len_row1 + i) or True)
                              if (str(q_arg)[0].isupper() and q_arg not in var_indexes)
                              else (((row1[var_indexes[q_arg]] in (row2[i], '_')
                                      if var_indexes[q_arg] < len_row1
                                      else row2[var_indexes[q_arg]-len_row1] in (row2[i], '_')) or row2[i] == '_')
                                    if str(q_arg)[0].isupper()
                                    else (row2[i] == q_arg or row2[i] == '_' or q_arg == '_'))
                             for i, q_arg in enumerate(q_atom_args)])]

                if cross_product_table:
                    len_row1 = len(cross_product_table[0])
                else:
                    cross_product_table = []
                    break

            elif q_atom_name in special_op_dict:
                arg_a, arg_b = q_atom_args

                a_is_variable = str.isupper(str(arg_a)[0])
                assert not a_is_variable or arg_a in var_indexes, \
                    "Special operation cannot execute with variable %s not unified.\n Variable index dictionary: %s" \
                    % (arg_a, str(var_indexes))

                b_is_variable = str.isupper(str(arg_b)[0])
                assert not b_is_variable or arg_b in var_indexes, \
                    "Special operation cannot execute with variable %s not unified.\n Variable index dictionary: %s" \
                    % (arg_b, str(var_indexes))

                # get_arg_value = lambda row, arg, is_variable: row[variable_indexes[arg]] if is_variable else arg
                cross_product_table = [row for row in cross_product_table
                                       if self.check_special_op(q_atom_name,
                                                                row[var_indexes[arg_a]] if a_is_variable else arg_a,
                                                                row[var_indexes[arg_b]] if b_is_variable else arg_b)
                                       or (row[var_indexes[arg_a]] if a_is_variable else arg_a) == '_'
                                       or (row[var_indexes[arg_b]] if b_is_variable else arg_b) == '_']

                if not cross_product_table:
                    cross_product_table = []
                    break

            else:
                cross_product_table = []
                break

        # #debug-comment
        # if cross_product_table:
        #     print (">>> Check query results -> Query: %s | Cross: %s | Atoms: %s"
        #            % (query.atom_list, cross_product_table, self.atom_dict))

        return cross_product_table

    # cdef int load_db(self) except -1:
    #     self.dao = MyDAO(sqlite3_in_memory=True)
    #     self.dao.save_example_to_db(self, is_last=False, atoms_with_don_t_care=True)
    #     return 0
    #
    # cdef int flush_db(self) except -1:
    #     self.dao = None
    #     return 0
    #
    # cdef list get_result_set_from_query(self, FOLQuery query, bint use_don_t_care=False):
    #     cdef list results = self.dao.get_true_ids(self.group_id, [self.example_id], query)
    #     # cdef list results = self.dao.get_result_set(self.group_id, [self.example_id], query)
    #     # #debug-comment
    #     # print (">>> Check query results -> Query: %s | Cross: %s | Atoms: %s"
    #     #        % (query.atom_list, result_set, [atom for atom in self.atoms_with_don_t_care if atom[0] == 'blocks9']))
    #
    #     return results

    # cdef dict _turn_atoms_to_lists(self, list atoms):
    #     #atom format is like (atom_name, [arg1, arg2, arg3, ...])
    #
    #     atoms_in_lists = {key: [args for atom_name, args in atoms if atom_name == key]
    #                       for key in set([atom[0] for atom in atoms])}
    #     return atoms_in_lists

    # cdef list _get_new_facts_from_rule(self, list rule, dict background_dict_of_facts=None, bint use_don_t_care=False):
    #
    #     cdef dict atoms_in_lists = \
    #         self._turn_atoms_to_lists(self.atoms_with_don_t_care if use_don_t_care else self.atoms)
    #     cdef dict fact_dict = dict(atoms_in_lists.items() + background_dict_of_facts.items())
    #
    #     cdef list new_facts = apply_modus_ponens(fact_dict, rule)
    #     return new_facts

    # if MyGlobals.USE_MODUS_PONENS:
    #     rule = ([('aa', ['X4', 'X1', 'X5']), ('bl_fact', ['X1', 'X2', 'X3'])], '->',
    #             [('blosum62', ['X4', 'X2', 'X3', 'X5'])])
    #     new_facts = self._get_new_facts_from_rule(rule, dict_of_facts)
    #     self.atoms.extend(new_facts)
    #     self.atoms_with_don_t_care.extend(new_facts)
    #
    #     rule = ([('aa', ['X1', '_', '_'])], '->', [('blosum62', ['X1', '_', '_', '_'])])
    #     self.atoms_with_don_t_care.extend(self._get_new_facts_from_rule(rule, {}, True))