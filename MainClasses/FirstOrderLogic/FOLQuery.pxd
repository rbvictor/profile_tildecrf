#!/usr/bin/env python

"""
Python Profile CRF Project - Query.pxd
"""

cdef class FOLQuery:
    """Class Query a Query object contains a list of atoms to be used in the decision tree.
    This query determines if an example is proved or not in a decision tree node.
    """

    cdef public list atom_list

    cdef FOLQuery clone(self)
    cdef bint has_atom(self, tuple atom)
    cdef void insert_atoms(self, list atoms)
    cdef int get_var_first_position(self, object var_name)
