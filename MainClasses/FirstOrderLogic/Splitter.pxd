

from MainClasses.FirstOrderLogic.FOLQuery cimport FOLQuery
from MainClasses.FirstOrderLogic.SplitInfo cimport SplitInfo

cdef class Splitter:
    """Class Splitter: Description"""

    cdef public FOLQuery query
    cdef public object rmode_manager
    cdef public int group_id
    cdef public list example_id_list
    cdef public float log_average
    cdef public float exp_average
    cdef public bytes dao_suffix
    cdef public bint coverage_mode_on

    cdef tuple get_best_coverage_info(self, long double max_coverage, int total_example_list_length,
                                      list false_example_id_list=*)
    cdef SplitInfo find_best_split_info(self, int iteration)
    cdef tuple _get_coverage_info(self, dao, conjunction, int total_length)
    cdef SplitInfo _get_split_info(self, object dao, list conjunction)