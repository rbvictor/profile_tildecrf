#!/usr/bin/env python

"""
Python Profile CRF Project - RmodeManager.py
"""

__author__ = "Romulo Barroso Victor"
__credits__ = ["Romulo Barroso Victor", "Gerson Zaverucha", "Juliana Bernardes"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Romulo Barroso Victor"
__email__ = "rbvictor@cos.ufrj.br"
__status__ = "Development"
__date__ = "10-Jul-2013"

from collections import Counter
from MainClasses.DAO.MyDAO import MyDAO
from MainClasses.FirstOrderLogic.Rmode cimport Rmode
from MainClasses.FirstOrderLogic.FOLQuery cimport FOLQuery
import MyGlobals
#from MainClasses.Priors import Blocks9


class RmodeManager:
    """
    RmodeManager Class
    """

    def __init__(self):
        """
        RmodeList constructor
        """

        #[STEP 1.1]

        self.rmode_order = {}
        self.rmode_dict = {}

    def add_rmode(self, rmode, order=0):
        #[STEP 1.1.1]

        if isinstance(order, int):
            order_list = [order]
        elif isinstance(order, list):
            order_list = order
        else:
            order_list = []

        for order_key in order_list:
            self.rmode_order.setdefault(order_key, []).append(rmode.atom_name)

        self.rmode_dict[rmode.atom_name] = rmode

    def _create_type_to_var_id_dict(self, FOLQuery query):

        var_id_to_type_list = \
            sorted([((<Rmode> self.rmode_dict[atom_name]).get_type(arg_index), int(str(args[arg_index])[1:]))
                    for atom_name, args in query.atom_list
                    for arg_index in xrange(len(args))
                    if atom_name in self.rmode_dict and str(args[arg_index])[0] == 'X'
                        and str.isdigit(str(args[arg_index])[1:])])

        if len(var_id_to_type_list) > 0:
            last_arg_type = var_id_to_type_list[0][1]
            last_var_ids = []
            type_to_var_id_dict = {}
            max_var_id = -1

            for arg_type, var_id in var_id_to_type_list:

                if arg_type != last_arg_type and len(last_var_ids) > 0:
                    local_var_id_list = sorted(list(set(last_var_ids)))
                    try:
                        local_max_var_id = max(local_var_id_list)
                    except ValueError, e:
                        print("!!!!!!!!! Error:\nlast_var_ids = " + str(last_var_ids) +
                              "\nvar_id_to_type_list = " + str(var_id_to_type_list) +
                              "\n+query = " + str(query.atom_list))
                        raise e
                    max_var_id = max(local_max_var_id, max_var_id)
                    type_to_var_id_dict[last_arg_type] = local_var_id_list
                    last_var_ids = []

                last_var_ids.append(var_id)
                last_arg_type = arg_type

            local_var_id_list = sorted(list(set(last_var_ids)))
            try:
                local_max_var_id = max(local_var_id_list)
            except ValueError, e:
                print("!!!!!!!!! Error:\nlast_var_ids = " + str(last_var_ids) +
                      "\nvar_id_to_type_list = " + str(var_id_to_type_list) +
                      "\n+query = " + str(query.atom_list))
                raise e
            max_var_id = max(local_max_var_id, max_var_id)
            type_to_var_id_dict[last_arg_type] = local_var_id_list

        else:
            type_to_var_id_dict = {}
            max_var_id = 0

        return type_to_var_id_dict, max_var_id

    def get_item_from_two_dicts(self, dict1, dict2, key):
        """
        :rtype: list
        """
        list1 = dict1[key] if dict1 and key in dict1 else []
        list2 = dict2[key] if dict2 and key in dict2 else []
        return list1 + list2

    def get_conjunction_list(self, dao_suffix, group_id, query, example_id_list, iteration=-1, average=None,
                             coverage_mode_on=False, max_coverage=1.0, total_length=0, false_example_id_list=None):
        #the rmode is the declaration of an atom, that is,
        #    it determines the number of arguments and the type of each argument: '+', '-', '+-'
        #    more information about rmode can be found in the Query.py source code

        dao = MyDAO(suffix=dao_suffix)

        type_to_var_id_dict, max_var_id = self._create_type_to_var_id_dict(query)
        count_atom_name_dict = dict(Counter([atom_name
                                             for atom_name, args in (<FOLQuery >query).atom_list]).most_common())

        if MyGlobals.ALTERNATE_RMODE_EACH_ITER and not coverage_mode_on:
            rmodes = [self.rmode_dict[r_name]
                      for r_name in self.rmode_order[iteration % len(self.rmode_order)]]
        else:
            r_names = [r_name for order in sorted(self.rmode_order) for r_name in self.rmode_order[order]]
            r_names = [r_names[i] for i in xrange(len(r_names)) if r_names[i] not in r_names[:i]]
            rmodes = [self.rmode_dict[r_name] for r_name in r_names]

        conjunction_lists = [self._get_conjunction_list_per_rmode(dao, dao_suffix, group_id, query, example_id_list,
                                                                  type_to_var_id_dict, max_var_id, rmode, average,
                                                                  coverage_mode_on, max_coverage, total_length,
                                                                  false_example_id_list)
                             for rmode in rmodes
                             if rmode.nb_of_literals - count_atom_name_dict.setdefault(rmode.atom_name, 0) > 0]

        del dao
        conjunction_list = [conjunction for c_list in conjunction_lists for conjunction in c_list]

        return conjunction_list

    def _get_conjunction_list_per_rmode(self, dao, dao_suffix, group_id, FOLQuery query, example_id_list,
                                        type_to_var_id_dict, max_var_id, Rmode rmode, average=None,
                                        coverage_mode_on=False, max_coverage=1.0, total_length=0, false_example_id_list=None):
        """ This method generates a new suggestion of a possible additional atom from rmode list.
        The rmode has the format ('name',['+'|'-'|'+-', '+'|'-'|'+-', ...]).
        :rtype: tuple
        """

        rmode_args = rmode.get_args()
        n_rmode_args = len(rmode_args)

        if rmode.is_const_gen:
            const_list_per_var = dao.get_list_from_const_gen(dao_suffix, group_id, example_id_list,
                                                             rmode.max_examples, rmode.max_values_per_example,
                                                             rmode.var_names, rmode.gen_query, average,
                                                             coverage_mode_on, max_coverage, total_length, false_example_id_list)

            if not const_list_per_var:  # if list is empty
                return []

            i_to_const_list_dict = \
                {key: list(value)
                 for keys, value in zip(rmode.var_names_indexes_in_output_rmode_args, const_list_per_var)
                 for key in keys}
        else:
            i_to_const_list_dict = {}

        end_next_conjunction = False
        possible_args_table_indexes = [-1] * n_rmode_args
        last_index_flags = [False] * n_rmode_args

        conjunction_list_per_rmode = []

        while not end_next_conjunction:
            possible_args_table = []
            temp_type_to_var_id_dict = {}
            temp_max_var_id = max_var_id
            first_const_index = -1

            for i in xrange(0, n_rmode_args):

                #mode '+': input variable: must have happened before at least once
                #mode '-': output variable: must never have happened before
                #mode '+-': input/output variable: no constraints
                rmode_arg_sign, rmode_arg_name = rmode_args[i]
                arg_type = rmode.get_type(i)

                # update possible_args_table

                if rmode_arg_sign == '-' or rmode_arg_sign == '+-':
                    possible_args_table.append(
                        self.get_item_from_two_dicts(type_to_var_id_dict, temp_type_to_var_id_dict, arg_type))
                
                if rmode_arg_sign == '+' or rmode_arg_sign == '+-':
                    temp_max_var_id += 1
                    temp_type_to_var_id_dict.setdefault(arg_type, []).append(temp_max_var_id)
                    
                    if rmode_arg_sign == '+':
                        possible_args_table.append([temp_max_var_id])
                    else:
                        possible_args_table[i].append(temp_max_var_id)

                if rmode_arg_sign == '--':
                    if first_const_index >= 0:
                        possible_args_table.append([i_to_const_list_dict[i][first_const_index]])
                    else:
                        possible_args_table.append(i_to_const_list_dict[i])

                if not rmode_arg_sign:
                    assert rmode_arg_name
                    possible_args_table.append([rmode_arg_name])

                # update possible_args_table_indexes

                if possible_args_table_indexes[i] == -1 or \
                        (possible_args_table_indexes[i] < len(possible_args_table[i]) - 1
                         and (i == n_rmode_args - 1 or all(last_index_flags[i + 1:]))):

                    possible_args_table_indexes[i] += 1
                    possible_args_table_indexes[i + 1: n_rmode_args] = [-1] * (n_rmode_args - (i + 1))
                    last_index_flags[i] = (possible_args_table_indexes[i] == len(possible_args_table[i]) - 1)

                    if rmode_arg_sign == '--' and first_const_index == -1:
                        first_const_index = possible_args_table_indexes[i]

                if i == n_rmode_args - 1 and last_index_flags[i]:
                    end_next_conjunction = all(last_index_flags)

            #final argument list is composed of a sequence of variables X1, X2, X3,... and constants from
            # dao.get_list_from_atom_name
            args = tuple([('X%d' % possible_args_table[i][possible_args_table_indexes[i]])
                          if rmode_args[i][0] in ['+', '-', '+-'] else possible_args_table[i][possible_args_table_indexes[i]]
                          for i in xrange(len(possible_args_table_indexes))])

            next_conjunction = [atom for atom in rmode.get_atoms(args) if not query.has_atom(atom)]

            if next_conjunction:
                conjunction_list_per_rmode.append(next_conjunction)

        return conjunction_list_per_rmode

    def use_aa_rmode(self):
        var_names = ['X1', 'X2']
        gen_query = FOLQuery([('aa', ('X1', 'X2'))])
        output_rmode = Rmode(10, 'aa', ('X1', 'X2'), ('position', 'amino_acid'))
        self.add_rmode(Rmode(10, '#', (MyGlobals.RMODE_CONST_GEN_MAX_EXAMPLES,
                                       MyGlobals.RMODE_CONST_GEN_MAX_VALUES_PER_EXAMPLE,
                                       var_names, gen_query, output_rmode)),
                       order=MyGlobals.AA_RMODE_ORDER)

    def use_ss_rmode(self):
        var_names = ['X1', 'X2']
        gen_query = FOLQuery([('ss', ('X1', 'X2'))])
        output_rmode = Rmode(10, 'ss', ('X1', 'X2'), ('position', 'secstr'))
        self.add_rmode(Rmode(10, '#', (MyGlobals.RMODE_CONST_GEN_MAX_EXAMPLES,
                                       MyGlobals.RMODE_CONST_GEN_MAX_VALUES_PER_EXAMPLE,
                                       var_names, gen_query, output_rmode)),
                       order=MyGlobals.SS_RMODE_ORDER)

    #Blosum rmode
    def use_blosum62_rmode(self):
        var_names = ['X1', 'X2']
        gen_query = FOLQuery([('blosum62', ('X1', 'X2', 'X3')), ('>', ('X3', str(MyGlobals.BLOSUM62_FLOOR)))])
        lookahead_rmodes = [Rmode(10, '>', ('-X3', str(MyGlobals.BLOSUM62_FLOOR)), ('score', 'score'))]
        output_rmode = Rmode(10, 'blosum62', ('X1', 'X2', '+X3'), ('position', 'amino_acid', 'score'),
                             lookahead_rmodes=lookahead_rmodes)
        rmode = Rmode(10, '#', (MyGlobals.RMODE_CONST_GEN_MAX_EXAMPLES,
                                MyGlobals.RMODE_CONST_GEN_MAX_VALUES_PER_EXAMPLE,
                                var_names, gen_query, output_rmode))
        self.add_rmode(rmode, order=MyGlobals.BLOSUM62_RMODE_ORDER)

    #Blosum rmode
    def use_blocks9_rmodes(self):
        var_names = ['X1', 'X2', 'X3']
        gen_query = FOLQuery([('blocks9', ('X1', 'X2', 'X3'))])
        lookahead_rmodes = [Rmode(10, '>=', ('-X0', 'X3'), ('score', 'score'))]
        output_rmode = Rmode(10, 'blocks9', ('X1', 'X2', '+X0'), ('position', 'component', 'score'),
                             lookahead_rmodes=lookahead_rmodes)
        self.add_rmode(Rmode(10, '#', (MyGlobals.RMODE_CONST_GEN_MAX_EXAMPLES,
                                       MyGlobals.RMODE_CONST_GEN_MAX_VALUES_PER_EXAMPLE,
                                       var_names, gen_query, output_rmode)),
                       order=MyGlobals.BLOCKS9_RMODES_ORDER)

    #Amino acid type rmode
    def use_aa_type_rmodes(self):
        var_names = ['X1', 'X2']
        gen_query = FOLQuery([('position_aa_type', ('X1', 'X2'))])
        output_rmode = Rmode(10, 'position_aa_type', ('X1', 'X2'), ('position', 'aa_type'))
        self.add_rmode(Rmode(10, '#', (MyGlobals.RMODE_CONST_GEN_MAX_EXAMPLES,
                                       MyGlobals.RMODE_CONST_GEN_MAX_VALUES_PER_EXAMPLE,
                                       var_names, gen_query, output_rmode)),
                       order=MyGlobals.AA_TYPE_RMODES_ORDER)