#!/usr/bin/env python

"""
Python Profile CRF Project - Splitter.py
"""

__author__ = "Romulo Barroso Victor"
__credits__ = ["Romulo Barroso Victor", "Gerson Zaverucha", "Juliana Bernardes"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Romulo Barroso Victor"
__email__ = "rbvictor@cos.ufrj.br"
__status__ = "Development"
__date__ = "10-Jul-2013"

import MyGlobals
from MainClasses.DAO.MyDAO import MyDAO


cdef class Splitter:
    """Class Splitter: Description"""

    def __cinit__(self, FOLQuery query, object rmode_manager, int group_id, list example_id_list, bytes dao_suffix,
                  float log_average=0.0, float exp_average=1.0, bint coverage_mode_on=False):
        
        """Splitter constructor"""
        
        self.query = FOLQuery([]) if query is None else query
        self.rmode_manager = rmode_manager
        self.group_id = group_id
        self.example_id_list = example_id_list
        self.log_average = log_average
        self.exp_average = exp_average
        self.dao_suffix = dao_suffix
        self.coverage_mode_on = coverage_mode_on

    def __getstate__(self):
        state_dict = {"query": self.query,
                      "rmode_manager": self.rmode_manager,
                      "group_id": self.group_id,
                      "example_id_list": self.example_id_list,
                      "log_average": self.log_average,
                      "exp_average": self.exp_average,
                      "dao_suffix": self.dao_suffix,
                      "coverage_mode_on": self.coverage_mode_on}
        return state_dict
    
    def __setstate__(self, state_dict):
        self.query = state_dict["query"]
        self.rmode_manager = state_dict["rmode_manager"]
        self.group_id = state_dict["group_id"]
        self.example_id_list = state_dict["example_id_list"]
        self.variance = state_dict["variance"]
        self.log_average = state_dict["log_average"]
        self.exp_average = state_dict["exp_average"]
        self.dao_suffix = state_dict["dao_suffix"]
        self.coverage_mode_on = state_dict["coverage_mode_on"]

    def __reduce__(self):
        return Splitter, (), self.__getstate__()

    cdef tuple get_best_coverage_info(self, long double max_coverage, int total_example_list_length, list false_example_id_list=None):
        assert self.coverage_mode_on, "This function can only be used with coverage mode on."

        cdef long double best_coverage, new_coverage
        cdef FOLQuery best_query, new_query
        cdef list best_example_id_list, new_example_id_list
        cdef list conjunction_list
        cdef object dao
        cdef int len_false_example_id_list = len(false_example_id_list) if false_example_id_list else 0

        if false_example_id_list is not None and len_false_example_id_list == 0:
            return None

        else:
            conjunction_list = \
                self.rmode_manager.get_conjunction_list(self.dao_suffix, self.group_id, self.query, self.example_id_list,
                                                        iteration=-1, average=None, coverage_mode_on=True,
                                                        max_coverage=max_coverage, total_length=total_example_list_length,
                                                        false_example_id_list=false_example_id_list)

            dao = MyDAO(suffix=self.dao_suffix, create_db_connection=True)

            best_coverage = 0.0
            best_query = None
            best_example_id_list = None

            for conjunction in conjunction_list:
                new_query, new_example_id_list, new_coverage = self._get_coverage_info(dao, conjunction, total_example_list_length)

                if new_coverage < max_coverage and \
                        ((not false_example_id_list and new_coverage > best_coverage) or
                         (false_example_id_list and (len(new_example_id_list) * 1.0 / len_false_example_id_list) > best_coverage)):

                    best_coverage = new_coverage
                    best_query = new_query
                    best_example_id_list = new_example_id_list

            del dao

            return (best_query, best_example_id_list, best_coverage) if best_query else None

    cdef SplitInfo find_best_split_info(self, int iteration):
        """
        :rtype:SplitInfo
        """
        cdef SplitInfo best_split_info, new_split_info
        cdef float best_entropy, new_split_info_entropy
        cdef list next_conjunction, conjunction_list
        dao = MyDAO(suffix=self.dao_suffix, create_db_connection=True)

        average = self.exp_average if MyGlobals.USE_EXP_VARIANCE else self.log_average
        conjunction_list = self.rmode_manager.get_conjunction_list(self.dao_suffix, self.group_id, self.query,
                                                                   self.example_id_list, iteration, average)

        best_entropy = float('inf')
        best_split_info = None

        for next_conjunction in conjunction_list:
            new_split_info = self._get_split_info(dao, next_conjunction)
            new_split_info_entropy = new_split_info.get_within_variance() if new_split_info is not None else MyGlobals.MAX_INT_VALUE

            if new_split_info is not None and new_split_info_entropy < best_entropy:
                #if new_split.entropy is smaller then the previous best entropy, then new_split is a better split
                #else averages that new_split is not a better split
                best_split_info = new_split_info
                best_entropy = new_split_info_entropy

        del dao
        return best_split_info

    cdef tuple _get_coverage_info(self, dao, conjunction, int total_length):
        #new_query is a clone object created so as not to destroy the original query object
        cdef FOLQuery query
        cdef list true_example_id_list
        cdef int n_true_examples
        cdef long double diff
        cdef long double true_coverage

        query = self.query.clone()
        query.insert_atoms(conjunction)

        #dao returns all positive examples according to query

        # true_example_id_list = dao.get_true_ids(self.group_id, self.example_id_list, query) ==> it is more correct but return stack overflow
        true_example_id_list = dao.get_true_ids(self.group_id, self.example_id_list, FOLQuery(conjunction))
        n_true_examples = len(true_example_id_list)

        # atom_name, args = conjunction[0]
        # arg0 = args[0]
        # arg0_first_char = arg0[0]

        cdef str arg0_first_char = str(conjunction[0][1][0][0])
        diff = ord(arg0_first_char) * 1e-5 + sum(true_example_id_list)/n_true_examples * 1e-15
        #see MyDAO.get_list_from_const_gen

        true_coverage = n_true_examples * 1.0/total_length + diff

        return query, true_example_id_list, true_coverage

    cdef SplitInfo _get_split_info(self, object dao, list conjunction):
        #new_query is a clone object created so as not to destroy the original query object
        cdef FOLQuery query
        cdef list true_example_id_list, false_example_id_list
        cdef float true_average, true_variance, true_exp_average, false_average, false_variance, false_exp_average
        cdef int n_true_examples, n_false_examples
        cdef SplitInfo split_info
        
        query = self.query.clone()
        query.insert_atoms(conjunction)

        #dao returns all positive examples according to query
        true_example_id_list = dao.get_true_ids(self.group_id, self.example_id_list, query)
        n_true_examples = len(true_example_id_list)
        if n_true_examples == 0:
            true_average, true_variance, true_exp_average = -MyGlobals.MAX_INT_VALUE, 0.0, 0.0
        else:
            true_average, true_variance, true_exp_average = dao.calculate_avg_and_variance(self.group_id,
                                                                                           true_example_id_list)
        false_example_id_list = list(set(self.example_id_list) - set(true_example_id_list))
        n_false_examples = len(false_example_id_list)

        if n_false_examples == 0:
            false_average, false_variance, false_exp_average = -MyGlobals.MAX_INT_VALUE, 0.0, 0.0
        else:
            false_average, false_variance, false_exp_average = dao.calculate_avg_and_variance(self.group_id,
                                                                                              false_example_id_list)

        split_info = \
            SplitInfo(query, true_example_id_list, false_example_id_list, n_true_examples, n_false_examples,
                      true_average, false_average, true_variance, false_variance, true_exp_average, false_exp_average)

        return split_info