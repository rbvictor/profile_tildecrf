

from MainClasses.FirstOrderLogic.FOLDecisionTree cimport FOLDecisionTree
from MainClasses.FirstOrderLogic.FOLPathExample cimport FOLPathExample
from MainClasses.FirstOrderLogic.FOLQuery cimport FOLQuery

cdef class FOLDecisionTree:
    """
    class FOLDecisionTree this class represents the first-order logic decision tree
    that will emulate the field function of the CRF
    """

    cdef public FOLQuery query
    cdef public int node_id
    cdef public int group_id
    cdef public object rmode_manager
    cdef public list example_id_list
    cdef public float average
    cdef public float variance
    cdef public float exp_average
    cdef public float min_variance
    cdef public bytes dao_suffix
    cdef public FOLDecisionTree true_child
    cdef public FOLDecisionTree false_child

    cdef void set_init_values(self, int group_id, object rmode_manager=*, FOLQuery query=*, int node_id=*,
                              list example_id_list=*, float average=*, float variance=*, float exp_average=*, 
                              bytes dao_suffix=*, float min_variance=*)
    cdef bint is_leaf(self)
    cdef int feed_example(self, FOLPathExample example, object dao_work_manager_queue=*) except -1
    cdef void clear_example_id_list(self)
    cdef void clear(self)
    cdef void _clear_recursive(self)
    cdef int induce_tree_from_root(self, int iteration) except -1
    cdef int set_value_to_false_leaf(self, long double average, long double exp_average, long double variance=*) except -1
    cdef int _induce_tree_recursive(self, int level=*, int iteration=*) except -1
    cdef long double calculate_foldt(self, FOLPathExample example) except 1e10
    cdef dict _convert_to_dictionary(self)
    cdef bytes _convert_to_json_string(self)
    cdef int _build_from_dictionary(self, dict json_dictionary, int node_id=*) except -1
    cdef int _build_from_json_string(self, bytes json_string) except -1
    cdef int save_tree_to_db(self, int sf_id, bytes target_f_char_id, int group_id, int tree_index) except -1
    cdef int load_tree_from_db(self, bytes target_f_char_id, int group_id, int tree_index) except -1
