import MyGlobals

_ModusPonens__cache = {}


def apply_modus_ponens(fact_dict, rule, store_cache=True):
    global _ModusPonens__cache
    new_facts = []

    tmp_fact_dict = dict(fact_dict)

    for p_atom_name, p_atom_args in rule[0]:
        for i in xrange(len(tmp_fact_dict.setdefault(p_atom_name, []))):
            fact = (p_atom_name, tmp_fact_dict[p_atom_name][i])

            str_rule = str(rule)
            str_fact = str(fact)

            if MyGlobals.USE_MODUS_PONENS_CACHE and str_rule in _ModusPonens__cache \
               and str_fact in _ModusPonens__cache[str_rule]:
                new_facts.extend(_ModusPonens__cache[str_rule][str_fact])

            else:
                new_rule = _apply_modus_ponens_on_fact(fact, rule)

                if new_rule:
                    new_p_atoms, arrow, new_q_atoms = new_rule
                    more_new_facts = []

                    if new_p_atoms:
                        new_fact_dict = {key: (tmp_fact_dict[key][i+1:] if key == p_atom_name else tmp_fact_dict[key])
                                         if key in tmp_fact_dict else []
                                         for key, args in new_rule[0]}

                        more_new_facts = apply_modus_ponens(new_fact_dict, new_rule, False)

                    elif new_q_atoms:
                        more_new_facts = new_q_atoms

                    if MyGlobals.USE_MODUS_PONENS_CACHE and store_cache:
                        _ModusPonens__cache.setdefault(str_rule, {})[str_fact] = more_new_facts

                    new_facts.extend(more_new_facts)

        tmp_fact_dict[p_atom_name] = []

    return new_facts

def _apply_modus_ponens_on_fact(fact, rule):

    p_atoms = [(p_atom_name, p_atom_args[:]) for p_atom_name, p_atom_args in rule[0]]
    arrow = rule[1]
    assert arrow in ['->', '-:']

    f_atom_name, f_args = fact[0], fact[1][:]
    len_args = len(f_args)
    substitutions = {}
    new_rule = None

    for p_atom in p_atoms:
        p_atom_name, p_args = p_atom

        if p_atom_name == f_atom_name and len(p_args) == len_args:
            unified = True

            for i in xrange(len_args):
                f_arg, p_arg = f_args[i], p_args[i]
                if f_arg == p_arg:
                    pass
                elif str.isupper(str(p_arg)[0]):  # p_arg is a variable
                    substitutions[p_arg] = f_arg
                    for j in xrange(i, len_args):
                        p_args[j] = f_arg if p_args[j] == p_arg else p_args[j]
                elif str.isupper(str(f_arg)[0]):  # f_arg is a variable
                    substitutions[f_arg] = p_arg
                    for j in xrange(i, len_args):
                        f_args[j] = p_arg if f_args[j] == f_arg else f_args[j]
                else:
                    unified = False
                    break

            if unified:
                new_rule = _apply_substitution_on_rule(substitutions, rule)
                new_p_atoms, new_arrow, new_q_atoms = new_rule
                #new_f_atom = (f_atom_name, f_args)
                new_p_atoms = [new_p_atom for new_p_atom in new_p_atoms
                               if not (new_p_atom[0] == f_atom_name and len(new_p_atom[1]) == len(f_args) and
                                       all([new_p_atom[1][i] == f_args[i] for i in xrange(len(f_args))]))]
                new_rule = new_p_atoms, new_arrow, new_q_atoms
                break

    return new_rule

def _apply_substitution_on_rule(substitutions, rule):
    p_atoms, arrow, q_atoms = rule
    if arrow in ["<-", ":-"]:
        aux = p_atoms
        p_atoms = q_atoms
        q_atoms = aux

    new_p_atoms = [(p_atom_name, [_apply_substitution_on_arg(substitutions, p_atom_arg)
                                  for p_atom_arg in p_atom_args])
                   for p_atom_name, p_atom_args in p_atoms]

    new_q_atoms = [(q_atom_name, [_apply_substitution_on_arg(substitutions, q_atom_arg)
                                  for q_atom_arg in q_atom_args])
                   for q_atom_name, q_atom_args in q_atoms]

    new_rule = new_p_atoms, "->", new_q_atoms

    return new_rule

def _apply_substitution_on_arg(substitutions, arg):
    new_arg = arg
    while new_arg in substitutions:
        new_arg = substitutions[new_arg]
    return new_arg