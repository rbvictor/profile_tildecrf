__author__ = 'rbvictor'

from MainClasses.FirstOrderLogic.FOLQuery cimport FOLQuery
from MainClasses.FirstOrderLogic.Rmode cimport Rmode


cdef class Rmode:
    """
    Rmode Class
    """
    cdef public bytes atom_name
    cdef public int nb_of_literals
    cdef public bint is_const_gen
    cdef public bint lookahead_is_on
    cdef public FOLQuery gen_query
    cdef public int max_examples
    cdef public int max_values_per_example
    cdef public list var_names
    cdef public list var_names_indexes_in_output_rmode_args
    cdef public dict arg_name_to_arg_index_dict
    cdef public list atom_name_to_arg_indexes
    cdef public tuple rmode_args
    cdef public tuple types
    cdef public int n_types
    cdef public Rmode output_rmode

    cdef tuple _convert_arg_to_tuple(self, object arg)
    cdef bytes get_type(self, int arg_index)
    cdef void add_types(self, tuple types)
    cdef tuple get_args(self)
    cdef list get_atoms(self, tuple args)
    cdef void set_lookahead_rmodes(self, list lookahead_rmodes)