from MainClasses.FirstOrderLogic.FTestTable cimport FTestTable


cdef class SplitInfo:
    """ the class object represents the attributes of the best query that minimizes the entropy determined
    by the within-group variance equation """

    #----------------------------------------------------------------------------------------------------#
    #   entropy = number_of_true_ids * true_ids_variance + number_of_false_ids * false_ids_variance      #
    #----------------------------------------------------------------------------------------------------#

    def __cinit__(self, FOLQuery query,
                        list     true_example_id_list,
                        list     false_example_id_list,
                        int      n_true_examples,
                        int      n_false_examples,
                        float    true_average,
                        float    false_average,
                        float    true_variance,
                        float    false_variance,
                        float    true_exp_average,
                        float    false_exp_average):

        self.query = query
        self.true_example_id_list = true_example_id_list
        self.false_example_id_list = false_example_id_list
        self.n_true_examples = n_true_examples
        self.n_false_examples = n_false_examples
        self.true_average = true_average
        self.false_average = false_average
        self.true_variance = true_variance
        self.false_variance = false_variance
        self.true_exp_average = true_exp_average
        self.false_exp_average = false_exp_average
        
    def __getstate__(self):
        state_dict = {"true_example_id_list": self.true_example_id_list,
                      "false_example_id_list": self.false_example_id_list,
                      "n_true_examples": self.n_true_examples,
                      "n_false_examples": self.n_false_examples,
                      "true_average": self.true_average,
                      "false_average": self.false_average,
                      "true_variance": self.true_variance,
                      "false_variance": self.false_variance,
                      "true_exp_average": self.true_exp_average,
                      "false_exp_average": self.false_exp_average}
        return state_dict

    def __setstate__(self, state_dict):
        self.true_example_id_list = state_dict["true_example_id_list"] 
        self.false_example_id_list = state_dict["false_example_id_list"] 
        self.n_true_examples = state_dict["n_true_examples"] 
        self.n_false_examples = state_dict["n_false_examples"] 
        self.true_average = state_dict["true_average"] 
        self.false_average = state_dict["false_average"] 
        self.true_variance = state_dict["true_variance"] 
        self.false_variance = state_dict["false_variance"] 
        self.true_exp_average = state_dict["true_exp_average"] 
        self.false_exp_average = state_dict["false_exp_average"] 

    def __reduce__(self):
        init_args = (self.query,
                     self.true_example_id_list,
                     self.false_example_id_list,
                     self.n_true_examples,
                     self.n_false_examples,
                     self.true_average,
                     self.false_average,
                     self.true_variance,
                     self.false_variance,
                     self.true_exp_average,
                     self.false_exp_average)

        return SplitInfo, init_args, self.__getstate__()

    cdef int get_len_true_examples(self):
        self.n_true_examples = len(self.true_example_id_list)
        return self.n_true_examples

    cdef int get_len_false_examples(self):
        self.n_false_examples = len(self.false_example_id_list)
        return self.n_false_examples

    cdef float get_within_variance(self):
        cdef int n_true_examples, n_false_examples
        cdef float within_variance
        
        n_true_examples = self.get_len_true_examples()
        n_false_examples = self.get_len_false_examples()
        
        within_variance = ((n_true_examples * self.true_variance + n_false_examples * self.false_variance) /
                                (n_true_examples + n_false_examples))
        
        return within_variance
    
    cdef float get_between_variance(self, float total_exp_average):
        cdef int n_true_examples, n_false_examples
        cdef float true_avg_minus_total_avg, false_avg_minus_total_avg, between_variance
        
        n_true_examples = self.get_len_true_examples()
        n_false_examples = self.get_len_false_examples()

        true_avg_minus_total_avg = self.true_exp_average - total_exp_average
        false_avg_minus_total_avg = self.false_exp_average - total_exp_average

        between_variance = ((n_true_examples * true_avg_minus_total_avg * true_avg_minus_total_avg
                             + n_false_examples * false_avg_minus_total_avg * false_avg_minus_total_avg) /
                            (n_true_examples + n_false_examples))

        return between_variance

    cdef bint check_f_test(self, float total_exp_average):
        cdef float denominator, f_value, f_critical
        cdef int i
        cdef int n_true_examples = self.get_len_true_examples()
        cdef int n_false_examples = self.get_len_false_examples()
        cdef int n = n_true_examples + n_false_examples
        cdef int k = 2
        cdef FTestTable f_test_table

        if n == k:
            return False
        else:
            denominator = self.get_within_variance() / (n - k)

            if denominator == 0.0:
                return True

            else:
                f_test_table = FTestTable()
                f_value = (self.get_between_variance(total_exp_average) / (k - 1)) / denominator
                f_critical = f_test_table.get_f_test_value(n - k)
                return f_value > f_critical