#!/usr/bin/env python

"""
Python Profile CRF Project - FOLClassificationTree.py
"""

import MyGlobals
from MainClasses.DAO.MyDAO import MyDAO
from MainClasses.FirstOrderLogic.Splitter cimport Splitter
from MainClasses.FirstOrderLogic.RmodeManager import RmodeManager


cdef class FOLClassificationTree:
    """
    class FOLClassificationTree
    """

    def __cinit__(self, int group_id, object rmode_manager=None, FOLQuery query=None, int node_id=1,
                  list example_id_list=None, bytes dao_suffix=(<bytes>""), long double coverage=1.01,
                  list false_example_id_list=None):

        self.node_id = node_id
        self.true_child = None
        self.false_child = None
        self.dao_suffix = dao_suffix
        self.query = query
        self.group_id = group_id
        self.example_id_list = example_id_list if example_id_list else []
        self.rmode_manager = RmodeManager() if not rmode_manager else rmode_manager
        self.coverage = coverage
        self.false_example_id_list = false_example_id_list

    def __getstate__(self):
        state_dict = {"node_id" : self.node_id,
                      "true_child" : self.true_child,
                      "false_child" : self.false_child,
                      "dao_suffix" : self.dao_suffix,
                      "query" : self.query,
                      "group_id" : self.group_id,
                      "example_id_list" : self.example_id_list,
                      "rmode_manager" : self.rmode_manager,
                      "coverage" : self.coverage,
                      "false_example_id_list" : self.false_example_id_list}
        return state_dict

    def __setstate__(self, state_dict):
        self.node_id                   = state_dict["node_id"]
        self.true_child                = state_dict["true_child"]
        self.false_child               = state_dict["false_child"]
        self.dao_suffix                = state_dict["dao_suffix"]
        self.query                     = state_dict["query"]
        self.group_id                  = state_dict["group_id"]
        self.example_id_list           = state_dict["example_id_list"]
        self.rmode_manager             = state_dict["rmode_manager"]
        self.coverage                  = state_dict["coverage"]
        self.false_example_id_list     = state_dict["false_example_id_list"]

    def __reduce__(self):
        init_args = ()
        return FOLClassificationTree, (self.group_id,), self.__getstate__()

    cdef bint is_leaf(self):
        """This method returns True if the node is a leaf and False, otherwise.
        :rtype: bool
        """
        return not self.true_child and not self.false_child

    cdef int feed_example(self, FOLPathExample example, object dao_work_manager_queue=None) except -1:
        """This method adds an example into the example id list and save it in the database."""
        dao = MyDAO(suffix=self.dao_suffix, create_db_connection=False, dao_work_manager_queue=dao_work_manager_queue)
        dao.save_example_to_db(example)
        return 0

    cdef int induce_tree_from_root(self) except -1:
        """This method triggers the top-down induction from the root of the decision tree."""

        cdef int group_id = self.group_id

        self.coverage= 1.01         #  It was supposed to be 1.0, but it has to a little bigger for managing children
                                    #with same coverage: see dao.get_list_from_const_gen (coverage_mode_on=True)

        self.example_id_list = MyDAO(suffix=self.dao_suffix).get_example_id_list(self.group_id)

        if self.example_id_list:
            self._induce_tree_recursive(0, len(self.example_id_list))

        return 0

    cdef int _induce_tree_recursive(self, int level, int total_example_list_length) except -1:
        """
        This is an auxiliary method of the induce_tree_from_root method. It is not recommended to use it otherwise.
        """
        cdef int len_example_id_list = len(self.example_id_list)
        cdef FOLQuery last_query, new_query
        cdef tuple best_coverage_info = None
        cdef Splitter splitter
        cdef list query_info_list, new_example_id_list, false_example_id_list
        cdef long double new_coverage
        cdef long double max_coverage = self.coverage


        if level < MyGlobals.MAX_CLASSIFICATION_TREE_DEPTH:
            splitter = Splitter(self.query, self.rmode_manager, self.group_id, self.example_id_list, self.dao_suffix,
                                coverage_mode_on=True)

            best_coverage_info = splitter.get_best_coverage_info(max_coverage, total_example_list_length, self.false_example_id_list)

            del splitter

        if level < MyGlobals.MAX_CLASSIFICATION_TREE_DEPTH and best_coverage_info is not None:
            new_query, new_example_id_list, new_coverage = best_coverage_info
            # print bin(self.node_id), new_coverage, new_query.atom_list

            #assign best split query to the query of this node
            last_query = self.query
            self.query = new_query

            true_id = self.node_id * 2 + 1
            self.true_child = FOLClassificationTree(self.group_id, self.rmode_manager, new_query, true_id,
                                                    new_example_id_list, self.dao_suffix, new_coverage)
            self.true_child._induce_tree_recursive(level + 1, total_example_list_length)

            # if self.node_id % 2 == 0:
            #     self.false_child = FOLClassificationTree(self.group_id, self.rmode_manager, None, self.node_id * 2,
            #                                          self.example_id_list, self.dao_suffix, self.coverage, [])
            #
            # else:

            false_example_id_list = [example_id
                                     for example_id in (self.false_example_id_list
                                                        if self.false_example_id_list else self.example_id_list)
                                     if example_id not in new_example_id_list]

            false_id = self.node_id * 2
            self.false_child = FOLClassificationTree(self.group_id, self.rmode_manager, last_query, self.node_id * 2,
                                                     self.example_id_list, self.dao_suffix, self.coverage, false_example_id_list)

            self.false_child._induce_tree_recursive(level + 1, total_example_list_length)
        else:
            self.query = None

        return 0

    cdef int check_example(self, FOLPathExample example) except -1:
        """
        This method calculate the predicted value of the input example,
        using the already induced first-order logic decision tree
        :rtype: float
        """
        cdef bint is_unified, don_t_care_included
        cdef FOLClassificationTree child_node
        cdef FOLQuery query

        if self.is_leaf():
            if self.node_id == 1:
                return 0
            else:
                return self.node_id % 2
        else:
            is_unified, don_t_care_included = example.check_query(self.query)
            if is_unified:
                return self.true_child.check_example(example)       # ---> example satisfies query
            else:
                return self.false_child.check_example(example)      # ---> example does NOT satisfy query
