from MainClasses.FirstOrderLogic.FOLQuery cimport FOLQuery

cdef class SplitInfo:
    """ the class object represents the attributes of the best query that minimizes the entropy determined
    by the within-group variance equation """

    cdef public FOLQuery query                 # -> the query that minimizes the entropy of the regression tree node;
    cdef public list true_example_id_list   # -> the ids of the examples found by executing the new_query;
    cdef public list false_example_id_list  # -> the set from minus operation of 
                                            #    foldt_node.example_id_list and true_example_id_list;
    cdef public int n_true_examples         # -> number of true examples;
    cdef public int n_false_examples        # -> number of false_examples;
    cdef public float true_average          # -> the average of the values of the true examples;
    cdef public float false_average         # -> the average of the values of the false examples;
    cdef public float true_variance         # -> the variance of the values of the true examples;
    cdef public float false_variance        # -> the variance of the values of the false examples;
    cdef public float true_exp_average
    cdef public float false_exp_average

    cdef int get_len_true_examples(self)
    cdef int get_len_false_examples(self)
    cdef float get_within_variance(self)
    cdef float get_between_variance(self, float total_exp_average)
    cdef bint check_f_test(self, float total_exp_average)