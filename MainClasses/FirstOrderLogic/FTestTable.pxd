
cdef class FTestTable:

    cdef int len_f_test_table
    cdef float* f_test_table

    cdef float* _get_f_test_alpha_005_list(self)
    cdef float* _get_f_test_alpha_001_list(self)
    cdef float* _get_f_test_alpha_0001_list(self)
    cdef float get_f_test_value(self, int ddf)