__author__ = 'rbvictor'

from MainClasses.FirstOrderLogic.FOLQuery cimport FOLQuery


cdef class Rmode:
    """
    Rmode Class
    """

    def __cinit__(self, int nb_of_literals, bytes atom_name, object rmode_args, object types=None,
                  list lookahead_rmodes=None):
        """
        Rmode constructor
        """

        #[STEP 1.1]

        self.nb_of_literals = nb_of_literals
        self.is_const_gen = <bint> (atom_name == "#")
        self.lookahead_is_on = False

        self.arg_name_to_arg_index_dict = {}
        self.atom_name_to_arg_indexes = []

        if not self.is_const_gen:
            self.atom_name = atom_name
            self.rmode_args = tuple([self._convert_arg_to_tuple(rmode_arg) for rmode_arg in rmode_args])
            self.types = types

        else:  # it is a constant generator
            self.max_examples = <int> rmode_args[0]
            self.max_values_per_example = <int> rmode_args[1]
            self.var_names = rmode_args[2]
            self.gen_query = <FOLQuery> rmode_args[3]
            self.output_rmode = <Rmode> rmode_args[4]

            self.atom_name = <bytes> self.output_rmode.atom_name
            output_rmode_args = self.output_rmode.rmode_args

            self.rmode_args = tuple([('--' if rmode_arg_name in self.var_names else rmode_arg_sign, rmode_arg_name)
                                    for rmode_arg_sign, rmode_arg_name in output_rmode_args])

            self.var_names_indexes_in_output_rmode_args = \
                [[i for i in xrange(len(output_rmode_args)) if output_rmode_args[i][1] == var_name]
                 for var_name in self.var_names]

            self.types = self.output_rmode.types

        self.n_types = len(self.types) if self.types else 0

        if self.types:
            assert self.n_types == len(self.rmode_args), "Number of types is not equal to number of modes"

        if lookahead_rmodes:
            self.set_lookahead_rmodes(lookahead_rmodes)

    def __getstate__(self):
        state_dict = {"atom_name": self.atom_name,
                      "nb_of_literals": self.nb_of_literals,
                      "is_const_gen": self.is_const_gen,
                      "lookahead_is_on": self.lookahead_is_on,
                      "gen_query": self.gen_query,
                      "max_examples": self.max_examples,
                      "max_values_per_example": self.max_values_per_example,
                      "var_names": self.var_names,
                      "var_names_indexes_in_output_rmode_args": self.var_names_indexes_in_output_rmode_args,
                      "arg_name_to_arg_index_dict": self.arg_name_to_arg_index_dict,
                      "atom_name_to_arg_indexes": self.atom_name_to_arg_indexes,
                      "rmode_args": self.rmode_args,
                      "types": self.types,
                      "n_types": self.n_types,
                      "output_rmode": self.output_rmode}
        return state_dict
        
    def __setstate__(self, state_dict):
        self.atom_name = state_dict["atom_name"]
        self.nb_of_literals = state_dict["nb_of_literals"]
        self.is_const_gen = state_dict["is_const_gen"]
        self.lookahead_is_on = state_dict["lookahead_is_on"]
        self.gen_query = state_dict["gen_query"]
        self.max_examples = state_dict["max_examples"]
        self.max_values_per_example = state_dict["max_values_per_example"]
        self.var_names = state_dict["var_names"]
        self.var_names_indexes_in_output_rmode_args = state_dict["var_names_indexes_in_output_rmode_args"]
        self.arg_name_to_arg_index_dict = state_dict["arg_name_to_arg_index_dict"]
        self.atom_name_to_arg_indexes = state_dict["atom_name_to_arg_indexes"]
        self.rmode_args = state_dict["rmode_args"]
        self.types = state_dict["types"]
        self.n_types = state_dict["n_types"]
        self.output_rmode = state_dict["output_rmode"]

    def __reduce__(self):
        init_args = (self.nb_of_literals, self.atom_name, self.rmode_args)
        return Rmode, init_args, self.__getstate__()

    cdef tuple _convert_arg_to_tuple(self, object arg):
        if isinstance(arg, str):
            if arg[:2] in ['+-', '--']:
                return arg[:2], arg[2:]
            elif arg[0] in ['+', '-']:
                return arg[0], arg[1:]
            else:
                return None, arg
        else:
            return None, arg

    cdef bytes get_type(self, int arg_index):
        if arg_index < self.n_types:
            return <bytes> self.types[arg_index]
        else:
            return <bytes> ""

    cdef void add_types(self, tuple types):
        self.n_types = len(types) if types else 0
        self.types = types

        if types:
            assert self.n_types == len(self.rmode_args), "Number of types is not equal to number of modes"

    cdef tuple get_args(self):
        return self.rmode_args

    cdef list get_atoms(self, tuple args):
        cdef Rmode rmode
        cdef int len_args, arg_index
        cdef list atoms, atom_indexes
        cdef tuple atom_args
        cdef bytes atom_name

        rmode = self.output_rmode if self.is_const_gen else self

        if rmode.lookahead_is_on:
            len_args = len(args)
            assert len_args == len(rmode.rmode_args)

            atoms = []
            for atom_name, arg_indexes in rmode.atom_name_to_arg_indexes:
                atom_args = tuple([args[arg_index] for arg_index in arg_indexes])
                atoms.append((atom_name, atom_args))
            return atoms

        else:
            return [(rmode.atom_name, args)]

    cdef void set_lookahead_rmodes(self, list lookahead_rmodes):
        assert not self.lookahead_is_on, "Lookahead option is already enabled."

        cdef int i
        cdef bytes l_rmode_atom_name, new_type
        cdef Rmode l_rmode
        cdef list l_rmodes, l_rmode_arg_indexes
        cdef tuple new_rmode_args, l_rmode_args
        cdef tuple new_types

        self.lookahead_is_on = True
        new_rmode_args = ()
        new_types = ()

        l_rmodes = [self] + lookahead_rmodes
        for l_rmode in l_rmodes:
            l_rmode_atom_name, l_rmode_args = l_rmode.atom_name, l_rmode.rmode_args
            l_rmode_arg_indexes = []

            for i in xrange(len(l_rmode_args)):
                l_rmode_arg_sign, l_rmode_arg_name = l_rmode_args[i]

                if l_rmode_arg_name in self.arg_name_to_arg_index_dict:
                    assert l_rmode.get_type(i) == self.get_type(self.arg_name_to_arg_index_dict[l_rmode_arg_name]) \
                               or l_rmode.get_type(i) is None \
                        or self.get_type(self.arg_name_to_arg_index_dict[l_rmode_arg_name]) is None, \
                        "Lookahead type does not match."
                else:
                    self.arg_name_to_arg_index_dict[l_rmode_arg_name] = len(self.arg_name_to_arg_index_dict)
                    new_rmode_args += (l_rmode_args[i],)
                    new_type = l_rmode.get_type(i)
                    if self.types and new_type:
                        new_types += (new_type,)

                l_rmode_arg_indexes.append(self.arg_name_to_arg_index_dict[l_rmode_arg_name])

            if not self.atom_name_to_arg_indexes:
                self.atom_name_to_arg_indexes = []

            self.atom_name_to_arg_indexes.append((l_rmode_atom_name, l_rmode_arg_indexes))

        self.types = new_types
        self.rmode_args = new_rmode_args