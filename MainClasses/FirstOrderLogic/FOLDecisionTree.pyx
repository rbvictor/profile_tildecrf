#!/usr/bin/env python

"""
Python Profile CRF Project - FOLDecisionTree.py
"""

import json
import MyGlobals
from MainClasses.DAO.MyDAO import MyDAO
from MainClasses.FirstOrderLogic.Splitter cimport Splitter
from MainClasses.FirstOrderLogic.SplitInfo cimport SplitInfo
from MainClasses.FirstOrderLogic.RmodeManager import RmodeManager


cdef class FOLDecisionTree:
    """
    class FOLDecisionTree: this class represents the first-order logic decision tree
    that will emulate the field function of the CRF
    """

    def __cinit__(self, int group_id, object rmode_manager=None, FOLQuery query=None, int node_id=1,
                  list example_id_list=None, float average=0.0, float variance=0.0, float exp_average=0.0,
                  bytes dao_suffix=(<bytes>""), float min_variance=0.0):
        """FOLDecisionTree constructor"""

        self.node_id = node_id
        self.true_child = None
        self.false_child = None
        self.average = average
        self.variance = variance
        self.exp_average = exp_average
        self.dao_suffix = dao_suffix
        self.query = FOLQuery([]) if query is None else query
        self.group_id = group_id
        self.example_id_list = example_id_list if example_id_list else []
        self.rmode_manager = RmodeManager() if not rmode_manager else rmode_manager
        self.min_variance = MyGlobals.MIN_VARIANCE if min_variance is None else min_variance

    def __getstate__(self):
        state_dict = {"query": self.query,
                      "node_id": self.node_id,
                      "group_id": self.group_id,
                      "rmode_manager": self.rmode_manager,
                      "example_id_list": self.example_id_list,
                      "average": self.average,
                      "variance": self.variance,
                      "exp_average": self.exp_average,
                      "min_variance": self.min_variance,
                      "dao_suffix": self.dao_suffix,
                      "true_child": self.true_child,
                      "false_child": self.false_child}
        return state_dict

    def __setstate__(self, state_dict):
        self.query = state_dict["query"]
        self.node_id = state_dict["node_id"]
        self.group_id = state_dict["group_id"]
        self.rmode_manager = state_dict["rmode_manager"]
        self.example_id_list = state_dict["example_id_list"]
        self.average = state_dict["average"]
        self.variance = state_dict["variance"]
        self.exp_average = state_dict["exp_average"]
        self.min_variance = state_dict["min_variance"]
        self.dao_suffix = state_dict["dao_suffix"]
        self.true_child = state_dict["true_child"]
        self.false_child = state_dict["false_child"]

    def __reduce__(self):
        init_args = ()
        return FOLDecisionTree, (self.group_id,), self.__getstate__()

    cdef void set_init_values(self, int group_id, object rmode_manager=None, FOLQuery query=None, int node_id=1,
                              list example_id_list=None, float average=0.0, float variance=0.0, float exp_average=0.0,
                              bytes dao_suffix=(<bytes>""), float min_variance=0.0):
        """FOLDecisionTree constructor"""

        self.node_id = node_id
        self.average = average
        self.variance = variance
        self.exp_average = exp_average
        self.dao_suffix = dao_suffix
        self.query = FOLQuery([]) if query is None else query
        self.group_id = group_id
        self.example_id_list = example_id_list if example_id_list else []
        self.rmode_manager = RmodeManager() if not rmode_manager else rmode_manager

    cdef bint is_leaf(self):
        """This method returns True if the node is a leaf and False, otherwise.
        :rtype: bool
        """
        return self.true_child is None and self.false_child is None

    cdef int feed_example(self, FOLPathExample example, object dao_work_manager_queue=None) except -1:
        """This method adds an example into the example id list and save it in the database."""
        dao = MyDAO(suffix=self.dao_suffix, create_db_connection=False, dao_work_manager_queue=dao_work_manager_queue)
        dao.save_example_to_db(example)

        # dao.save_example_to_db(example, is_last)
        return 0

    cdef void clear_example_id_list(self):
        self.example_id_list = []
        if self.true_child:
            self.true_child.clear_example_id_list()
        if self.false_child:
            self.false_child.clear_example_id_list()

    cdef void clear(self):
        self._clear_recursive()

        query = None
        group_id = 0
        average = 0.0
        variance = 0.0
        exp_average = 0.0
        min_variance = 0.0

        self.set_init_values(self.node_id, self.rmode_manager, query, group_id, self.example_id_list,
                             average, variance, exp_average, self.dao_suffix, min_variance)

    cdef void _clear_recursive(self):
        """This method clears recursively all the decision tree."""
        self.query = None

        if self.true_child:
            self.true_child._clear_recursive()
            self.true_child = None

        if self.false_child:
            self.false_child._clear_recursive()
            self.false_child = None

    cdef int induce_tree_from_root(self, int iteration) except -1:
        """This method triggers the top-down induction from the root of the decision tree."""

        cdef int group_id = self.group_id
        cdef object dao = MyDAO(suffix=self.dao_suffix)

        self.example_id_list = dao.get_example_id_list(group_id)

        if not self.example_id_list:
            # if MyGlobals.USE_CLASSIFICATION_TREE:
            #     self.average, self.variance, self.exp_average = -1.0, 0.0, 0.36787944117144233
            #     # self.average, self.variance, self.exp_average = 0.0, 0.0, 1.0
            # else:
            self.average, self.variance, self.exp_average = 0.0, 0.0, 1.0
            self.query = None
            self.true_child = None
            self.false_child = None

        else:
            self.average, self.variance, self.exp_average = dao.calculate_avg_and_variance(self.group_id)
            dao = None
            self._induce_tree_recursive(0, iteration)

        return 0

    cdef int set_value_to_false_leaf(self, long double average, long double exp_average, long double variance=0.0) except -1:
        cdef FOLDecisionTree false_leaf

        false_leaf = self
        while false_leaf.false_child is not None:
            false_leaf = false_leaf.false_child

        false_leaf.average, false_leaf.variance, false_leaf.exp_average = average, variance, exp_average
        return 0

    cdef int _induce_tree_recursive(self, int level=0, int iteration=0) except -1:
        """
        This is an auxiliary method of the induce_tree_from_root method. It is not recommended to use it otherwise.
        """
        cdef int len_example_id_list = len(self.example_id_list)
        cdef FOLQuery last_query
        cdef Splitter splitter
        cdef SplitInfo best_split

        self.true_child = None
        self.false_child = None

        if level < MyGlobals.MAX_TREE_DEPTH and self.example_id_list is not None \
            and len_example_id_list > MyGlobals.MIN_NUM_EXAMPLES \
            and (level < MyGlobals.MIN_TREE_DEPTH or self.variance > self.min_variance):

            splitter = Splitter(self.query, self.rmode_manager, self.group_id, self.example_id_list,
                                self.dao_suffix, self.average, self.exp_average)

            best_split = splitter.find_best_split_info(iteration)

            del splitter

            if best_split is not None:
                if best_split.get_len_true_examples() > 0 and best_split.get_len_false_examples() > 0 \
                    and best_split.check_f_test(self.exp_average):
                    #assign best split query to the query of this node
                    last_query = self.query
                    self.query = best_split.query

                    self.true_child = FOLDecisionTree(self.group_id, self.rmode_manager, self.query,
                                                      self.node_id * 2 + 1, best_split.true_example_id_list,
                                                      best_split.true_average, best_split.true_variance,
                                                      best_split.true_exp_average, self.dao_suffix, self.min_variance)

                    self.true_child._induce_tree_recursive(level + 1, iteration)

                    self.false_child = FOLDecisionTree(self.group_id, self.rmode_manager, last_query,
                                                       self.node_id * 2, best_split.false_example_id_list,
                                                       best_split.false_average, best_split.false_variance,
                                                       best_split.false_exp_average, self.dao_suffix, self.min_variance)

                    self.false_child._induce_tree_recursive(level + 1, iteration)

        # cdef bint is_power_of_two = not (self.node_id & (self.node_id - 1))
        # if is_power_of_two and self.is_leaf():
        #     if MyGlobals.USE_CLASSIFICATION_TREE:
        #         # this node is false all the way from the root to the leaf
        #         self.average, self.variance, self.exp_average = -1.0, 0.0, 0.36787944117144233
        #         # self.average, self.variance, self.exp_average = 0.0, 0.0, 1.0
        #     else:
        #         self.average, self.variance, self.exp_average = 0.0, 0.0, 1.0

        return 0

    cdef long double calculate_foldt(self, FOLPathExample example) except 1e10:
        """
        This method calculate the predicted value of the input example,
        using the already induced first-order logic decision tree
        :rtype: float
        """
        cdef long double result
        cdef bint is_unified, don_t_care_included

        if self.is_leaf():
            result = self.average
            # print("Tree node: %s | Score: %s" % (bin(self.node_id), result))
        else:
            is_unified, don_t_care_included = example.check_query(self.query)
            if don_t_care_included:
                result = self.average                                   # ---> example satisfies query with "don't care" values
            elif is_unified:
                result = self.true_child.calculate_foldt(example)       # ---> example satisfies query
            else:
                result = self.false_child.calculate_foldt(example)      # ---> example does NOT satisfy query

        return result

    cdef dict _convert_to_dictionary(self):
        """This method converts the regression tree to Python dictionary-type object, which looks like a json object
        :rtype: dict
        """
        cdef dict true_json
        cdef dict false_json
        cdef list atom_list

        true_json = self.true_child._convert_to_dictionary() if self.true_child else None
        false_json = self.false_child._convert_to_dictionary() if self.false_child else None
        atom_list = self.query.atom_list if self.query else []

        return {'average': self.average, 'variance': self.variance, 'exp_average': self.exp_average,
                'query': atom_list, 'true_child': true_json, 'false_child': false_json}

    cdef bytes _convert_to_json_string(self):
        """This method converts the regression tree to a json string.
        :rtype: bytes
        """
        cdef bytes json_string = <bytes> json.dumps(self._convert_to_dictionary())
        return json_string

    cdef int _build_from_dictionary(self, dict json_dictionary, int node_id=1) except -1:
        """
        This method builds a regression tree based on the input Python dictionary, which looks like a json object.
        """
        from math import exp

        self.average = json_dictionary['average']
        self.variance = json_dictionary['variance']
        self.exp_average = json_dictionary['exp_average'] if 'exp_average' in json_dictionary else exp(self.average)
        self.query = FOLQuery(atom_list=json_dictionary['query'], convert_utf_to_str=True)
        self.node_id = node_id

        if json_dictionary['true_child']:
            #If json_dictionary['true_child'] is not None, then recursively build the true child tree.
            self.true_child = FOLDecisionTree()
            self.true_child._build_from_dictionary(json_dictionary['true_child'], node_id * 2 + 1)

        if json_dictionary['false_child']:
            #If json_dictionary['false_child'] is not None, then recursively build the false child tree.
            self.false_child = FOLDecisionTree()
            self.false_child._build_from_dictionary(json_dictionary['false_child'], node_id * 2)

        return 0

    cdef int _build_from_json_string(self, bytes json_string) except -1:
        """This method builds a regression tree based on the input json string."""
        self._build_from_dictionary(json.loads(json_string))
        return 0

    cdef int save_tree_to_db(self, int sf_id, bytes target_f_char_id, int group_id, int tree_index) except -1:
        cdef bint close_connection
        cdef bytes json_string
        cdef object dao

        dao = MyDAO(suffix=self.dao_suffix, create_db_connection=False)
        json_string = self._convert_to_json_string()

        dao.save_tree_to_db(sf_id, target_f_char_id, group_id, tree_index, json_string)
        dao = None

        return 0

    cdef int load_tree_from_db(self, bytes target_f_char_id, int group_id, int tree_index) except -1:
        cdef bint close_connection
        cdef object dao
        cdef bytes tree_json_dump

        dao = MyDAO(suffix=self.dao_suffix, create_db_connection=False)
        tree_json_dump = dao.load_tree_from_db(target_f_char_id, group_id, tree_index)
        dao = None

        if tree_json_dump:
            self._build_from_json_string(tree_json_dump)
            return 1
        else:
            return 0
