
from MainClasses.FirstOrderLogic.FOLPathExample cimport FOLPathExample
from MainClasses.FirstOrderLogic.FOLQuery cimport FOLQuery

cdef class FOLClassificationTree:
    """
    class FOLDecisionTree: this class represents the first-order logic decision tree
    that will emulate the field function of the CRF
    """

    cdef public int node_id
    cdef public FOLClassificationTree true_child
    cdef public FOLClassificationTree false_child
    cdef public bytes dao_suffix
    cdef public FOLQuery query
    cdef public int group_id
    cdef public list example_id_list
    cdef public object rmode_manager
    cdef public long double coverage
    cdef public list false_example_id_list

    cdef bint is_leaf(self)
    cdef int feed_example(self, FOLPathExample example, object dao_work_manager_queue=*) except -1
    cdef int induce_tree_from_root(self) except -1
    cdef int _induce_tree_recursive(self, int level, int total_example_list_length) except -1
    cdef int check_example(self, FOLPathExample example) except -1
