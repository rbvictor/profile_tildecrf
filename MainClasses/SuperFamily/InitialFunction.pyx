__author__ = 'romulo'


cdef class InitialFunction:
    def __cinit__(self, dict log_prob_dict):
        self.log_prob_dict = log_prob_dict

    def __getstate__(self):
        state_dict = {"log_prob_dict": self.log_prob_dict}
        return state_dict

    def __setstate__(self, state_dict):
        self.log_prob_dict = state_dict["log_prob_dict"]

    def __reduce__(self):
        init_args = (self.log_prob_dict,)
        return InitialFunction, init_args, self.__getstate__()

    cdef long double execute(self, FOLPathExample example) except 1:
        cdef int position = example.path_position
        cdef str current_state, current_aa, current_ss
        cdef str last_state, last_aa, last_ss
        cdef long double score
        
        current_state, current_aa, current_ss = example.path_list[position]
        last_state, last_aa, last_ss = example.path_list[position-1]

        # print("path_list: " + str(example.path_list))

        cdef str _current_state = current_state if current_state in self.log_prob_dict else ''
        cdef str _last_state = last_state if last_state in self.log_prob_dict[_current_state] else ''

        score = self.log_prob_dict[_current_state][current_aa] + self.log_prob_dict[_current_state][_last_state]

        if score > 0.0:
            print (_current_state, current_aa, self.log_prob_dict[_current_state][current_aa], '|',
                    _current_state, _last_state, self.log_prob_dict[_current_state][_last_state])

        return score