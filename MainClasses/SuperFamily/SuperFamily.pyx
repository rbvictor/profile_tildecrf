#!/usr/bin/env python

"""
Python Profile CRF Project - SuperFamily.py
"""

__author__ = "Romulo Barroso Victor"
__credits__ = ["Romulo Barroso Victor", "Gerson Zaverucha", "Juliana Bernardes"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Romulo Barroso Victor"
__email__ = "rbvictor@cos.ufrj.br"
__status__ = "Development"
__date__ = "10-Jul-2013"

import multiprocessing
import MyGlobals
from MainClasses.SuperFamily.InitialFunctionFactory import InitialFunctionFactory
from MainClasses.DAO.MyDAO import MyDAO
from MainClasses.DynamicObjects.FieldFunction cimport FieldFunction


class SuperFamily():
    """
    This class extends the Family class and includes more attributes and methods that are specific to the superfamilies.
    """

    def __init__(self, rmode_manager, sf_id, target_f_char_id, dao_suffix, nb_of_match_states=None,
                 initial_value_function_type=None):
        """SuperFamily constructor"""

        # [STEP 1.2.3.1]

        # Family.__init__(self, sf_id)
        self.sf_id = sf_id
        self.nb_of_match_states = nb_of_match_states  # number of match states
                                                      # match states belonging to the super family ancestral protein
        # These are field functions belonging to this superfamily. Each superfamily has its own field functions.
        self.ff_dict = {}
        self.rmode_manager = rmode_manager
        self.field_functions_ready = False
        self.dao_suffix = dao_suffix
        self.state_aa_count_dict = {}
        self.from_state_to_state_count_dict = {}
        self.target_f_char_id = target_f_char_id
        self.marked_positions = None
        self._set_marked_positions()
        self.initial_value_function_type = initial_value_function_type \
            if initial_value_function_type is not None else MyGlobals.INITIAL_VALUE_FUNCTION_TYPE


    def initialize_field_functions(self, bint enable_insert_ff=True, list dao_work_manager_queue_list=None):

        #if self.initial_value_function_type == 'pseudocount':
        #    initial_value_function = PseudocountFunction(self.amino_acid_count_dict, self.state_transition_count_dict)

        if self.initial_value_function_type == 'score':
            init_factory = InitialFunctionFactory('score', self.state_aa_count_dict)

        elif self.initial_value_function_type == 'hmm':
            init_factory = InitialFunctionFactory('hmm', self.target_f_char_id)
            self.nb_of_match_states = init_factory.nb_match_states

        else:
            init_factory = InitialFunctionFactory('none', None)

        if not dao_work_manager_queue_list:
            dao_work_manager_queue_list = [None]
            n_queues = 1
        else:
            n_queues = len(dao_work_manager_queue_list)

        self.ff_dict[0] = {1000 + i: FieldFunction(1000 + i, rmode_manager=self.rmode_manager, dao_suffix=self.dao_suffix,
                                                   initial_value_function=init_factory.create_function(1000 + i),
                                                   ff_type='match',
                                                   dao_work_manager_queue=dao_work_manager_queue_list[i % n_queues])
                           for i in range(1, self.nb_of_match_states + 1)}

        self.ff_dict[1] = {i: FieldFunction(i, rmode_manager=self.rmode_manager, dao_suffix=self.dao_suffix,
                                            default_value=0.0, ff_type='insert',
                                            dao_work_manager_queue=dao_work_manager_queue_list[i % n_queues])
                              if not enable_insert_ff
                              else FieldFunction(i, rmode_manager=self.rmode_manager, dao_suffix=self.dao_suffix,
                                                 initial_value_function=init_factory.create_function(i), ff_type='insert',
                                                 dao_work_manager_queue=dao_work_manager_queue_list[i % n_queues])
                              for i in xrange(self.nb_of_match_states + 1)}

    def fetch_sequences(self, num_of_sequences=None):
        """this method returns the next sequence sequence loaded from db, using the method select_sequences_from_db"""

        dao = MyDAO(suffix=self.dao_suffix)
        dao.select_super_family_sequences(self.sf_id, self.target_f_char_id)
        return dao.fetch_all_sequences()

    def get_field_functions_dict(self):
        """This method returns a tuple containing two collections: match_field_functions and insert_field_functions."""
        return self.ff_dict

    def get_profile_classes(self):
        """This method returns the profile classes (match states) belonging to this superfamily."""
        return ["M%d" % (x+1) for x in xrange(self.nb_of_match_states)]

    def load_field_functions_ff_dict(self, ff_dict):
        """
        This method saves into the object attributes a tuple containing two collections:
        match_field_functions and insert_field_functions.
        """
        self.ff_dict = ff_dict

    def _parallel_update_field_functions(self, ff_dict, ff_type_list, is_classification=False):
        global _SuperFamily__run_update_ff

        mng = multiprocessing.Manager()
        process_count = 0
        ff_queue = mng.Queue()

        len_ff_dict = len(ff_dict)
        process_dict = {i : {} for i in xrange(len_ff_dict)}
        new_ff_dict = {i : {} for i in xrange(len_ff_dict)}

        max_size_of_processes = MyGlobals.NUM_PARALLEL_UPDATE_FF

        print("Check it: %s" % str(ff_dict.keys()))

        for i in ff_dict:
            ns = mng.Namespace()
            ns.count = 0
            len_ff_dict_i = len(ff_dict[i])
            keys = ff_dict[i].keys()
            middle_ff_index = (min(keys) + max(keys)) / 2
            ff_index_list = sorted(keys, key=lambda x: abs(x - middle_ff_index))

            iterable_args = ((ff_dict[i][ff_index], i, ff_index, ff_type_list[i], ns, ff_queue, len_ff_dict_i,
                              self.target_f_char_id, self.sf_id, is_classification)
                             for ff_index in ff_index_list)

            for t_args in iterable_args:
                ff_list_index = t_args[1]
                ff_index = t_args[2]
                p = multiprocessing.Process(target=_SuperFamily__run_update_ff, args=t_args)
                p.start()
                process_dict[ff_list_index][ff_index] = p
                process_count += 1

                if process_count >= max_size_of_processes:
                    # print(".... Queue size: %d" % ff_queue.qsize())
                    ff_list_index, ff_index, ff = ff_queue.get()
                    # print((ff_list_index, ff_index))
                    new_ff_dict[ff_list_index][ff_index] = ff
                    ff_queue.task_done()
                    p = process_dict[ff_list_index].pop(ff_index)
                    p.join()
                    process_count -= 1

        while process_count > 0:
            # print(".... Queue size: %d" % ff_queue.qsize())
            ff_list_index, ff_index, ff = ff_queue.get()
            # print((ff_list_index, ff_index))
            new_ff_dict[ff_list_index][ff_index] = ff
            ff_queue.task_done()
            p = process_dict[ff_list_index].pop(ff_index)
            p.join()
            process_count -= 1

        ff_queue.join()
        return new_ff_dict

    def update_field_functions(self, neg_example_queue=None, is_classification=False):
        """
        This method triggers the update of all field functions (and its regression trees) belonging to this
        superfamily.
        """

        # self.dao.create_index_of_all_tables(lite=True)
        self.ff_dict = self._parallel_update_field_functions(self.ff_dict, ["match", "insert"], is_classification)
        
        if neg_example_queue and not is_classification:
            self._update_ff_negative_score(neg_example_queue)
            
        self.field_functions_ready = True
        
    def _update_ff_negative_score(self, neg_example_queue):
        neg_example_count_dict = {0: {}, 1: {}}
        neg_example_log_score_dict = {0: {}, 1: {}}

        while not neg_example_queue.empty():
            q_neg_example_count_dict, q_neg_example_log_score_dict, q_neg_example_exp_score_dict = neg_example_queue.get()

            for is_insert in q_neg_example_count_dict:
                for ff_group_id in q_neg_example_count_dict[is_insert]:
                    neg_example_count_dict[is_insert][ff_group_id] = \
                        neg_example_count_dict[is_insert].setdefault(ff_group_id, 0) + q_neg_example_count_dict[is_insert][ff_group_id]

                    neg_example_log_score_dict[is_insert][ff_group_id] = \
                        neg_example_log_score_dict[is_insert].setdefault(ff_group_id, 0.0) + q_neg_example_log_score_dict[is_insert][ff_group_id]

            neg_example_queue.task_done()

        for is_insert in neg_example_count_dict:
            for ff_group_id in neg_example_count_dict[is_insert]:
                neg_log_score = float(neg_example_log_score_dict[is_insert][ff_group_id])
                neg_count = float(neg_example_count_dict[is_insert][ff_group_id])
                (<FieldFunction> self.ff_dict[is_insert][ff_group_id]).add_negative_score(neg_log_score / neg_count)

    def save_field_functions_to_db(self):
        cdef FieldFunction ff

        # [STEP 1.2.3.3]
        for key in self.ff_dict:
            for ff in self.ff_dict[key].values():
                ff.save_trees_to_db(self.sf_id, self.target_f_char_id)

    def load_field_functions_from_db(self, int num_trees):
        cdef FieldFunction ff

        self.initialize_field_functions(enable_insert_ff=MyGlobals.ENABLE_INSERT_FIELD_FUNCTIONS)
        for key in self.ff_dict:
            for ff in self.ff_dict[key].values():
                ff.load_trees_from_db(self.target_f_char_id, num_trees)

    def _set_marked_positions(self, gap_ratio=0.5):
        dao = MyDAO(suffix=self.dao_suffix)
        align_seq_marked_pos_list = dao.get_super_family_aligned_sequences(self.sf_id, self.target_f_char_id)

        list_of_aligned_sequences, list_of_marked_positions = zip(*align_seq_marked_pos_list)
        self.marked_positions = None

        if list_of_marked_positions and bool(list_of_marked_positions[0]):
            # if marked positions already exist in database then just assign it !!!!!!!!!!!!!!!!!!
            self.marked_positions = list_of_marked_positions[0]

        elif list_of_aligned_sequences:
            max_gaps = len(list_of_aligned_sequences) * gap_ratio
            marked_positions_list = (['1' if len([x for x in column if x == '-']) <= max_gaps else '0'
                                      for column in zip(*list_of_aligned_sequences)])
            self.marked_positions = ''.join(marked_positions_list)
            # print("Marked positions: " + str(marked_positions))

        if self.marked_positions:
            marked_positions = self.marked_positions
            self.nb_of_match_states = list(marked_positions).count('1')

            state_transitions = []
            state_amino_acid_list = []
            n_states = 0

            for aligned_sequence in list_of_aligned_sequences:
                sequence_states = ['M' if int(marked_positions[i]) and aligned_sequence[i] != '-' else
                                   ('D' if int(marked_positions[i]) and aligned_sequence[i] == '-' else
                                    ('I' if not int(marked_positions[i]) and aligned_sequence[i] != '-' else '')
                                    )
                                   for i in xrange(len(aligned_sequence))
                                   if int(marked_positions[i]) or aligned_sequence[i] != '-']

                n_states = len(sequence_states)

                state_index_increments = [int(sequence_state in ['M', 'D']) for sequence_state in sequence_states]
                state_indexes = [sum(state_index_increments[:i + 1]) for i in xrange(n_states)]

                sequence_states = ["%s%d" % (sequence_state, state_index)
                                   for sequence_state, state_index in zip(sequence_states, state_indexes)]

                state_amino_acid_list.extend(zip(sequence_states,
                                                 [aligned_sequence[i] for i in xrange(len(aligned_sequence))
                                                  if int(marked_positions[i]) or aligned_sequence[i] != '-']))

                state_transitions.extend([('B0', sequence_states[0])] +
                                         [(sequence_states[i], sequence_states[i + 1]) for i in xrange(n_states - 1)])

            state_amino_acid_list.sort()
            state_transitions.sort()
            aa_list = MyGlobals.AMINO_ACID_LIST

            state_list = ['B0', 'I0'] + ['%s%d' % (c, i) for i in range(1, n_states + 1) for c in ['M','D','I']]

            self.state_aa_count_dict = \
                {state : {aa : state_amino_acid_list.count((state, aa))
                          for aa in aa_list}
                 for state in state_list}

            self.from_state_to_state_count_dict = \
                {from_state : {to_state : state_amino_acid_list.count((from_state, to_state))
                               for to_state in ["I%d" % int(from_state[1:]), "M%d" % (int(from_state[1:]) + 1),
                                                "D%d" % (int(from_state[1:]) + 1)]}
                 for from_state in state_list}


cpdef _SuperFamily__run_update_ff(FieldFunction field_function, int ff_list_index, int ff_index, bytes ff_type,
                                  object ns, object ff_queue, int ff_count, bytes target_f_char_id, int sf_id,
                                  bint is_classification):

    ns.count += 1
    cdef int count = ns.count
    print("----> Updating %s function #%d (%d of %d) <<< ..." % (ff_type, ff_index, count, ff_count))

    if is_classification:
        field_function.update_classification_function()
    else:
        field_function.update_function()
        field_function.save_last_tree_to_db(sf_id, target_f_char_id)

    ff_queue.put((ff_list_index, ff_index, field_function))

    print("<---- Finished updating %s function #%d (%d of %d) <<< ..." % (ff_type, ff_index, count, ff_count))