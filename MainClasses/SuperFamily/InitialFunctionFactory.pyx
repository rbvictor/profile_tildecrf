__author__ = 'rbvictor'

import MyGlobals
from MainClasses.SuperFamily.InitialFunction cimport InitialFunction
from math import exp, log

class InitialFunctionFactory:

    def __init__(self, str init_func_type, *args):
        self.init_func_type = init_func_type
        self.nb_match_states = None

        if init_func_type == 'hmm':
            self.nb_match_states, self.log_prob_dict = self._get_log_prob_dict_from_hmm(*args)
        elif init_func_type == 'score':
            self.nb_match_states, self.log_prob_dict = self._get_log_prob_dict_from_count(*args)
        else:
            self.nb_match_states, self.log_prob_dict = None, None

        #import pprint
        #pprint.pprint(self.log_prob_dict)
        if self.log_prob_dict:
            for state in self.log_prob_dict:
                if state and state[0] in ('m', 'i'):

                    aa_b_value = exp(self.log_prob_dict[state]['n'] + exp(self.log_prob_dict[state]['d']))
                    self.log_prob_dict[state]['b'] = log(aa_b_value) if aa_b_value > 0 else -MyGlobals.MAX_FLOAT_VALUE

                    aa_z_value = exp(self.log_prob_dict[state]['e'] + exp(self.log_prob_dict[state]['q']))
                    self.log_prob_dict[state]['z'] = log(aa_z_value) if aa_z_value > 0 else -MyGlobals.MAX_FLOAT_VALUE

                    aa_j_value = exp(self.log_prob_dict[state]['i'] + exp(self.log_prob_dict[state]['l']))
                    self.log_prob_dict[state]['j'] = log(aa_j_value) if aa_j_value > 0 else -MyGlobals.MAX_FLOAT_VALUE

                    self.log_prob_dict[state]['x'] = 0.0

    def create_function(self, int group_id):
        cdef bint is_match_or_delete
        cdef int st_index
        cdef list st_letters
        cdef dict log_prob_dict
        cdef str l

        if self.log_prob_dict is None:
            return None

        else:
            is_match_or_delete = bool(group_id / 1000)
            st_index = group_id % 1000
            st_letters = ['m', 'd'] if is_match_or_delete else ['i']
            log_prob_dict = {}

            for l in st_letters:
                state = '%s%d' % (l, st_index)
                log_prob_dict[state] = self.log_prob_dict[state].copy()

            return InitialFunction(log_prob_dict)


    @staticmethod
    def _get_log_prob_dict_from_count(*args):
        from MainClasses.Priors.Blosum62 import Blosum62decimal_dict
        from MainClasses.Priors.BackgroundProbability import Q_prob

        state_aa_count_dict = args[0]
        constant_a = MyGlobals.PSEUDOCOUNT_CONST_A
        distribution_q = Q_prob
        sum_state_aa_count_dict = {state: sum(aa_count_dict.values())
                                   for state, aa_count_dict in state_aa_count_dict.items()}

        log_prob_dict = {}
        nb_matches = 0

        for state in state_aa_count_dict:
            state_aa_ratios = {}
            st_index = int(state[1:])
            nb_matches = max(st_index, nb_matches)

            if state[0].upper() == 'd':
                state_aa_ratios['-'] = 1.0
            else:
                for aa in MyGlobals.AMINO_ACID_LIST[:-1]:
                    numerator = state_aa_count_dict[state].setdefault(aa, 0) + constant_a * distribution_q[aa]
                    denominator = sum_state_aa_count_dict[state] + constant_a
                    state_aa_ratios[aa] = numerator * 1.0 / denominator

            log_prob_dict[state] = {}
            for aa in state_aa_ratios:
                blosum_scores = Blosum62decimal_dict[aa]
                log_prob_dict[state][aa] = log(sum([state_aa_ratios[_aa] * blosum_scores[_aa] for _aa in state_aa_ratios]))

            log_prob_dict[state][''] = 0.0

        log_prob_dict[''] = {aa: log(distribution_q[aa]) for aa in distribution_q}
        log_prob_dict[''][''] = 0.0

        if len(args) > 1:
            from_state_to_state_count_dict = args[1]
            sum_from_state_to_state_count_dict = {from_state: sum(to_state_count_dict.values())
                                                  for from_state, to_state_count_dict in from_state_to_state_count_dict.items()}

            for from_state in from_state_to_state_count_dict:
                for to_state in from_state_to_state_count_dict[from_state]:
                    numerator = from_state_to_state_count_dict[from_state][to_state] + constant_a
                    denominator = sum_from_state_to_state_count_dict[from_state] \
                                  + len(from_state_to_state_count_dict[from_state]) * constant_a
                    log_prob_dict[to_state][from_state] = log(numerator * 1.0 / denominator)

        return nb_matches, log_prob_dict

    @staticmethod
    def _get_log_prob_dict_from_hmm(*args):

        target_f_char_id = args[0]

        hmm_dict = {}
        tmp_folder = "%s%s" % (MyGlobals.get_current_dir(), MyGlobals.TMP_FOLDER)
        hmm_filename = tmp_folder + MyGlobals.HMMBUILD_OUTPUT_FILENAME_FORMAT % target_f_char_id

        aa_header = ['a', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'k', 'l',
                     'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'y']
                    #m->m     m->i     m->d     i->m     i->i     d->m     d->d

        begin_st_header = [(('begin%d', 0), ('m%d', 1)), (('begin%d', 0), ('i%d', 0)), (('begin%d', 0), ('d%d', 1)),
                           (('i%d', 0), ('m%d', 1)), (('i%d', 0), ('i%d', 0))]

        other_st_header = [(('m%d', 0), ('m%d', 1)), (('m%d', 0), ('i%d', 0)), (('m%d', 0), ('d%d', 1)),
                           (('i%d', 0), ('m%d', 1)), (('i%d', 0), ('i%d', 0)), (('d%d', 0), ('m%d', 1)),
                           (('d%d', 0), ('d%d', 1))]

        n_aa = len(aa_header)

        with open(hmm_filename, 'r') as f:
            start_saving = False
            line_index = 0
            hmm_index = 0

            for line in f:
                line_split = line.replace('*', str(MyGlobals.HMM_MAX_FLOAT)).split()

                if start_saving:
                    line_index = (line_index + 1) % 3

                    if line_split[0] == "//":
                        hmm_dict['end'] = hmm_dict.pop('m%d' % (hmm_index + 1))
                        break

                    elif line_index == 0 or line_index == 1:
                        if line_index == 0:
                            hmm_index = int(line_split.pop(0))
                            hmm_dict.setdefault('d%d' % hmm_index, {})['-'] = 0.0

                        dict_to_update = {aa_header[i]: -float(line_split[i]) for i in xrange(n_aa)}
                        hmm_dict.setdefault('%s%d' % ('i' if line_index else 'm', hmm_index), {}).update(dict_to_update)

                    elif line_index == 2:
                        st_header = begin_st_header if hmm_index == 0 else other_st_header

                        for i in xrange(len(st_header)):
                            (src_st, src_i), (dst_st, dst_i) = st_header[i]
                            hmm_dict.setdefault(dst_st % (hmm_index + dst_i), {})[src_st % hmm_index] = -float(line_split[i])
                            hmm_dict[dst_st % (hmm_index + dst_i)][''] = -float(str(MyGlobals.HMM_MAX_FLOAT))

                elif line_split[0] == "COMPO":
                    start_saving = True

        return hmm_index, hmm_dict


