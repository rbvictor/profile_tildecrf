__author__ = 'Romulo Barroso Victor'

# 1- small {A,G,S,T}
# 2- polar {D,E,H,K,N,Q,R,S,T,W,Y} 
# 3- polar uncharged {N,Q}                          
# 4- aromatic {F,H,W,Y}
# 5- charged {D,E,H,I,K,L,R,V}
# 6- positively charged {H,K,R}
# 7- negatively charged {D,E} 
# 8- tiny {A,G}  
# 9- bulky {F,H,R,W,Y}  
# 10- aliphatic {I,L,V}    
# 11- hydrophobic {I,L,M,V} 
# 12- hydrophilic basic {K,R,H}  
# 13- hydrophilic acidic {E,D,N,Q}    
# 14- neutral weakly hydrophobic {A,G,P,S, T}    
# 15- hydrophobic aromatic {F,W,Y}     
# 16- acidic {E,D}


type_to_aa_dict = {"small": ['A', 'G', 'S', 'T'], 
                   "polar": ['D', 'E', 'H', 'K', 'N', 'Q', 'R', 'S', 'T', 'W', 'Y'],
                   "polar_uncharged": ['N', 'Q'], 
                   "aromatic": ['F', 'H', 'W', 'Y'],
                   "charged": ['D', 'E', 'H', 'I', 'K', 'L', 'R', 'V'], 
                   "positively_charged": ['H', 'K', 'R'],
                   "negatively_charged": ['D', 'E'], 
                   "tiny": ['A', 'G'], 
                   "bulky": ['F', 'H', 'R', 'W', 'Y'],
                   "aliphatic": ['I', 'L', 'V'], 
                   "hydrophobic": ['I', 'L', 'M', 'V'], 
                   "hydrophilic_basic": ['K', 'R', 'H'],
                   "hydrophilic_acidic": ['E', 'D', 'N', 'Q'], 
                   "neutral_weakly_hydrophobic": ['A', 'G', 'P', 'S', 'T'],
                   "hydrophobic_aromatic": ['F', 'W', 'Y'], 
                   "acidic": ['E', 'D']}

for aa_type in type_to_aa_dict:
    if 'N' in type_to_aa_dict[aa_type] or 'D' in type_to_aa_dict[aa_type]:
        type_to_aa_dict[aa_type].append('B')
    if 'E' in type_to_aa_dict[aa_type] or 'Q' in type_to_aa_dict[aa_type]:
        type_to_aa_dict[aa_type].append('Z')
    if 'I' in type_to_aa_dict[aa_type] or 'L' in type_to_aa_dict[aa_type]:
        type_to_aa_dict[aa_type].append('J')
    type_to_aa_dict[aa_type].append('X')

aa_to_type_dict = {}
for aa_type in type_to_aa_dict:
    for aa in type_to_aa_dict[aa_type]:

        if aa not in aa_to_type_dict:

            aa_type_list = []
            aa_to_type_dict[aa] = aa_type_list
            aa_to_type_dict[aa.lower()] = aa_type_list

        aa_to_type_dict[aa].append(aa_type)