__author__ = 'rbvictor'


Blocks9 = [  # Ratio (r) of amino acid frequency relative to background frequency
    [('sat', 2), ('cgp', 1), ('nvm', 0.25), ('qhrikfldw', 0.25), ('ey', 0.125)],             # 1
    [('y', 8), ('fw', 4), ('h', 2), ('lm', 0.5), ('nqicvsr', 0.25), ('tpakdge', 0.125)],     # 2
    [('qe', 2), ('knrshdta', 1), ('mpyg', 0.5), ('vliwcf', 0.25)],                           # 3
    [('kr', 4), ('q', 2), ('h', 1), ('netms', 0.5), ('pwyalgvci', 0.25), ('df', 0.125)],     # 4
    [('lm', 4), ('i', 2), ('fv', 1), ('wyctq', 0.25), ('aphr', 0.125), ('ksendg', 0)],       # 5
    [('iv', 4), ('lm', 1), ('cta', 0.5), ('f', 0.25), ('yspwn', 0.125), ('edqkrdgh', 0)],    # 6
    [('d', 4), ('en', 2), ('qhs', 1), ('kgpta', 0.5), ('ry', 0.25), ('mylfwic', 0.125)],     # 7
    [('m', 2), ('ivlftyca', 1), ('wshqrnk', 0.5), ('peg', 0.25), ('d', 0.125)],              # 8
    [('pgw', 2), ('chrde', 1), ('nqkfytlam', 0.5), ('svi', 0.25)]                            # 9
]

Blocks9_dicts = [dict([(aa, score) for aa_string, score in Blocks9[i] for aa in aa_string])
                for i in xrange(len(Blocks9))]

# Component 1 favors small neutral residues.
# Component 2 favors the aromatics.
# Component 3 gives high probability to most of the polar residues (except for C, Y and W).
# Component 4 gives high probability to positively charged amino acids and residues with NH2 groups.
# Component 5 gives high probability to residues that are aliphatic or large and non-polar.
# Component 6 prefers I and V (aliphatic residues commonly found in /? sheets), and allows substitutions with L and M.
# Component 7 gives high probability to negatively charged residues, allowing substitutions with certain of the
#             hydrophilic polar residues.
# Component 8 gives high probability to uncharged hydrophobics, with the exception of glycine.
# Component 9 gives high probability to distributions peaked around individual amino acids (especially P, G, W and C)
