#!/usr/bin/env python

"""
Python Profile CRF Project - LearningManager.py
"""
import MyGlobals

__author__ = "Romulo Barroso Victor"
__credits__ = ["Romulo Barroso Victor", "Gerson Zaverucha", "Juliana Bernardes"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Romulo Barroso Victor"
__email__ = "rbvictor@cos.ufrj.br"
__status__ = "Development"
__date__ = "10-Jul-2013"

import multiprocessing
import MainClasses.AucRoc as AucRoc
from MainClasses.DAO.MyDAO import MyDAO
from MainClasses import Logger
from MainClasses.SuperFamily.SuperFamily import SuperFamily
from MainClasses.DynamicObjects.Trellis cimport Trellis


class LearningManager:
    """This class consists of a learning manager whose methods execute the necessary training procedures."""

    def __init__(self, rmode_manager, file_loaded=False):
        """LearningManager constructor"""

        # [STEP 1.2]
        self.file_loaded = file_loaded
        self.rmode_manager = rmode_manager
        self.auc_dict = {}
        self.logger = Logger.Logger()

    @staticmethod
    def clear_results_folder():
        import os
        results_folder_path = "%s%s" % (MyGlobals.get_current_dir(), MyGlobals.RESULTS_FOLDER)

        if os.path.exists(results_folder_path):
            import shutil
            shutil.rmtree(results_folder_path)
        os.makedirs(results_folder_path)

    @staticmethod
    def clear_sqlite3_db_folder():
        import os
        sqlite3_db_folder = MyGlobals.SQLITE3_DB_FOLDER

        if MyGlobals.USE_SQLITE3 and os.path.exists(sqlite3_db_folder):
            import shutil
            shutil.rmtree(sqlite3_db_folder)

        os.makedirs(sqlite3_db_folder)

    def get_super_families_from_db(self):
        print("-> Loading super families in database ...........................................")

        # [STEP 1.2.1]
        super_family_list = []

        for (sf_id, target_f_char_id, nb_of_match_states, num_trees) in MyDAO().get_saved_trees_info_from_db():
            dao_suffix = "_tf_%s" % str(target_f_char_id).replace('.', '_')
            super_family = SuperFamily(self.rmode_manager, sf_id, target_f_char_id, dao_suffix, nb_of_match_states)
            super_family.initialize_field_functions()
            super_family.load_field_functions_from_db(num_trees)

            super_family_list.append(super_family)

        return super_family_list

    def load_sequence_file_to_db(self, filename):
        # [STEP 1.2.2]
        import os
        if os.path.exists(filename):
            print(">>>> Loading sequence file into database...")
            MyDAO().load_sequence_file_to_db(filename)
            self.file_loaded = True

    @staticmethod
    def load_ss_file_to_db(filename):
        import os
        if os.path.exists(filename):
            print(">>>> Loading secondary structure file into database...")
            MyDAO().load_ss_file_to_db(filename)

    @staticmethod
    def load_training_info_table(filename):
        import os
        if os.path.exists(filename):
            print(">>>> Loading training info into database...")
            MyDAO().load_training_info_table(filename)

    @staticmethod
    def load_alignments_to_db(run_hmmer=False):
        print(">>>> Loading alignments into database...")
        MyDAO().load_alignments_to_db(run_hmmer)

    @staticmethod
    def reset_ff_and_results_tables(load_trees):
        cdef object dao = MyDAO()

        if not load_trees:
            print(">>>> Resetting FIELD_FUNCTIONS table...")
            dao.create_field_function_table()

        print(">>>> Resetting RESULTS table...")
        dao.create_results_table()
        dao = None

    def execute_training_and_test(self, nb_of_iterations, batch_size, target_family_index, nb_of_target_families,
                                  load_trees):

        """This method triggers the training procedure
           for all super families from the database or loaded from a file."""

        # [STEP 1.2.3]
        self.clear_results_folder()
        self.auc_dict = {}

        if load_trees:
            super_family_list = self.get_super_families_from_db()

            for super_family in super_family_list:
                results, auc = self.evaluate_training(super_family, 0)
                self.logger.print_results(results, super_family.target_f_char_id, auc, 0)
                self.auc_dict.setdefault((super_family.sf_id, super_family.target_f_char_id), []).append(auc)

        else:
            dao = MyDAO()

            if MyGlobals.USE_SQLITE3:
                self.clear_sqlite3_db_folder()
            else:
                dao.drop_tables(lite=False)

            training_info_list = dao.get_training_info_list()

            if nb_of_target_families is None:
                training_info_list = training_info_list[target_family_index:]
            else:
                training_info_list = training_info_list[target_family_index: (target_family_index + nb_of_target_families)]

            nb_of_match_states_dict = {(sf_id, target_f_char_id): dao.get_super_family_average_length(sf_id, target_f_char_id)
                                        for sf_id, target_f_char_id in training_info_list}
            dao = None
            print(":: Training info list: %s" % str(training_info_list))
            for sf_id, target_f_char_id in training_info_list:
                nb_of_match_states = nb_of_match_states_dict[(sf_id, target_f_char_id)]

                super_family = SuperFamily(self.rmode_manager, sf_id, target_f_char_id,
                                           ("_tf_%s" % str(target_f_char_id).replace('.', '_')),
                                           nb_of_match_states, MyGlobals.INITIAL_VALUE_FUNCTION_TYPE)

                self.train_and_test_super_family(super_family, target_f_char_id, nb_of_iterations, batch_size)

                #print("-> Saving superfamily in database ............................................")
                #super_family.save_field_functions_to_db()

        self.logger.print_overall_results(self.auc_dict)
    # -- end execute_training --

    def train_and_test_super_family(self, super_family, target_f_char_id, nb_of_iterations, batch_size, baum_welch=None):
        """
        This method triggers the training procedure for a given superfamily of sequences,
        given a target family to be identified.
        If batch is True, the field functions of a given superfamily will be updated at once,
        after all sequences belonging to this superfamily are processed.
        Otherwise, the field functions will be updated after each sequence is processed.
        """

        # [STEP 1.2.3.2]
        global _LearningManager__calculate_table
        from random import shuffle

        assert isinstance(super_family, SuperFamily)

        print("-> Targeting family ......................................................... # %s" % target_f_char_id)
        # super_family.set_target_family_id_list(target_family_id_list)

        if MyGlobals.USE_CLASSIFICATION_TREE:
            neg_example_queue = multiprocessing.Manager().Queue()
        else:
            neg_example_queue = None

        if MyGlobals.USE_MULTIPROCESSING and MyGlobals.USE_PP_IN_DAO:
            dao_work_manager_queue_list = [multiprocessing.Manager().Queue()
                                           for i in xrange(MyGlobals.NUM_OF_DAO_WORK_MANAGERS)]
        else:
            dao_work_manager_queue_list = []

        super_family.initialize_field_functions(enable_insert_ff=MyGlobals.ENABLE_INSERT_FIELD_FUNCTIONS,
                                                dao_work_manager_queue_list=dao_work_manager_queue_list)

        marked_positions = super_family.marked_positions
        profile_classes = super_family.get_profile_classes()
        dao_suffix = super_family.dao_suffix

        iteration_count = 0

        if MyGlobals.TEST_EVERY_ITERATION or MyGlobals.TEST_FIRST_ITERATION:
            results, auc = self.evaluate_training(super_family, iteration_count)
            self.logger.print_results(results, target_f_char_id, auc, iteration_count)
            self.auc_dict.setdefault((super_family.sf_id, target_f_char_id), []).append(auc)

        super_family_sequences = super_family.fetch_sequences()
        len_super_family_sequences = len(super_family_sequences)

        sf_seq_begin = 0
        sf_seq_end = batch_size

        #todo: test classification mode
        #todo: raise the number in blosum62 rmode

        for iteration_count in xrange(1, nb_of_iterations + 1):

            print("--> Iteration . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . # %d"
                  % iteration_count)

            is_classification = MyGlobals.USE_CLASSIFICATION_TREE and iteration_count == 1

            if is_classification:
                batch_sequences = super_family_sequences[:]

            else:
                if sf_seq_end > len_super_family_sequences:
                    new_super_family_sequences = super_family_sequences[:]
                    shuffle(new_super_family_sequences)
                    sf_seq_end -= len_super_family_sequences
                    batch_sequences = super_family_sequences[sf_seq_begin:] + new_super_family_sequences[0: sf_seq_end]
                    super_family_sequences = new_super_family_sequences

                else:
                    batch_sequences = super_family_sequences[sf_seq_begin: sf_seq_end]

                sf_seq_begin = sf_seq_end
                sf_seq_end += batch_size

                batch_sequences.sort(key=lambda seq: len(seq["aa_string"]), reverse=True)

            is_init = not super_family.field_functions_ready
            ff_dict = super_family.get_field_functions_dict()

            iterable_args = ((next_sequence, profile_classes, ff_dict, marked_positions,
                              dao_suffix, is_init, neg_example_queue, is_classification)
                              for next_sequence in batch_sequences
                              if next_sequence["f_char_id"] != target_f_char_id)
            processes = []

            dao_work_manager_list = []

            # if MyGlobals.USE_MULTIPROCESSING and MyGlobals.USE_PP_IN_DAO:
            from MainClasses.DAO.DAOWorkManager import DAOWorkManager

            dao_work_manager_list = [DAOWorkManager(suffix=super_family.dao_suffix, input_queue=dao_work_manager_queue)
                                     for dao_work_manager_queue in dao_work_manager_queue_list]

            for dao_work_manager in dao_work_manager_list:
                dao_work_manager.start()

            for t_args in iterable_args:
                # if MyGlobals.USE_MULTIPROCESSING and MyGlobals.USE_PP_IN_BATCHES:
                # else:
                #     _LearningManager__calculate_table(*t_args)

                p = multiprocessing.Process(target=_LearningManager__calculate_table, args=t_args)
                p.start()
                processes.append(p)

                if len(processes) >= MyGlobals.NUM_PARALLEL_SEQUENCES_IN_TRAINING:
                    p = processes.pop(0)
                    p.join()


            for p in processes:
                p.join()

            # if MyGlobals.USE_MULTIPROCESSING and MyGlobals.USE_PP_IN_DAO:
            for dao_work_manager in dao_work_manager_list:
                dao_work_manager.finish_jobs()

            print ("---> Joining inserting processes...")

            for dao_work_manager in dao_work_manager_list:
                dao_work_manager.join()

            dao_work_manager_list = []

            print("---> Updating field functions..........")
            super_family.update_field_functions(neg_example_queue, is_classification)

            if not is_classification and (MyGlobals.TEST_EVERY_ITERATION or iteration_count == nb_of_iterations):
                results, auc = self.evaluate_training(super_family, iteration_count)
                self.logger.print_results(results, target_f_char_id, auc, iteration_count)
                self.auc_dict.setdefault((super_family.sf_id, target_f_char_id), []).append(auc)

            if MyGlobals.USE_SQLITE3:
                self.clear_sqlite3_db_folder()
            else:
                MyDAO().drop_tables(lite=False)

        # super_family.fetch_sequences()  # empty MySQL buffer

    @staticmethod
    def evaluate_training(super_family, iteration_count):
        """
        This method compute the probabilities of the best alignments of the target family sequences in each
        superfamily match states, then it computes the final score.
        The superfamily containing the biggest probability is the one predicted by the algorithm as the target family's
        superfamily, as they share common ancestry.
        """
        # [STEP 1.2.4]

        target_f_char_id = super_family.target_f_char_id

        print("-> Evaluating family ........................................................ # %s"
              % str(target_f_char_id))

        use_multiprocessing = MyGlobals.USE_MULTIPROCESSING and MyGlobals.USE_PP_IN_BATCHES
        results = []
        sequence_list = MyDAO().get_testing_examples(target_f_char_id)
        sequence_count = 0
        len_sequence_list = len(sequence_list)
        profile_classes = super_family.get_profile_classes()
        ff_dict = super_family.get_field_functions_dict()

        mng = multiprocessing.Manager()
        ns = mng.Namespace()
        ns.count = 0
        result_queue = None if not use_multiprocessing else mng.Queue()

        iterable_args = ((seq_id, seq_char_id, f_char_id, target_f_char_id, aa_string, secstr,
                          profile_classes, ff_dict, ns, len_sequence_list, iteration_count, result_queue)
                        for seq_id, seq_char_id, seq_char_id2, f_char_id, aa_string, secstr in sequence_list)

        if use_multiprocessing:
            processes_dict = {}
            results = []

            for _args in iterable_args:
                #result_id = seq_char_id, f_char_id, target_f_char_id
                result_id = _args[1:4]

                p = multiprocessing.Process(target=_LearningManager__evaluate_sequence, args=_args)
                p.start()
                processes_dict[result_id] = p

                if len(processes_dict) >= MyGlobals.NUM_PARALLEL_SEQUENCES_IN_TESTING:
                    result = result_queue.get()
                    result_id = result[0]
                    results.append(result)
                    result_queue.task_done()

                    p = processes_dict.pop(result_id)
                    p.join()

            while len(processes_dict) > 0:
                result = result_queue.get()
                result_id = result[0]
                results.append(result)
                result_queue.task_done()

                p = processes_dict.pop(result_id)
                p.join()

        else:
            results = [_LearningManager__evaluate_sequence(*_args) for _args in iterable_args]

        score_and_positivity_list = []
        length_list = []

        for result_id, result in results:
            seq_char_id, f_char_id, target_f_char_id = result_id
            is_positive = (f_char_id == target_f_char_id)
            score, path_length, col_row_prob_list = result
            score_and_positivity_list.append((score, is_positive))

        auc = AucRoc.calculate_auc(score_and_positivity_list)

        dao = MyDAO()
        dao.save_testing_result(iteration_count, (('auc', 'auc', target_f_char_id), (auc, 0, None)))
        del dao

        return results, auc


def _LearningManager__calculate_table(next_sequence, profile_classes, ff_dict, marked_positions,
                                      dao_suffix, is_init, neg_example_queue, is_classification):
    # import gc

    cdef Trellis trellis

    trellis = Trellis(trellis_id=next_sequence["id"], pc_rows=profile_classes, aa_columns=list(next_sequence["aa_string"]),
                      ss_columns=(list(next_sequence["secstr"]) if next_sequence["secstr"] else []),
                      aligned_sequence=list(next_sequence["aligned_aa_string"]), marked_positions=list(marked_positions))

    print("----> Calculating trellis... ")
    trellis.calculate_trellis(MyGlobals.NUM_ITER_IN_CALC_TRELLIS, ff_dict)     # (nb_of_iterations, is_initial_value, is_training)
    print("----> Generating examples... ")
    trellis.generate_examples(ff_dict, neg_example_queue, is_classification)
    print("----> Finished generating examples... ")

    trellis = None
    del trellis
    # gc.collect()
    return ''


def _LearningManager__evaluate_sequence(seq_id, seq_char_id, f_char_id, target_f_char_id, aa_string, secstr,
                                        profile_classes, ff_dict, ns, len_sequence_list, iteration_count, result_queue):
    import gc

    cdef Trellis trellis

    ns.count += 1
    count = ns.count

    result_id = seq_char_id, f_char_id, target_f_char_id

    print(">>>> Building trellis...")

    trellis = Trellis(trellis_id=seq_id, pc_rows=profile_classes, aa_columns=list(aa_string),
                      ss_columns=(list(secstr) if secstr else []))

    print(">>>> Calculating values for sequence %s from family %s against target family %s... "
                  "[#%d of %d]\n"
          % (seq_char_id, f_char_id, target_f_char_id, count, len_sequence_list))

    trellis.calculate_trellis(MyGlobals.NUM_ITER_IN_CALC_TRELLIS, ff_dict, True)  # (nb_of_iterations, is_initial_value, is_training)
    best_path_aa_string_length, avg_log_score = trellis.get_avg_log_score()

    print("<<<< Sequence %s from family %s against target family %s -> best path score: %e | string length: %d [#%d of %d]"
          % (seq_char_id, f_char_id, target_f_char_id, avg_log_score, best_path_aa_string_length, count, len_sequence_list))

    col_row_prob_list = None # trellis.get_col_row_prob_list() if MyGlobals.PLOT_HIST2D else None
    trellis = None
    del trellis

    result = result_id, (avg_log_score, best_path_aa_string_length, col_row_prob_list)

    dao = MyDAO()
    dao.save_testing_result(iteration_count, result)
    del dao

    gc.collect()

    if result_queue is None:
        return result
    else:
        result_queue.put(result)
        return 0
