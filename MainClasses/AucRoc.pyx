__author__ = 'romulo'

import MyGlobals


def calculate_auc(list prob_and_if_positive_list, bint reversed_sort=True):
    cdef list prob_and_if_positive_list_sorted
    prob_and_if_positive_list_sorted = sorted(prob_and_if_positive_list, key=lambda x: x[0], reverse=reversed_sort)

    if MyGlobals.USE_AUC_PR:
        return _calculate_auc_pr(prob_and_if_positive_list_sorted)
    else:
        return _calculate_auc_roc(prob_and_if_positive_list_sorted)


cdef float _calculate_auc_roc(list prob_and_if_positive_list) except -1:
    cdef int false_positive_count = 0
    cdef int true_positive_count = 0
    cdef int prev_false_positive_count = 0
    cdef int prev_true_positive_count = 0

    cdef float prob_value
    cdef float prev_prob_value = -1.0
    cdef float trapezoid_area
    cdef float auc_roc = 0

    cdef bint is_positive

    for prob_value, is_positive in prob_and_if_positive_list:

        if prob_value != prev_prob_value:
            trapezoid_area = abs((false_positive_count-prev_false_positive_count)*
                                 (true_positive_count+prev_true_positive_count)/2.0)
            auc_roc += trapezoid_area
            prev_true_positive_count = true_positive_count
            prev_false_positive_count = false_positive_count

        prev_prob_value = prob_value

        if is_positive:
            true_positive_count += 1
        else:
            false_positive_count += 1

    trapezoid_area = abs((false_positive_count - prev_false_positive_count) *
                         (true_positive_count + prev_true_positive_count)/2.0)
    auc_roc = (auc_roc + trapezoid_area) / (max(true_positive_count, 1) * max(false_positive_count, 1))

    return auc_roc


cdef float _calculate_auc_pr(list prob_and_if_positive_list) except -1:
    cdef int classifier_positive_count = 0
    cdef int true_positive_count = 0
    cdef int actual_positive_count = 0

    cdef float prev_precision = 1.0
    cdef float prev_recall = 0.0
    cdef float precision
    cdef float recall

    cdef float prob_value
    cdef float prev_prob_value = -1.0
    cdef float trapezoid_area
    cdef float auc_pr = 0

    cdef bint is_positive
    cdef bint first_trapezoid = True
    cdef list is_positive_list

    is_positive_list = list(zip(*prob_and_if_positive_list)[1])
    actual_positive_count = is_positive_list.count(True)

    for prob_value, is_positive in prob_and_if_positive_list:
        if prob_value != prev_prob_value and classifier_positive_count > 0:
            precision = true_positive_count * 1.0 / classifier_positive_count
            recall = true_positive_count * 1.0 / actual_positive_count

            if first_trapezoid:
                first_trapezoid = False
                prev_precision = precision

            trapezoid_area = abs((recall - prev_recall) * (precision + prev_precision)/2.0)
            auc_pr += trapezoid_area

            prev_precision = precision
            prev_recall = recall

        prev_prob_value = prob_value

        if is_positive:
            true_positive_count += 1

        classifier_positive_count += 1

    trapezoid_area = abs((recall - prev_recall) * (precision + prev_precision)/2.0)
    auc_pr += trapezoid_area

    return auc_pr
