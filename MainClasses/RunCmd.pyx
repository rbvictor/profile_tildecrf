__author__ = 'rbvictor'

import subprocess as sub
import os
import signal
import threading


class RunCmd(threading.Thread):
    def __init__(self, cmd, timeout=None):
        threading.Thread.__init__(self)
        self.cmd = cmd
        self.timeout = timeout

    def run(self):
        self.p = sub.Popen(self.cmd, stdout=sub.PIPE, shell=True, preexec_fn=os.setsid)
        self.p.wait()

    def run_cmd(self):
        self.start()

        if self.timeout:
            self.join(self.timeout)
        else:
            self.join()

        if self.is_alive():
            os.killpg(self.p.pid, signal.SIGKILL)
            self.join()
            return False

        else:
            return True