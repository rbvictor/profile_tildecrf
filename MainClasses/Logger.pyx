import MyGlobals

__author__ = 'rbvictor'

import os


class Logger:
    def __init__(self):
        pass

    @staticmethod
    def print_overall_results(auc_dict):

        results_folder = "%s%s" % (MyGlobals.get_current_dir(), MyGlobals.RESULTS_FOLDER)

        if not os.path.exists(results_folder):
            os.makedirs(results_folder)

        auc_table_filename = "auc_table.txt"

        with open(results_folder + auc_table_filename, 'w') as f:
            auc_dict_keys = sorted([key for key in auc_dict])

            last_sf_id = None
            line = ""
            auc_per_line_list = []
            all_auc_list = []

            for sf_id, target_f_char_id in auc_dict_keys:

                if last_sf_id != sf_id:
                    if line:
                        avg_auc_per_line = sum(auc_per_line_list) / len(auc_per_line_list)
                        line += ("AVERAGE:\t%s\t|\n" % str(avg_auc_per_line))
                        f.write(line)

                    auc_per_line_list = []
                    line = ("\n%d \t|\t" % sf_id)
                    last_sf_id = sf_id

                auc = auc_dict[(sf_id, target_f_char_id)][-1]
                line += ("target \"%s\":\t%s\t|\t" % (target_f_char_id, str(auc)))

                auc_per_line_list.append(auc)
                all_auc_list.append(auc)

            if line:
                avg_auc_per_line = sum(auc_per_line_list) / len(auc_per_line_list)
                line += ("AVERAGE:\t%s\t|\n" % str(avg_auc_per_line))
                f.write(line)

            total_avg_auc = sum(all_auc_list)/len(all_auc_list)
            line = "\n\nTOTAL AVERAGE:\t%s" % str(total_avg_auc)
            f.write(line)

    @staticmethod
    def print_results(results, target_f_char_id, auc, iteration_index=None):
        confusion_matrix_dict_a = {}

        results_folder = None
        hist_folder = None
        confusion_matrix_folder = None
        results.sort(key=lambda x: (x[0][1], x[0][0]))

        for result_id, result in results:
            print("--> Printing results for %s ..." % str(result_id))

            sequence_char_id, sequence_sf_id, model_sf_id = result_id
            is_positive = (model_sf_id == sequence_sf_id)
            score, path_length, col_row_prob_list = result

            if results_folder is None:
                results_folder = "%s%s" % (MyGlobals.get_current_dir(), MyGlobals.RESULTS_FOLDER)

                hist_folder = results_folder + "hist2d/"
                confusion_matrix_folder = results_folder + "confusion_matrix/"

                if not os.path.exists(results_folder):
                    os.makedirs(results_folder)

                if not os.path.exists(hist_folder):
                    os.makedirs(hist_folder)

                if not os.path.exists(confusion_matrix_folder):
                    os.makedirs(confusion_matrix_folder)

            confusion_matrix_dict_a.setdefault(model_sf_id, []).append((sequence_char_id, is_positive, score))

            filename = "tf_%s_seq_%s_%s%s" % (target_f_char_id, sequence_char_id, str(is_positive),
                                             ("_itr_%d" % iteration_index) if iteration_index is not None else "")

            if MyGlobals.PLOT_HIST2D and col_row_prob_list is not None:
                png_filename = "%s_hist2d.png" % filename
                Logger._create_histogram_2d(col_row_prob_list, hist_folder + png_filename)

        txt_filename = "confusion_matrix_tf_%s_%s.txt" % \
                       (target_f_char_id, ("_itr_%d" % iteration_index) if iteration_index is not None else "")

        with open(confusion_matrix_folder + txt_filename, 'w') as f:
            f.write("Using best path probability...\n")
            f.write(Logger._pretty_print_confusion_matrix(confusion_matrix_dict_a))
            f.close()

        tf_auc_filepath = results_folder + "auc_tf_%s.txt" % target_f_char_id
        with open(tf_auc_filepath, 'a') as f:
            f.write("Itr %d:\t%s\n" % (iteration_index, str(auc)))
            f.close()

    @staticmethod
    def _pretty_print_confusion_matrix(confusion_matrix_dict):
        s = ""
        delimiter = None
        row_header_length = 8
        column_header_length = 16
        num_columns_per_line = 10

        keys = sorted([key for key in confusion_matrix_dict])
        max_num_columns = len(confusion_matrix_dict[keys[0]])

        for i in range(0, max_num_columns, num_columns_per_line):
            print_column_headers = True

            for key in keys:
                column_headers, is_positive_list, values = \
                    zip(*confusion_matrix_dict[key][i:min(i+num_columns_per_line, max_num_columns)])

                if print_column_headers:
                    s += "|".join([" " * row_header_length] +
                                  [" " + str(header) + " " * (column_header_length - len(header) - 1)
                                   for header in column_headers])
                    if not delimiter:
                        delimiter = "\n" + "-" * len(s) + "\n"
                    s += delimiter
                    print_column_headers = False

                s += "|".join([" " + str(key) + " " * (row_header_length - len(str(key)) - 1)] +
                              ["%s %e %s" % ("+" if is_positive_list[i] else "-", values[i],
                                             "+" if is_positive_list[i] else "-") for i in range(len(values))])
                s += "\n"
            s += "\n"

        return s

    @staticmethod
    def _create_histogram_2d(col_row_prob_list, filename):
        import matplotlib.pyplot as plt

        x, y, weights = zip(*col_row_prob_list)

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        step = 4
        max_x, max_y = max(x), max(y)

        x_edges = [i - 0.5 for i in xrange(0, max_x + (2 * step - max_x % step), step)]
        y_edges = [i - 0.5 for i in xrange(0, max_y + (2 * step - max_y % step), step)]

        hist = ax.hist2d(x, y, bins=[x_edges, y_edges], normed=True, weights=weights)

        plt.gca().invert_yaxis()
        fig.colorbar(hist[3])
        fig.savefig(filename, bbox_inches=0)
