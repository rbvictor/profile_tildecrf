#!/usr/bin/env python

"""
Python Profile CRF Project - MyGlobal.py
"""

# --- General data
# MY_OS = 'mac'
MY_OS = 'linux'
USE_AUC_PR = True

AMINO_ACID_LIST = ['a', 'r', 'n', 'd', 'c', 'q', 'e', 'g', 'h', 'i', 'l', 'k',
                   'm', 'f', 'p', 's', 't', 'w', 'y', 'v', 'b', 'z', 'x', '-']

# --- Multiple files
USE_MULTIPROCESSING = True

from sys import float_info, maxint
MIN_FLOAT_VALUE = float_info.min
MAX_FLOAT_VALUE = float_info.max
MAX_INT_VALUE = maxint

from math import sqrt
SQRT_MIN_FLOAT_VALUE = sqrt(MIN_FLOAT_VALUE)

USE_BAUM_WELCH = False

# Main
EXECUTE_TRAINING = True
NUM_OF_ITERATIONS = 10
BATCH_SIZE = 3
RMODE_CONST_GEN_MAX_EXAMPLES = 4
RMODE_CONST_GEN_MAX_VALUES_PER_EXAMPLE = 1
# FASTA_FILE = "/data/scop.1.53.1e-25.fasta"
# TRAINING_INFO_TABLE_FILE = "/data/all_seq_fam_membership.txt"
FASTA_FILE = "/data/sample_sequences.fasta"
TRAINING_INFO_TABLE_FILE = "/data/sample_seq_membership.txt"
# FASTA_FILE = "/data/other_examples.fasta"
# TRAINING_INFO_TABLE_FILE = "/data/other_examples_training_info.txt"
SS_FILE = "/data/ss_selected.txt"


USE_AA_RMODE = True
AA_RMODE_ORDER = [0]
USE_BLOSUM62_RMODE = True
BLOSUM62_RMODE_ORDER = [0]
BLOSUM62_FLOOR = 0.0
USE_AA_TYPE_RMODES = True
AA_TYPE_RMODES_ORDER = [0]
USE_BLOCKS9_RMODES = False
BLOCKS9_RMODES_ORDER = [0]
USE_SS_RMODE = False
SS_RMODE_ORDER = [0]
ALTERNATE_RMODE_EACH_ITER = True



# MyDAO credentials

# MYSQL_HOST = "9.0.0.252"   #server_oar
# MYSQL_LOGIN = "romulobvictor"
# MYSQL_DBNAME = "romulobvictor"
# MYSQL_PASSWD = "12romulo21"
MYSQL_HOST = "localhost"
MYSQL_LOGIN = "root"
MYSQL_DBNAME = "db_profile"
MYSQL_PASSWD = ""

MYSQL_PORT = 3306

# MyDAO
USE_PP_IN_DAO = True
USE_TABLE_NAME_WITH_GROUP_ID = True
MAX_MYSQL_TABLE_SIZE_IN_GB = 4
NUM_MYSQL_CONNECT_ATTEMPTS = 10
NUM_OF_QUERIES_BEFORE_COMMIT = 10000
USE_EXP_VARIANCE = True
CLUSTAL_PATH = "/externals/clustal/%s/clustalo" % MY_OS
USE_LINE_NUM_AS_SEQ_ID = True
#DAO_CONSUMERS_IN_ROUND_ROBIN = False
TMP_FOLDER = "/tmp/"
KEEP_TMP_FILES = True
USE_SQLITE3 = True
SQLITE3_DB_FILENAME = "%s/MyDB%s_gr_%s.db"
SQLITE3_DB_FOLDER = "/Volumes/ramdisk/MyDB" if MY_OS == 'mac' else "/dev/shm/MyDB"
SQLITE3_TIMEOUT = 60
SQLITE3_PRAGMAS = ["PRAGMA journal_mode=WAL", "PRAGMA synchronous=OFF", "PRAGMA page_size=65536",
                   "PRAGMA cache_size=10000"]

#Trellis
LEARNING_RATE = 2.0
USE_CLASSIFICATION_TREE = True

# FOLClassificationTree
MAX_CLASSIFICATION_TREE_DEPTH = 16

# MyDAO -- related to FOLClassificationTree
MIN_COVERAGE = 0.20
MIN_FALSE_EXAMPLE_COVERAGE = 0.10

#DAOWorkManager
NUM_OF_DAO_WORK_MANAGERS = 16

# HMMER options
HMMER_PATH = "/externals/hmmer/%s/binaries" % MY_OS
USE_PRIORS_IN_HMMER = False
ALWAYS_USE_HMMER_CONSENSUS = True

# FieldFunction
FF_KEEPS_INIT_FUNC = True

# FOLDecisionTree
MIN_NUM_EXAMPLES = 1  # it should be greater than or equal to 3
MIN_VARIANCE = 0.001
INSERT_MIN_VARIANCE = 2.5 * MIN_VARIANCE
MAX_TREE_DEPTH = 16
MIN_TREE_DEPTH = 1


# Splitter

# LearningManager
USE_PP_IN_BATCHES = True
NUM_PARALLEL_SEQUENCES_IN_TRAINING = 2
NUM_PARALLEL_SEQUENCES_IN_TESTING = 3
PROCESS_TIMEOUT = 10
ENABLE_INSERT_FIELD_FUNCTIONS = True
RESULTS_FOLDER = "/results/"
TEST_EVERY_ITERATION = False
TEST_FIRST_ITERATION = False
PLOT_HIST2D = False
NUM_ITER_IN_CALC_TRELLIS = 2                    # minimum 2 please!!! Otherwise, special argument '_' (that unifies with everything)
                                                # will be among positive examples

# SuperFamily
USE_PP_IN_FF_UPDATE = True
NUM_PARALLEL_UPDATE_FF = 8
APPLY_HEURISTICS = True
INITIAL_VALUE_FUNCTION_TYPE_OPTIONS = ['none', 'score', 'hmm']
INITIAL_VALUE_FUNCTION_TYPE = <str> INITIAL_VALUE_FUNCTION_TYPE_OPTIONS[2]
HMM_MAX_FLOAT = 20.0
# HMM_MAX_FLOAT = 'inf'
PSEUDOCOUNT_CONST_A = 20.0

# PathExample
WINDOW_LENGTH = 5
ONLY_MATCH_STATES = False

#Filename formats
CLUSTAL_INPUT_FILENAME_FORMAT = "tf_%s_clustalo_input.fasta"
CLUSTAL_OUTPUT_FILENAME_FORMAT = "tf_%s_clustalo_output.sto"
HMMBUILD_INPUT_FILENAME_FORMAT = "tf_%s_hmmbuild_input.sto"
HMMBUILD_OUTPUT_FILENAME_FORMAT = "tf_%s_hmmbuild_output.hmm"
HMMALIGN_OUTPUT_FILENAME_FORMAT = "tf_%s_hmmalign_output.sto"
HMMSEARCH_INPUT_FILENAME_FORMAT = "tf_%s_hmmsearch_input.fasta"
HMMSEARCH_OUTPUT_FILENAME_FORMAT = "tf_%s_hmmsearch_output.out"

BACKGROUND_PROB_TYPE = ["Blosum62", "Altschul", "Dayhoff", "Robinson"][1]

#F_Test_Table
F_TEST_TABLE_ALPHA = 0.5


def get_current_dir():
    import os
    return os.path.dirname(os.path.abspath(__file__))

