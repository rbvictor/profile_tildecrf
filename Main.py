#!/usr/bin/env python

"""
Python Profile CRF Project - Main.py
"""
from MainClasses.FirstOrderLogic.RmodeManager import RmodeManager


__author__ = "Romulo Barroso Victor"
__credits__ = ["Romulo Barroso Victor", "Gerson Zaverucha", "Juliana Bernardes"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Romulo Barroso Victor"
__email__ = "rbvictor@cos.ufrj.br"
__status__ = "Development"
__date__ = "10-Jul-2013"

import datetime
import sys
import os
from MainClasses.LearningManager import LearningManager
import MyGlobals

if __name__ == '__main__':

    argv = sys.argv
    n_argv = len(argv)

    target_family_index = 0
    nb_of_target_families = 0
    load_files = False
    load_files_only = False
    load_trees = False
    reset_tables = False
    run_hmmer = False
    i = 0

    while i < n_argv:
        if argv[i] == "--start":
            target_family_index = int(argv[i+1])
            i += 1

        elif argv[i] == "--size":
            nb_of_target_families = int(argv[i+1])
            i += 1

        elif argv[i] == "--load-files":
            print(":: --load-files on")
            load_files = True

        elif argv[i] == "--load-files-only":
            print(":: --load-files-only on")
            load_files_only = True

        elif argv[i] == "--load-trees":
            print(":: --load-trees on")
            load_trees = True

        elif argv[i] == "--reset-tables":
            print(":: --reset-tables on")
            reset_tables = True

        elif argv[i] == "--run-hmmer":
            print(":: --run-hmmer on")
            run_hmmer = True

        elif i > 0:
            print("!! option not recognized: %s" % argv[i])

        i += 1

    start_time = datetime.datetime.now()
    print(":: Execution started on " + str(start_time))

    if MyGlobals.MY_OS == 'mac' and not os.path.exists("/Volumes/ramdisk"):
        from MainClasses.RunCmd import RunCmd
        RunCmd("diskutil erasevolume HFS+ \"ramdisk\" `hdiutil attach -nomount ram://4194304`").run_cmd()

    # [STEP 1]

    #setup rmodes...
    rmode_mng = RmodeManager()

    if MyGlobals.USE_AA_RMODE:
        rmode_mng.use_aa_rmode()

    if MyGlobals.USE_SS_RMODE:
        rmode_mng.use_ss_rmode()

    #Amino acid type rmode
    if MyGlobals.USE_AA_TYPE_RMODES:
        rmode_mng.use_aa_type_rmodes()

    #Blosum rmode
    if MyGlobals.USE_BLOSUM62_RMODE:
        rmode_mng.use_blosum62_rmode()

    #Blocks9 rmode
    if MyGlobals.USE_BLOCKS9_RMODES:
        rmode_mng.use_blocks9_rmodes()

    learn_mng = LearningManager(rmode_mng, not load_files)

    #execute training...
    data_dir = 'data'

    #reset FIELD_FUNCTIONS and RESULTS tables from MySQL
    if reset_tables or load_files or load_files_only:
        learn_mng.reset_ff_and_results_tables(load_trees)

    #Load all sequences FASTA file
    if load_files or load_files_only:
        filename_tuple_list = []

        # noinspection PyUnresolvedReferences
        current_dir = MyGlobals.get_current_dir()
        fasta_file_path = "%s%s" % (current_dir, MyGlobals.FASTA_FILE)
        training_info_table_file_path = "%s%s" % (current_dir, MyGlobals.TRAINING_INFO_TABLE_FILE)
        ss_file_path = "%s%s" % (current_dir, MyGlobals.SS_FILE)

        learn_mng.load_sequence_file_to_db(fasta_file_path)
        learn_mng.load_training_info_table(training_info_table_file_path)
        learn_mng.load_ss_file_to_db(ss_file_path)
        learn_mng.load_alignments_to_db(run_hmmer)

    execute_training = nb_of_target_families and MyGlobals.EXECUTE_TRAINING

    if execute_training and not load_files_only:
        learn_mng.execute_training_and_test(nb_of_iterations=MyGlobals.NUM_OF_ITERATIONS,
                                            batch_size=MyGlobals.BATCH_SIZE,
                                            target_family_index=target_family_index,
                                            nb_of_target_families=nb_of_target_families,
                                            load_trees=load_trees)
        end_time = datetime.datetime.now()
        print(":: Training ended on " + str(end_time) + " and lasted " + str(end_time - start_time))

    end_time = datetime.datetime.now()
    print(":: Execution ended on " + str(end_time) + " and lasted " + str(end_time - start_time))

