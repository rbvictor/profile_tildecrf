__author__ = 'rbvictor'
import MySQLdb
import MyGlobals

if __name__ == "__main__":

    conn = MySQLdb.connect(host='localhost', port=36, user="root", passwd="", db="db_profile")
    cursor = conn.cursor()

    print("Importing ss data...")

    cursor.execute("DROP TABLE IF EXISTS SS_DATA")

    cursor.execute("CREATE TABLE SS_DATA ("
                   "name VARCHAR(10),"
                   "sequence MEDIUMTEXT,"
                   "secstr MEDIUMTEXT)")

    cursor.execute("COMMIT")

    with open("%s/data/ss.txt" % MyGlobals.get_current_dir(), 'r') as f:
        name=""
        sequence = ""
        secstr = ""
        flag = ""
        commit_count = 0

        for line in f:

            if line[0] == '>':
                if sequence and secstr:
                    cursor.execute("INSERT INTO SS_DATA VALUES ('%s', '%s', '%s')"
                                   % (name, sequence, secstr))
                    sequence = ""
                    secstr = ""

                    commit_count += 1
                    if commit_count == 100:
                        cursor.execute("COMMIT")
                        commit_count = 0

                split_line = line.lower().strip('\n').strip('\r')[1:].split(':')
                name = 'd%s%s' % (split_line[0].lower(), split_line[1])
                flag = split_line[2]

            elif flag == 'sequence':
                sequence += line.lower().strip('\n').strip('\r')

            elif flag == 'secstr':
                secstr += line.lower().strip('\n').strip('\r').replace(' ', '-')

        if sequence and secstr:
            cursor.execute("INSERT INTO SS_DATA VALUES ('%s', '%s', '%s')"
                           % (name, sequence, secstr))
            sequence = ""
            secstr = ""
            cursor.execute("COMMIT")

        f.close()

    print("Creating table...")

    cursor.execute("DROP TABLE IF EXISTS SECONDARY_STRUCTURES")

    cursor.execute("CREATE TABLE SECONDARY_STRUCTURES(                                                             \n"
                   "SELECT char_id,                                                                                \n"
                   "	 IF(locate(left(aa_string, 5), sequence) > 0,                                              \n"
                   "		substring(secstr, locate(left(aa_string, 5), sequence), length(aa_string)),            \n"
                   "		reverse(substring(reverse(secstr),                                                     \n"
                   "               locate(left(reverse(aa_string), 5), reverse(sequence)),                         \n"
                   "               length(aa_string)))) AS secstr                                                  \n"
                   "FROM loaded_sequences, SS_DATA                                                                 \n"
                   "WHERE replace(left(char_id, 6), '_', 'a') = left(name, 6)                                      \n"
                   "	OR (mid(char_id, 2, 4) = mid(name, 2, 4) AND right(char_id, 1) = right(name, 1))           \n"
                   ")")

    cursor.execute("COMMIT")
    cursor.close()
    conn.close()
